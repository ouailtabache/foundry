################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/HX711.c \
../Core/Src/SPI_Handler.c \
../Core/Src/VL53L0x.c \
../Core/Src/additionalFunctions.c \
../Core/Src/ads1115.c \
../Core/Src/am2320.c \
../Core/Src/commandHandler.c \
../Core/Src/dma.c \
../Core/Src/encoders.c \
../Core/Src/endStops.c \
../Core/Src/foundryData.c \
../Core/Src/freertos.c \
../Core/Src/gpio.c \
../Core/Src/i2c.c \
../Core/Src/main.c \
../Core/Src/max31856.c \
../Core/Src/motionGenerator.c \
../Core/Src/motorHandler.c \
../Core/Src/pouringControl.c \
../Core/Src/processHandler.c \
../Core/Src/sendData.c \
../Core/Src/sensorRead.c \
../Core/Src/spi.c \
../Core/Src/stm32h7xx_hal_msp.c \
../Core/Src/stm32h7xx_hal_timebase_tim.c \
../Core/Src/stm32h7xx_it.c \
../Core/Src/syscalls.c \
../Core/Src/sysmem.c \
../Core/Src/system_stm32h7xx.c \
../Core/Src/tim.c \
../Core/Src/usart.c \
../Core/Src/usb_otg.c 

OBJS += \
./Core/Src/HX711.o \
./Core/Src/SPI_Handler.o \
./Core/Src/VL53L0x.o \
./Core/Src/additionalFunctions.o \
./Core/Src/ads1115.o \
./Core/Src/am2320.o \
./Core/Src/commandHandler.o \
./Core/Src/dma.o \
./Core/Src/encoders.o \
./Core/Src/endStops.o \
./Core/Src/foundryData.o \
./Core/Src/freertos.o \
./Core/Src/gpio.o \
./Core/Src/i2c.o \
./Core/Src/main.o \
./Core/Src/max31856.o \
./Core/Src/motionGenerator.o \
./Core/Src/motorHandler.o \
./Core/Src/pouringControl.o \
./Core/Src/processHandler.o \
./Core/Src/sendData.o \
./Core/Src/sensorRead.o \
./Core/Src/spi.o \
./Core/Src/stm32h7xx_hal_msp.o \
./Core/Src/stm32h7xx_hal_timebase_tim.o \
./Core/Src/stm32h7xx_it.o \
./Core/Src/syscalls.o \
./Core/Src/sysmem.o \
./Core/Src/system_stm32h7xx.o \
./Core/Src/tim.o \
./Core/Src/usart.o \
./Core/Src/usb_otg.o 

C_DEPS += \
./Core/Src/HX711.d \
./Core/Src/SPI_Handler.d \
./Core/Src/VL53L0x.d \
./Core/Src/additionalFunctions.d \
./Core/Src/ads1115.d \
./Core/Src/am2320.d \
./Core/Src/commandHandler.d \
./Core/Src/dma.d \
./Core/Src/encoders.d \
./Core/Src/endStops.d \
./Core/Src/foundryData.d \
./Core/Src/freertos.d \
./Core/Src/gpio.d \
./Core/Src/i2c.d \
./Core/Src/main.d \
./Core/Src/max31856.d \
./Core/Src/motionGenerator.d \
./Core/Src/motorHandler.d \
./Core/Src/pouringControl.d \
./Core/Src/processHandler.d \
./Core/Src/sendData.d \
./Core/Src/sensorRead.d \
./Core/Src/spi.d \
./Core/Src/stm32h7xx_hal_msp.d \
./Core/Src/stm32h7xx_hal_timebase_tim.d \
./Core/Src/stm32h7xx_it.d \
./Core/Src/syscalls.d \
./Core/Src/sysmem.d \
./Core/Src/system_stm32h7xx.d \
./Core/Src/tim.d \
./Core/Src/usart.d \
./Core/Src/usb_otg.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/%.o Core/Src/%.su Core/Src/%.cyclo: ../Core/Src/%.c Core/Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32H7A3xxQ -c -I../Core/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../Drivers/CMSIS/Include -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I"D:/FOUNDRY_FIRMWARE_STM32/01_INTEGRATION/FOUNDRY_FIRMWARE/Drivers/VL53L0X_Api/core/inc" -I"D:/FOUNDRY_FIRMWARE_STM32/01_INTEGRATION/FOUNDRY_FIRMWARE/Drivers/VL53L0X_Api/platform/inc" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Core-2f-Src

clean-Core-2f-Src:
	-$(RM) ./Core/Src/HX711.cyclo ./Core/Src/HX711.d ./Core/Src/HX711.o ./Core/Src/HX711.su ./Core/Src/SPI_Handler.cyclo ./Core/Src/SPI_Handler.d ./Core/Src/SPI_Handler.o ./Core/Src/SPI_Handler.su ./Core/Src/VL53L0x.cyclo ./Core/Src/VL53L0x.d ./Core/Src/VL53L0x.o ./Core/Src/VL53L0x.su ./Core/Src/additionalFunctions.cyclo ./Core/Src/additionalFunctions.d ./Core/Src/additionalFunctions.o ./Core/Src/additionalFunctions.su ./Core/Src/ads1115.cyclo ./Core/Src/ads1115.d ./Core/Src/ads1115.o ./Core/Src/ads1115.su ./Core/Src/am2320.cyclo ./Core/Src/am2320.d ./Core/Src/am2320.o ./Core/Src/am2320.su ./Core/Src/commandHandler.cyclo ./Core/Src/commandHandler.d ./Core/Src/commandHandler.o ./Core/Src/commandHandler.su ./Core/Src/dma.cyclo ./Core/Src/dma.d ./Core/Src/dma.o ./Core/Src/dma.su ./Core/Src/encoders.cyclo ./Core/Src/encoders.d ./Core/Src/encoders.o ./Core/Src/encoders.su ./Core/Src/endStops.cyclo ./Core/Src/endStops.d ./Core/Src/endStops.o ./Core/Src/endStops.su ./Core/Src/foundryData.cyclo ./Core/Src/foundryData.d ./Core/Src/foundryData.o ./Core/Src/foundryData.su ./Core/Src/freertos.cyclo ./Core/Src/freertos.d ./Core/Src/freertos.o ./Core/Src/freertos.su ./Core/Src/gpio.cyclo ./Core/Src/gpio.d ./Core/Src/gpio.o ./Core/Src/gpio.su ./Core/Src/i2c.cyclo ./Core/Src/i2c.d ./Core/Src/i2c.o ./Core/Src/i2c.su ./Core/Src/main.cyclo ./Core/Src/main.d ./Core/Src/main.o ./Core/Src/main.su ./Core/Src/max31856.cyclo ./Core/Src/max31856.d ./Core/Src/max31856.o ./Core/Src/max31856.su ./Core/Src/motionGenerator.cyclo ./Core/Src/motionGenerator.d ./Core/Src/motionGenerator.o ./Core/Src/motionGenerator.su ./Core/Src/motorHandler.cyclo ./Core/Src/motorHandler.d ./Core/Src/motorHandler.o ./Core/Src/motorHandler.su ./Core/Src/pouringControl.cyclo ./Core/Src/pouringControl.d ./Core/Src/pouringControl.o ./Core/Src/pouringControl.su ./Core/Src/processHandler.cyclo ./Core/Src/processHandler.d ./Core/Src/processHandler.o ./Core/Src/processHandler.su ./Core/Src/sendData.cyclo ./Core/Src/sendData.d ./Core/Src/sendData.o ./Core/Src/sendData.su ./Core/Src/sensorRead.cyclo ./Core/Src/sensorRead.d ./Core/Src/sensorRead.o ./Core/Src/sensorRead.su ./Core/Src/spi.cyclo ./Core/Src/spi.d ./Core/Src/spi.o ./Core/Src/spi.su ./Core/Src/stm32h7xx_hal_msp.cyclo ./Core/Src/stm32h7xx_hal_msp.d ./Core/Src/stm32h7xx_hal_msp.o ./Core/Src/stm32h7xx_hal_msp.su ./Core/Src/stm32h7xx_hal_timebase_tim.cyclo ./Core/Src/stm32h7xx_hal_timebase_tim.d ./Core/Src/stm32h7xx_hal_timebase_tim.o ./Core/Src/stm32h7xx_hal_timebase_tim.su ./Core/Src/stm32h7xx_it.cyclo ./Core/Src/stm32h7xx_it.d ./Core/Src/stm32h7xx_it.o ./Core/Src/stm32h7xx_it.su ./Core/Src/syscalls.cyclo ./Core/Src/syscalls.d ./Core/Src/syscalls.o ./Core/Src/syscalls.su ./Core/Src/sysmem.cyclo ./Core/Src/sysmem.d ./Core/Src/sysmem.o ./Core/Src/sysmem.su ./Core/Src/system_stm32h7xx.cyclo ./Core/Src/system_stm32h7xx.d ./Core/Src/system_stm32h7xx.o ./Core/Src/system_stm32h7xx.su ./Core/Src/tim.cyclo ./Core/Src/tim.d ./Core/Src/tim.o ./Core/Src/tim.su ./Core/Src/usart.cyclo ./Core/Src/usart.d ./Core/Src/usart.o ./Core/Src/usart.su ./Core/Src/usb_otg.cyclo ./Core/Src/usb_otg.d ./Core/Src/usb_otg.o ./Core/Src/usb_otg.su

.PHONY: clean-Core-2f-Src

