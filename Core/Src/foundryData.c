/*
 * foundryStatus.c
 *
 *  Created on: Aug 18, 2022
 *      Author: MM3D
 */

#include "foundryData.h"

void foundry_init(foundry *Foundry) {
	Foundry->state = 0;

	Foundry->pressure = 0;
	Foundry->loadcell_reading = 0;
	Foundry->crucible_volume = 0;
	Foundry->humidity = 0;
	Foundry->ambient_temp = 0;

	Foundry->c_pos = 0;
	Foundry->z_pos = 0;
	Foundry->t_pos = 0;

	Foundry->thermocouplesArray[0] = 0;
	Foundry->thermocouplesArray[1] = 0;
	Foundry->faultThermocouplesArray[0] = 0;
	Foundry->faultThermocouplesArray[1] = 0;

	Foundry->Smooth_pressure = 0;
	Foundry->distance = 0;
	Foundry->distance_interne=0;
}

void setCrucibleVolume(foundry *Foundry, double volume) {
	Foundry->crucible_volume = volume;
}

double getCrucibleVolume(foundry *Foundry) {
	return Foundry->crucible_volume;
}
