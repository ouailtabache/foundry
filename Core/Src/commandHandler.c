/*
 * commandHandler.c
 *
 *  Created on: Aug 12, 2022
 *      Author: MM3D | awitc
 */

#include "commandHandler.h"

uint8_t led_green_blink = OFF, temp_green_blink_state = ON;
uint8_t led_yellow_blink = OFF, temp_yellow_blink_state = ON;
uint8_t led_red_blink = OFF, temp_red_blink_state = ON;
extern uint8_t InitStatuts;
queue_commandRx queueCommandRx;
uint8_t rxBuffer;
//uint8_t rxData[RX_SIZE];
uint8_t cnt_rx;
uint8_t state_rx=0;

FLASK_DETECTION_STATUS ResultFlask;

command get_command(uint8_t rxData[]) {
	/*
	 * command structure
	 *    "$<type>:<command>:<value>:<value2>:<value3>;"
	 */

	command Command;
	char type[RX_SIZE], command[RX_SIZE], value[RX_SIZE], value2[RX_SIZE], value3[RX_SIZE];
	strcpy(type, "");
	strcpy(command, "");
	strcpy(value, "0");
	strcpy(value2, "0");
	strcpy(value3, "0");

	// remove first and last characters ('$' and ';')
	char *temp = rxData+1;
	temp[strlen(temp) - 1] = '\0';

    char *p_rxData = strtok(temp, ":");

    // split rxData
    int cnt = 0;
    while (p_rxData != NULL)
    {
        if (cnt == 0) {
            strcpy(type, p_rxData);
        }
        else if (cnt == 1) {
            strcpy(command, p_rxData);
        }
        else if (cnt == 2) {
            strcpy(value, p_rxData);
        }
        else if (cnt == 3) {
            strcpy(value2, p_rxData);
        }
        else if (cnt == 4) {
            strcpy(value3, p_rxData);
        }
        else {
            /* empty */
        }
        p_rxData = strtok(NULL, ":");
        cnt++;
    }

	Command.type = 99;
	Command.command = 99;
	Command.value = atof(value);
    Command.value2 = atof(value2);
    Command.value3 = atof(value3);

	if (strcmp(type, "LED") == 0) {
		Command.type = LED;
		// ------------------------------------------//
		if (strcmp(command, "LED_GREEN_ON") == 0) Command.command = LED_GREEN_ON;
		else if (strcmp(command, "LED_GREEN_OFF") == 0) Command.command = LED_GREEN_OFF;
		else if (strcmp(command, "LED_GREEN_BLINK") == 0) Command.command = LED_GREEN_BLINK;
		// ------------------------------------------//
		else if (strcmp(command, "LED_YELLOW_ON") == 0) Command.command = LED_YELLOW_ON;
		else if (strcmp(command, "LED_YELLOW_OFF") == 0) Command.command = LED_YELLOW_OFF;
		else if (strcmp(command, "LED_YELLOW_BLINK") == 0) Command.command = LED_YELLOW_BLINK;
		// ------------------------------------------//
		else if (strcmp(command, "LED_RED_ON") == 0) Command.command = LED_RED_ON;
		else if (strcmp(command, "LED_RED_OFF") == 0) Command.command = LED_RED_OFF;
		else if (strcmp(command, "LED_RED_BLINK") == 0) Command.command = LED_RED_BLINK;
		// ------------------------------------------//
		else if (strcmp(command, "LED_PANEL_ON") == 0) Command.command = LED_PANEL_ON;
		else if (strcmp(command, "LED_PANEL_OFF") == 0) Command.command = LED_PANEL_OFF;
		// ------------------------------------------//
	}
	else if (strcmp(type, "BUZZER") == 0) {
		Command.type = BUZZER;
		// ------------------------------------------//
		if (strcmp(command, "BUZZER_ON") == 0) Command.command = BUZZER_ON;
		else if (strcmp(command, "BUZZER_OFF") == 0) Command.command = BUZZER_OFF;
		else if (strcmp(command, "BUZZER_SET") == 0) Command.command = BUZZER_SET;
		// ------------------------------------------//
	}
	else if (strcmp(type, "LATCH") == 0) {
		Command.type = LATCH;
		// ------------------------------------------//
		if (strcmp(command, "LATCH_ON") == 0) Command.command = LATCH_ON;
		else if (strcmp(command, "LATCH_OFF") == 0) Command.command = LATCH_OFF;
		// ------------------------------------------//
	}
	else if (strcmp(type, "MOTOR") == 0) {
		Command.type = MOTOR;
		// ------------------------------------------//
		if (strcmp(command, "MOTOR_C_FRWD_DEG") == 0) Command.command = MOTOR_C_FRWD_DEG;
		else if (strcmp(command, "MOTOR_C_SETPOINT") == 0) Command.command = MOTOR_C_SETPOINT;
		else if (strcmp(command, "MOTOR_C_SET_MAX_VEL") == 0) Command.command = MOTOR_C_SET_MAX_VEL;
		else if (strcmp(command, "MOTOR_C_SET_MAX_ACC") == 0) Command.command = MOTOR_C_SET_MAX_ACC;
		else if (strcmp(command, "MOTOR_C_PAUSE") == 0) Command.command = MOTOR_C_PAUSE;
		else if (strcmp(command, "MOTOR_C_STOP_HOME") == 0) Command.command = MOTOR_C_STOP_HOME;
		else if (strcmp(command, "MOTOR_C_SET_KP") == 0) Command.command = MOTOR_C_SET_KP;
		else if (strcmp(command, "MOTOR_C_SET_KI") == 0) Command.command = MOTOR_C_SET_KI;
		else if (strcmp(command, "MOTOR_C_SET_KD") == 0) Command.command = MOTOR_C_SET_KD;
		else if (strcmp(command, "MOTOR_C_SET_PWM") == 0) Command.command = MOTOR_C_SET_PWM;

		else if (strcmp(command, "MOTOR_T_SETPOINT") == 0) Command.command = MOTOR_T_SETPOINT;
		else if (strcmp(command, "MOTOR_T_FRWD_DEG") == 0) Command.command = MOTOR_T_FRWD_DEG;
		else if (strcmp(command, "MOTOR_T_SET_MAX_VEL") == 0) Command.command = MOTOR_T_SET_MAX_VEL;
		else if (strcmp(command, "MOTOR_T_SET_MAX_ACC") == 0) Command.command = MOTOR_T_SET_MAX_ACC;
		else if (strcmp(command, "MOTOR_T_PAUSE") == 0) Command.command = MOTOR_T_PAUSE;
		else if (strcmp(command, "MOTOR_T_STOP_HOME") == 0) Command.command = MOTOR_T_STOP_HOME;
		else if (strcmp(command, "MOTOR_T_SET_KP") == 0) Command.command = MOTOR_T_SET_KP;
		else if (strcmp(command, "MOTOR_T_SET_KI") == 0) Command.command = MOTOR_T_SET_KI;
		else if (strcmp(command, "MOTOR_T_SET_KD") == 0) Command.command = MOTOR_T_SET_KD;
		else if (strcmp(command, "MOTOR_T_SET_PWM") == 0) Command.command = MOTOR_T_SET_PWM;

		else if (strcmp(command, "MOTOR_Z_SETPOINT") == 0) Command.command = MOTOR_Z_SETPOINT;
		else if (strcmp(command, "MOTOR_Z_FRWD_MM") == 0) Command.command = MOTOR_Z_FRWD_MM;
		else if (strcmp(command, "MOTOR_Z_SET_MAX_VEL") == 0) Command.command = MOTOR_Z_SET_MAX_VEL;
		else if (strcmp(command, "MOTOR_Z_SET_MAX_ACC") == 0) Command.command = MOTOR_Z_SET_MAX_ACC;
		else if (strcmp(command, "MOTOR_Z_PAUSE") == 0) Command.command = MOTOR_Z_PAUSE;
		else if (strcmp(command, "MOTOR_Z_STOP_HOME") == 0) Command.command = MOTOR_Z_STOP_HOME;
		else if (strcmp(command, "MOTOR_Z_SET_KP") == 0) Command.command = MOTOR_Z_SET_KP;
		else if (strcmp(command, "MOTOR_Z_SET_KI") == 0) Command.command = MOTOR_Z_SET_KI;
		else if (strcmp(command, "MOTOR_Z_SET_KD") == 0) Command.command = MOTOR_Z_SET_KD;
		else if (strcmp(command, "MOTOR_Z_SET_PWM") == 0) Command.command = MOTOR_Z_SET_PWM;

		else if (strcmp(command, "MOTOR_ALL_STOP_HOME") == 0) Command.command = MOTOR_ALL_STOP_HOME;
	}
	else if (strcmp(type, "VALVE") == 0) {
		Command.type = VALVE;
		// ------------------------------------------//
		if (strcmp(command, "VALVE_ON") == 0) Command.command = VALVE_ON;
		else if (strcmp(command, "VALVE_OFF") == 0) Command.command = VALVE_OFF;
		// ------------------------------------------//
	}
	else if (strcmp(type, "SYSTEM") == 0) {
		Command.type = SYSTEM;
		// ------------------------------------------//
		if (strcmp(command, "SOFT_RESET") == 0) Command.command = SOFT_RESET;
		else if (strcmp(command, "TARE_LOADCELL") == 0) Command.command = TARE_LOADCELL;
		else if (strcmp(command, "SET_VOLUME") == 0) Command.command = SET_VOLUME;
		else if (strcmp(command, "EMERGENCY_STOP") == 0) Command.command = EMERGENCY_STOP;
		else if (strcmp(command, "CHECK_CRUCIBLE") == 0) Command.command = CHECK_CRUCIBLE;
		// ------------------------------------------//
	}
	else if (strcmp(type, "HEATERS") == 0) {
		Command.type = HEATERS;
		// ------------------------------------------//
		if (strcmp(command, "HEATERS_SET") == 0) Command.command = HEATERS_SET;
		else if (strcmp(command, "HEATERS_ON") == 0) Command.command = HEATERS_ON;
		else if (strcmp(command, "HEATERS_OFF") == 0) Command.command = HEATERS_OFF;
		else if (strcmp(command, "HEATERS_CONTROL_ON") == 0) Command.command = HEATERS_CONTROL_ON;
		else if (strcmp(command, "HEATERS_CONTROL_OFF") == 0) Command.command = HEATERS_CONTROL_OFF;
		// ------------------------------------------//
	}
	else if (strcmp(type, "PROCESS") == 0) {
		Command.type = PROCESS;
		// ------------------------------------------//
		if (strcmp(command, "START_HOMING") == 0) Command.command = START_HOMING;
		else if (strcmp(command, "START_POURING") == 0) Command.command = START_POURING;
		else if (strcmp(command, "START_VOLUME_DETECTION") == 0) Command.command = START_VOLUME_DETECTION;
		else if (strcmp(command, "START_DEGASSING") == 0) Command.command = START_DEGASSING;
		else if (strcmp(command, "START_ADDING") == 0) Command.command = START_ADDING;
		else if (strcmp(command, "START_NOZZLE_CHANGE") == 0) Command.command = START_NOZZLE_CHANGE;
		else if (strcmp(command, "NOZZLE_CHANGE_DONE") == 0) Command.command = NOZZLE_CHANGE_DONE;
		else if (strcmp(command, "START_EMPTYING") == 0) Command.command = START_EMPTYING;
		else if (strcmp(command, "EMPTYING_DONE") == 0) Command.command = EMPTYING_DONE;
		else if (strcmp(command, "START_CRUCIBLE_CHANGE") == 0) Command.command = START_CRUCIBLE_CHANGE;
		else if (strcmp(command, "START_COVER_CRUCIBLE") == 0) Command.command = START_COVER_CRUCIBLE;
		else if (strcmp(command, "START_FLASK_STATUS") == 0) Command.command = START_FLASK_STATUS;
		else if (strcmp(command, "STOP_PROCESS") == 0) Command.command = STOP_PROCESS;
		// ------------------------------------------//
	}


	if ((Command.type != 99) && (Command.command != 99)) {

		/* send ack */
		char tmp[RX_SIZE];

		if (Command.value == 0 && Command.value2 == 0 && Command.value3 == 0) {
			sprintf(tmp, "%s:%s", type, command);
		}
		else if (Command.value2 == 0 && Command.value3 == 0) {
			sprintf(tmp, "%s:%s:%lf", type, command, Command.value);
		}
		else if (Command.value3 == 0) {
			sprintf(tmp, "%s:%s:%lf:%lf", type, command, Command.value, Command.value2);
		}
		else {
			sprintf(tmp, "%s:%s:%lf:%lf:%lf", type, command, Command.value, Command.value2, Command.value3);
		}
		send_acknowledge(sendQueueHandle, tmp);
	}
	else {
		/* raise error - wrong command type */
		Command.type = 99;
		Command.command = 99;
	}

	return Command;
};

void execute_command_from_value(uint8_t type_v, uint8_t command_v, float value_v){
	command c;
	c.type = type_v;
	c.command = command_v;
	c.value = value_v;
	execute_command(&c);
}

void execute_command_from_values(uint8_t type_v, uint8_t command_v, float value_v, float value2_v){
	command c;
	c.type = type_v;
	c.command = command_v;
	c.value = value_v;
	c.value2 = value2_v;
	execute_command(&c);
}

void write_state(command* Command){
	osMessageQueuePut(commandsExecuteQueueHandle, Command, 0, 100);
}

void execute_command(command *Command) {
	switch(Command->type)
	{
		case LED:
			switch(Command->command)
			{
				case LED_GREEN_ON:
					HAL_GPIO_WritePin(LED_GREEN, ON);
					break;
				case LED_GREEN_OFF:
					HAL_GPIO_WritePin(LED_GREEN, OFF);
					led_green_blink = OFF;
					break;
				case LED_GREEN_BLINK:
					led_green_blink = ON;
					break;
				case LED_YELLOW_ON:
					HAL_GPIO_WritePin(LED_YELLOW, ON);
					break;
				case LED_YELLOW_OFF:
					HAL_GPIO_WritePin(LED_YELLOW, OFF);
					led_yellow_blink = OFF;
					break;
				case LED_YELLOW_BLINK:
					led_yellow_blink = ON;
					break;
				case LED_RED_ON:
					HAL_GPIO_WritePin(LED_RED, ON);
					break;
				case LED_RED_OFF:
					HAL_GPIO_WritePin(LED_RED, OFF);
					led_red_blink = OFF;
					break;
				case LED_RED_BLINK:
					led_red_blink = ON;
					break;
				case LED_PANEL_ON:
					HAL_GPIO_WritePin(LED_PANEL, ON);
					break;
				case LED_PANEL_OFF:
					HAL_GPIO_WritePin(LED_PANEL, OFF);
					break;
				default:
					/* wrong command */
					break;
			}
			break;

		case HEATERS:
			switch (Command->command)
			{
				case HEATERS_ON:
					HAL_GPIO_WritePin(HEATER_PIN, ON);
					break;
				case HEATERS_OFF:
					HAL_GPIO_WritePin(HEATER_PIN, OFF);
					break;
				case HEATERS_SET:
					temp_target = Command->value;
					is_target_temp_achieved = false;
					break;
				case HEATERS_CONTROL_ON:
					temperature_control_enabled = true;
					break;
				case HEATERS_CONTROL_OFF:
					temperature_control_enabled = false;
					HAL_GPIO_WritePin(HEATER_PIN, OFF);
					break;
				default:
					/* wrong command */
					break;
			}
			break;

		case LATCH:
			switch (Command->command)
			{
				case LATCH_ON:
					/* test code */
					TIM8->CCR1 = 160; /* 0 - 255 */
					HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_1);
					break;
				case LATCH_OFF:
					HAL_TIM_PWM_Stop(&htim8, TIM_CHANNEL_1);
					break;
				default:
					/* wrong command */
					break;
			}
			break;

		case BUZZER:
			switch (Command->command)
			{
				case BUZZER_ON:
					/* test code */
					TIM8->CCR2 = 160; /* 0 - 255 */
					HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_2);
					break;
				case BUZZER_OFF:
					HAL_TIM_PWM_Stop(&htim8, TIM_CHANNEL_2);
					break;
				case BUZZER_SET:
					/* code to alter the PWM of the buzzer */
					TIM8->CCR2 = (uint8_t) Command->value; /* 0 - 255 */
					HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_2);
					break;
				default:
					/* wrong command */
					break;
			}
			break;

		case MOTOR:
			switch (Command->command)
			{
			case MOTOR_C_SETPOINT:
				motor_C.PID.setpoint = MOTOR_C_FRWD_FACTOR * encoder_convert_from_unit(Command->value, motor_C.unit, motor_C.config.GEARBOX_RATIO);
				break;
			case MOTOR_C_FRWD_DEG:
				if (ENABLE_MOTOR_LIMITS && current_process.current_homing_subprocess != HOME_C) {
					if (Command->value >= MOTOR_C_LIM_MAX) {
						Command->value = MOTOR_C_LIM_MAX;
					}
					else if (Command->value <= MOTOR_C_LIM_MIN) {
						Command->value = MOTOR_C_LIM_MIN;
					}
				}
				motor_C.motion.target = MOTOR_C_FRWD_FACTOR * encoder_convert_from_unit(Command->value, motor_C.unit, motor_C.config.GEARBOX_RATIO);
				break;
			case MOTOR_C_SET_MAX_VEL:
				setMaxVelocity(&motor_C.motion, Command->value);
				break;
			case MOTOR_C_SET_MAX_ACC:
				setMaxAcceleration(&motor_C.motion, Command->value);
				break;
			case MOTOR_C_PAUSE:
				motor_C.motion.target=motor_C.PID.input;
				break;
			case MOTOR_C_STOP_HOME:
				disable_PWM(&motor_C);

				setInitPosition(&motor_C.motion, motor_C.initPos);
				motion_reset(&motor_C.motion);

				encoder_reset(ENCODER_C); //pid reset
				motor_PID_Reset(&motor_C);

				enable_PWM(&motor_C);

				break;
			case MOTOR_C_SET_KP:
				motor_C.PID.Kp=Command->value;
				break;
			case MOTOR_C_SET_KI:
				motor_C.PID.Ki=Command->value;
				break;
			case MOTOR_C_SET_KD:
				motor_C.PID.Kd=Command->value;
				break;
			case MOTOR_C_SET_PWM:
				motor_C.PID.output=Command->value;
				break;
			case MOTOR_T_SETPOINT:
				motor_T.PID.setpoint = MOTOR_T_FRWD_FACTOR * encoder_convert_from_unit(Command->value, motor_T.unit, motor_T.config.GEARBOX_RATIO);
				break;
			case MOTOR_T_FRWD_DEG:
				if (ENABLE_MOTOR_LIMITS && current_process.current_homing_subprocess != HOME_T) {
					if (Command->value >= MOTOR_T_LIM_MAX) {
						Command->value = MOTOR_T_LIM_MAX;
					}
					else if (Command->value <= MOTOR_T_LIM_MIN) {
						Command->value = MOTOR_T_LIM_MIN;
					}
				}
				motor_T.motion.target = MOTOR_T_FRWD_FACTOR * encoder_convert_from_unit(Command->value, motor_T.unit, motor_T.config.GEARBOX_RATIO);
				break;
			case MOTOR_T_SET_MAX_VEL:
				setMaxVelocity(&motor_T.motion, Command->value);
				break;
			case MOTOR_T_SET_MAX_ACC:
				setMaxAcceleration(&motor_T.motion, Command->value);
				break;
			case MOTOR_T_PAUSE:
				motor_T.motion.target=motor_T.PID.input;
				break;
			case MOTOR_T_STOP_HOME:
				disable_PWM(&motor_T);

				setInitPosition(&motor_T.motion, motor_T.initPos);
				motion_reset(&motor_T.motion);

				encoder_reset(ENCODER_T); //pid reset
				motor_PID_Reset(&motor_T);

				enable_PWM(&motor_T);

				break;
			case MOTOR_T_SET_KP:
				motor_T.PID.Kp=Command->value;
				break;
			case MOTOR_T_SET_KI:
				motor_T.PID.Ki=Command->value;
				break;
			case MOTOR_T_SET_KD:
				motor_T.PID.Kd=Command->value;
				break;
			case MOTOR_T_SET_PWM:
				motor_T.PID.output=Command->value;
				break;
			case MOTOR_Z_SETPOINT:
				motor_Z.PID.setpoint = MOTOR_Z_FRWD_FACTOR * encoder_convert_from_unit(Command->value, motor_Z.unit, motor_Z.config.GEARBOX_RATIO);
				break;
			case MOTOR_Z_FRWD_MM:
				if (ENABLE_MOTOR_LIMITS && current_process.current_homing_subprocess != HOME_Z) {
					if (Command->value >= MOTOR_Z_LIM_MAX) {
						Command->value = MOTOR_Z_LIM_MAX;
					}
					else if (Command->value <= MOTOR_Z_LIM_MIN) {
						Command->value = MOTOR_Z_LIM_MIN;
					}
				}
				motor_Z.motion.target = MOTOR_Z_FRWD_FACTOR * encoder_convert_from_unit(Command->value, motor_Z.unit, motor_Z.config.GEARBOX_RATIO);
				break;
			case MOTOR_Z_SET_MAX_VEL:
				setMaxVelocity(&motor_Z.motion, Command->value);
				break;
			case MOTOR_Z_SET_MAX_ACC:
				setMaxAcceleration(&motor_Z.motion, Command->value);
				break;
			case MOTOR_Z_PAUSE:
				motor_Z.motion.target=motor_Z.PID.input;
				break;
			case MOTOR_Z_STOP_HOME:
				disable_PWM(&motor_Z);

				setInitPosition(&motor_Z.motion, motor_Z.initPos);
				motion_reset(&motor_Z.motion);

				encoder_reset(ENCODER_Z); //pid reset
				motor_PID_Reset(&motor_Z);

				enable_PWM(&motor_Z);

				break;
			case MOTOR_Z_SET_KP:
				motor_Z.PID.Kp=Command->value;
				break;
			case MOTOR_Z_SET_KI:
				motor_Z.PID.Ki=Command->value;
				break;
			case MOTOR_Z_SET_KD:
				motor_Z.PID.Kd=Command->value;
				break;
			case MOTOR_Z_SET_PWM:
				motor_Z.PID.output=Command->value;
				break;

			case MOTOR_ALL_STOP_HOME:
				execute_command_from_value(MOTOR, MOTOR_C_STOP_HOME,0);
				execute_command_from_value(MOTOR, MOTOR_T_STOP_HOME,0);
				execute_command_from_value(MOTOR, MOTOR_Z_STOP_HOME,0);
				break;

			default:
				/* wrong command */
				break;
			}
			break;

		case VALVE:
			switch (Command->command)
			{
			case VALVE_ON:
				HAL_GPIO_WritePin(VALVE_PIN, ON);
				break;
			case VALVE_OFF:
				HAL_GPIO_WritePin(VALVE_PIN, OFF);
				break;
			default:
				/* wrong command */
				break;
			}
			break;

		case SYSTEM:
			switch (Command->command)
			{
			case SOFT_RESET:
				HAL_NVIC_SystemReset();
				break;
			case TARE_LOADCELL:
				hx711_tare(&Hx711_, 10);
				break;
			case SET_VOLUME:
				setCrucibleVolume(&Foundry, Command->value);
				break;
			case EMERGENCY_STOP:

				releaseProcess(&current_process);
				execute_command_from_value(MOTOR, MOTOR_ALL_STOP_HOME, 0);
				execute_command_from_value(VALVE, VALVE_OFF, 0);

				temperature_control_enabled = false;
				HAL_GPIO_WritePin(HEATER_PIN, OFF);

				execute_command_from_value(LED, LED_PANEL_ON, 0);
				execute_command_from_value(LED, LED_RED_BLINK, 0);

				break;
			case CHECK_CRUCIBLE:
				send_acknowledge(sendQueueHandle, "CHECK_CRUCIBLE_DONE:1");
				break;
			default:
				/* wrong command */
				break;
			}
			break;

		case PROCESS:
			switch (Command->command)
			{
			case START_HOMING:
				setProcess(HOMING);
				break;
			case START_POURING:
				/* set pouring parameters, respectively Q, Tr, Tst */
				setPouringParameters(&pouringControl, Command->value, Command->value2, Command->value3);
				setProcess(POURING);
				break;
			case START_VOLUME_DETECTION:
				setProcess(VOLUME_DETECTION);
				break;
			case START_DEGASSING:
				setProcess(DEGASSING);
				current_process.t_degassing_duration = Command->value;
				break;
			case START_ADDING:
				setProcess(ADDING);
				break;
			case START_NOZZLE_CHANGE:
				setProcess(NOZZLE_CHANGE);
				break;
			case NOZZLE_CHANGE_DONE:
				current_process.flag.nozzle_change_done = true;
				break;
			case START_EMPTYING:
				setProcess(EMPTYING);
				break;
			case EMPTYING_DONE:
				current_process.flag.emptying_done = true;
				break;
			case START_CRUCIBLE_CHANGE:
				setProcess(CRUCIBLE_CHANGE);
				break;
			case START_COVER_CRUCIBLE:
				setProcess(COVER_CRUCIBLE);
				break;
			case STOP_PROCESS:
				send_value(sendQueueHandle, current_process.current_homing_subprocess);
				if (current_process.current != HOMING) {
					releaseProcess(&current_process);
					// stop motor movements
					motor_Z.motion.target = motor_Z.PID.input;
					motor_T.motion.target = motor_T.PID.input;
					motor_C.motion.target = motor_C.PID.input;

					motor_Z.state = MOTOR_IS_OFF;
					motor_T.state = MOTOR_IS_OFF;
					motor_C.state = MOTOR_IS_OFF;

					osDelay(1000);

					// reset the queue, preventing some other queued up processes
					osMessageQueueReset(processQueueHandle);
					osDelay(1000);

					// queue up homing and cover crucible
					setProcess(HOMING);

					HAL_GPIO_WritePin(VALVE_PIN, OFF);
					HAL_GPIO_WritePin(HEATER_PIN, OFF);
					osDelay(VALVE_TIMEOUT_T);

					setProcess(COVER_CRUCIBLE);
				}
				else {
					send_error(sendQueueHandle, "CAN_NOT_STOP_HOMING");
				}
				break;
			case START_FLASK_STATUS:
				ResultFlask = CheckFlask();

				if (InitStatuts ==1 )
				{
					send_acknowledge(sendQueueHandle, "INIT_DONE");
				}
				else if (InitStatuts == 2)
				{
					send_acknowledge(sendQueueHandle, "INIT_ERROR");
				}

				switch(ResultFlask)
				{
				case FLASK_DETECTED :
					send_acknowledge(sendQueueHandle, "FLASK_DETECTED");
				break;
				case FLASK_NOT_DETECTED:
					send_acknowledge(sendQueueHandle, "FLASK_NOT_DETECTED");
				break;
				case SENSOR_ERROR :
					send_acknowledge(sendQueueHandle, "SENSOR_ERROR");
				break;
						}
						break;
			default:
				/* SHOULD NOT END UP HERE */
//				setProcess(IDLE);
				/* wrong command */
				send_error(sendQueueHandle, "WRONG_PROCESS_CMD");
				break;
			}
			break;

		default:
			/* wrong type of command */
			break;
	}
}

void led_blink() {
	command Command;
	Command.type = LED;

	if (led_green_blink) {
		if (temp_green_blink_state == ON) {
			Command.command = LED_GREEN_OFF;
			execute_command(&Command);
		    temp_green_blink_state = OFF;
		    led_green_blink = ON;
		}
		else {
			Command.command = LED_GREEN_ON;
			execute_command(&Command);
		    temp_green_blink_state = ON;
		}
	}
	if (led_yellow_blink) {
	  	if (temp_yellow_blink_state == ON) {
	  		Command.command = LED_YELLOW_OFF;
	  		execute_command(&Command);
	  		temp_yellow_blink_state = OFF;
	  		led_yellow_blink = ON;
	  	}
	  	else {
	  		Command.command = LED_YELLOW_ON;
	  		execute_command(&Command);
	  		temp_yellow_blink_state = ON;
	  	}
	}
	if (led_red_blink) {
		  if (temp_red_blink_state == ON) {
		  	Command.command = LED_RED_OFF;
		  	execute_command(&Command);
		  	temp_red_blink_state = OFF;
		  	led_red_blink = ON;
		  }
		  else {
		  	Command.command = LED_RED_ON;
		  	execute_command(&Command);
		  	temp_red_blink_state = ON;
		  }
	}
}
