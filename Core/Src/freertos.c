/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * File Name          : freertos.c
  * Description        : Code for freertos applications
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "FreeRTOS.h"
#include "task.h"
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "commandHandler.h"
#include "sendData.h"
#include "sensorRead.h"
#include "motorHandler.h"
#include "encoders.h"
#include "motionGenerator.h"
#include "endStops.h"
#include "pouringControl.h"


/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
typedef StaticQueue_t osStaticMessageQDef_t;
typedef StaticSemaphore_t osStaticSemaphoreDef_t;
/* USER CODE BEGIN PTD */
typedef struct {
	uint8_t msg[MSG_SIZE];
} queue_data;


queue_data queueData;
command Command;

sensors Sensors;
foundry Foundry;
hx711_t Hx711_;

Motor_Handle motor_C, motor_T, motor_Z;
pouringControl_HandleTypeDef pouringControl;
end_stops endStops;
Process current_process;

uint8_t handshake_cmd[] = HANDSHAKE_TYPE_COMMAND;
uint8_t handshake = 0;



float temp_target;
int is_target_temp_achieved=0;
extern int material_count;
float temp_crucible=0;

bool temperature_control_enabled = false;
bool init_freertos_done = false;
//init_freertos_done is a flag used to make sure the firmware doesn't attempt to read the sensor data
//while the initialisation is happening. this causes two different tasks to attempt to get data from
//the hx711 module, which prevents the sensor data from being read properly.
bool stopActivated=false;

bool goingForwardC = false;
bool goingForwardT = false;
bool goingForwardZ = false;

bool correct_setup_C=false;
bool correct_setup_T=false;
bool correct_setup_Z=false;

bool homming_was_done=false;

//uint8_t local_rxData[RX_SIZE];

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN Variables */

/* USER CODE END Variables */
/* Definitions for commandHandler */
osThreadId_t commandHandlerHandle;
const osThreadAttr_t commandHandler_attributes = {
  .name = "commandHandler",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityLow2,
};
/* Definitions for sendData */
osThreadId_t sendDataHandle;
const osThreadAttr_t sendData_attributes = {
  .name = "sendData",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityLow,
};
/* Definitions for sensorRead */
osThreadId_t sensorReadHandle;
const osThreadAttr_t sensorRead_attributes = {
  .name = "sensorRead",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* Definitions for controlTemperat */
osThreadId_t controlTemperatHandle;
const osThreadAttr_t controlTemperat_attributes = {
  .name = "controlTemperat",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityLow1,
};
/* Definitions for motorControl */
osThreadId_t motorControlHandle;
const osThreadAttr_t motorControl_attributes = {
  .name = "motorControl",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityRealtime7,
};
/* Definitions for motionControl */
osThreadId_t motionControlHandle;
const osThreadAttr_t motionControl_attributes = {
  .name = "motionControl",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityRealtime,
};
/* Definitions for processHandler */
osThreadId_t processHandlerHandle;
const osThreadAttr_t processHandler_attributes = {
  .name = "processHandler",
  .stack_size = 256 * 4,
  .priority = (osPriority_t) osPriorityNormal1,
};
/* Definitions for sendQueue */
osMessageQueueId_t sendQueueHandle;
const osMessageQueueAttr_t sendQueue_attributes = {
  .name = "sendQueue"
};
/* Definitions for commandsRxQueue */
osMessageQueueId_t commandsRxQueueHandle;
uint8_t commandsRxQueueBuffer[ 10 * sizeof( queue_commandRx ) ];
osStaticMessageQDef_t commandsRxQueueControlBlock;
const osMessageQueueAttr_t commandsRxQueue_attributes = {
  .name = "commandsRxQueue",
  .cb_mem = &commandsRxQueueControlBlock,
  .cb_size = sizeof(commandsRxQueueControlBlock),
  .mq_mem = &commandsRxQueueBuffer,
  .mq_size = sizeof(commandsRxQueueBuffer)
};
/* Definitions for commandsExecuteQueue */
osMessageQueueId_t commandsExecuteQueueHandle;
uint8_t commandsExecuteQueueBuffer[ 16 * sizeof( command ) ];
osStaticMessageQDef_t commandsExecuteQueueControlBlock;
const osMessageQueueAttr_t commandsExecuteQueue_attributes = {
  .name = "commandsExecuteQueue",
  .cb_mem = &commandsExecuteQueueControlBlock,
  .cb_size = sizeof(commandsExecuteQueueControlBlock),
  .mq_mem = &commandsExecuteQueueBuffer,
  .mq_size = sizeof(commandsExecuteQueueBuffer)
};
/* Definitions for processQueue */
osMessageQueueId_t processQueueHandle;
const osMessageQueueAttr_t processQueue_attributes = {
  .name = "processQueue"
};
/* Definitions for sTIM_sensorRead */
osTimerId_t sTIM_sensorReadHandle;
const osTimerAttr_t sTIM_sensorRead_attributes = {
  .name = "sTIM_sensorRead"
};
/* Definitions for sTIM_controlTemperature */
osTimerId_t sTIM_controlTemperatureHandle;
const osTimerAttr_t sTIM_controlTemperature_attributes = {
  .name = "sTIM_controlTemperature"
};
/* Definitions for sTIM_motorPID */
osTimerId_t sTIM_motorPIDHandle;
const osTimerAttr_t sTIM_motorPID_attributes = {
  .name = "sTIM_motorPID"
};
/* Definitions for sTIM_processHandler */
osTimerId_t sTIM_processHandlerHandle;
const osTimerAttr_t sTIM_processHandler_attributes = {
  .name = "sTIM_processHandler"
};
/* Definitions for sTIM_motionControl */
osTimerId_t sTIM_motionControlHandle;
const osTimerAttr_t sTIM_motionControl_attributes = {
  .name = "sTIM_motionControl"
};
/* Definitions for commandBuffer */
osSemaphoreId_t commandBufferHandle;
osStaticSemaphoreDef_t commandBufferControlBlock;
const osSemaphoreAttr_t commandBuffer_attributes = {
  .name = "commandBuffer",
  .cb_mem = &commandBufferControlBlock,
  .cb_size = sizeof(commandBufferControlBlock),
};

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN FunctionPrototypes */

/* USER CODE END FunctionPrototypes */

void StartCommandHandler(void *argument);
void StartSendData(void *argument);
void StartSensorRead(void *argument);
void StartControlTemperature(void *argument);
void StartMotorControl(void *argument);
void StartMotionControl(void *argument);
void StartProcessHandler(void *argument);
void sTIM_sensorRead_Callback(void *argument);
void sTIM_controlTemperatureHandle_Callback(void *argument);
void sTIM_motorPID_Callback(void *argument);
void sTIM_processHandler_Callback(void *argument);
void sTIM_motionControl_Callback(void *argument);

void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */

/**
  * @brief  FreeRTOS initialization
  * @param  None
  * @retval None
  */
void MX_FREERTOS_Init(void) {
  /* USER CODE BEGIN Init */
	foundry_init(&Foundry);
  /* USER CODE END Init */

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* Create the semaphores(s) */
  /* creation of commandBuffer */
  commandBufferHandle = osSemaphoreNew(1, 1, &commandBuffer_attributes);

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* Create the timer(s) */
  /* creation of sTIM_sensorRead */
  sTIM_sensorReadHandle = osTimerNew(sTIM_sensorRead_Callback, osTimerPeriodic, NULL, &sTIM_sensorRead_attributes);

  /* creation of sTIM_controlTemperature */
  sTIM_controlTemperatureHandle = osTimerNew(sTIM_controlTemperatureHandle_Callback, osTimerPeriodic, NULL, &sTIM_controlTemperature_attributes);

  /* creation of sTIM_motorPID */
  sTIM_motorPIDHandle = osTimerNew(sTIM_motorPID_Callback, osTimerPeriodic, NULL, &sTIM_motorPID_attributes);

  /* creation of sTIM_processHandler */
  sTIM_processHandlerHandle = osTimerNew(sTIM_processHandler_Callback, osTimerPeriodic, NULL, &sTIM_processHandler_attributes);

  /* creation of sTIM_motionControl */
  sTIM_motionControlHandle = osTimerNew(sTIM_motionControl_Callback, osTimerPeriodic, NULL, &sTIM_motionControl_attributes);

  /* USER CODE BEGIN RTOS_TIMERS */

  /* USER CODE END RTOS_TIMERS */

  /* Create the queue(s) */
  /* creation of sendQueue */
  sendQueueHandle = osMessageQueueNew (16, sizeof(queueData), &sendQueue_attributes);

  /* creation of commandsRxQueue */
  commandsRxQueueHandle = osMessageQueueNew (10, sizeof(queue_commandRx), &commandsRxQueue_attributes);

  /* creation of commandsExecuteQueue */
  commandsExecuteQueueHandle = osMessageQueueNew (16, sizeof(command), &commandsExecuteQueue_attributes);

  /* creation of processQueue */
  processQueueHandle = osMessageQueueNew (5, sizeof(Process), &processQueue_attributes);

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of commandHandler */
  commandHandlerHandle = osThreadNew(StartCommandHandler, NULL, &commandHandler_attributes);

  /* creation of sendData */
  sendDataHandle = osThreadNew(StartSendData, NULL, &sendData_attributes);

  /* creation of sensorRead */
  sensorReadHandle = osThreadNew(StartSensorRead, NULL, &sensorRead_attributes);

  /* creation of controlTemperat */
  controlTemperatHandle = osThreadNew(StartControlTemperature, NULL, &controlTemperat_attributes);

  /* creation of motorControl */
  motorControlHandle = osThreadNew(StartMotorControl, NULL, &motorControl_attributes);

  /* creation of motionControl */
  motionControlHandle = osThreadNew(StartMotionControl, NULL, &motionControl_attributes);

  /* creation of processHandler */
  processHandlerHandle = osThreadNew(StartProcessHandler, NULL, &processHandler_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */

  if (ENABLE_SENSOR_INIT_MESSAGES) {
	  HAL_UART_Transmit(&huart3, "init_started\n\r", strlen("init_started_encoders\n\r"), 200);
  }

  encoders_init();

  if (ENABLE_SENSOR_INIT_MESSAGES) {
	  HAL_UART_Transmit(&huart3, "encoders_init_done\n\r", strlen("encoders_init_done\n\r"), 200);
  }

  motor_Init(&motor_C, MOTOR_C, ENCODER_C, DEG);
  motor_Init(&motor_T, MOTOR_T, ENCODER_T, DEG);
  motor_Init(&motor_Z, MOTOR_Z, ENCODER_Z, MM);

  if (ENABLE_SENSOR_INIT_MESSAGES) {
	  HAL_UART_Transmit(&huart3, "motor_init_done\n\r", strlen("motor_init_done\n\r"), 200);
  }

  pouringControl = pouringControl_Init();

  if (ENABLE_SENSOR_INIT_MESSAGES) {
	  HAL_UART_Transmit(&huart3, "pouring_init_done\n\r", strlen("pouring_init_done\n\r"), 200);
  }

  Sensors = sensors_init();

  if (ENABLE_SENSOR_INIT_MESSAGES) {
	  HAL_UART_Transmit(&huart3, "sensors_init_done\n\r", strlen("sensors_init_done\n\r"), 200);
  }


  if (ENABLE_SENSOR_INIT_MESSAGES) {
	  HAL_UART_Transmit(&huart3, "loadcells_init\n\r", strlen("loadcells_init\n\r"), 200);
  }

//  hx711_init(&Hx711_, HX711_CLK_GPIO_Port, HX711_CLK_Pin, HX711_DATA_GPIO_Port, HX711_DATA_Pin);
//  hx711_coef_set(&Hx711_, 28.7); 	/* calibration */
//  hx711_tare(&Hx711_, 10);			/* tare function */
//
//  if (ENABLE_SENSOR_INIT_MESSAGES) {
//	  HAL_UART_Transmit(&huart3, "loadcells_init_done\n\r", strlen("loadcells_init_done\n\r"), 200);
//  }

  initStateInterrupts(&endStops);

  if (ENABLE_SENSOR_INIT_MESSAGES) {
	  HAL_UART_Transmit(&huart3, "endstops_init_done\n\r", strlen("endstops_init_done\n\r"), 200);
  }

  updateStateInterrupts(&endStops); // this is necessary, because if the endstop is triggered at the beginning the init overwrites it as false

  if (ENABLE_SENSOR_INIT_MESSAGES) {
	  HAL_UART_Transmit(&huart3, "endstops_update_done\n\r", strlen("endstops_update_done\n\r"), 200);
  }

  /* USER CODE END RTOS_EVENTS */

}

/* USER CODE BEGIN Header_StartCommandHandler */
/**
  * @brief  Function implementing the commandHandler thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartCommandHandler */
void StartCommandHandler(void *argument)
{
  /* USER CODE BEGIN StartCommandHandler */

	if (ENABLE_THREAD_INIT_MESSAGES) {
		HAL_UART_Transmit(&huart3, "commandHandler_init\n\r", strlen("commandHandler_init\n\r"), 200);
	}

  osStatus_t status;
  queue_commandRx queue_commandRx_local;
  command queue_command_local;
  /* Infinite loop */
  for(;;)
  {
    status = osMessageQueueGet(commandsRxQueueHandle, &queue_commandRx_local, 1, osWaitForever);

    if (status == osOK) {
		uint8_t local_rxData[queue_commandRx_local.len+1];
		memcpy(local_rxData, queue_commandRx_local.msg , sizeof local_rxData);
		local_rxData[queue_commandRx_local.len]='\0';
		if (handshake == 1) {
			if((local_rxData[0]=='$')&&(local_rxData[sizeof local_rxData - 2]==';')) {
				queue_command_local = get_command(local_rxData); // get commands from data
				if((queue_command_local.type!=99) && (queue_command_local.command!=99)){
					execute_command(&queue_command_local);
				}else{
					send_error(sendQueueHandle, local_rxData);
				}
			}else{
				send_error(sendQueueHandle, local_rxData);
			}
		} else if (strcmp(local_rxData, handshake_cmd) == 0) {
			send_acknowledge(sendQueueHandle, HANDSHAKE_COMMAND);
			handshake = 1;

			/* start task timers */
			osTimerStart(sTIM_motorPIDHandle, MOTOR_CONTROL_T);
			osTimerStart(sTIM_motionControlHandle, MOTION_CONTROL_T);

			osTimerStart(sTIM_sensorReadHandle, SENSOR_READ_T);
			osTimerStart(sTIM_controlTemperatureHandle, CONTROL_TEMP_T);
			osTimerStart(sTIM_processHandlerHandle, PROCESS_HANDLER_T);
		}
    }
    osDelay(1);
  }
  /* USER CODE END StartCommandHandler */
}

/* USER CODE BEGIN Header_StartSendData */
/**
* @brief Function implementing the sendData thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartSendData */
void StartSendData(void *argument)
{
  /* USER CODE BEGIN StartSendData */
	if (ENABLE_THREAD_INIT_MESSAGES) {
		HAL_UART_Transmit(&huart3, "sendData_init\n\r", strlen("sendData_init\n\r"), 200);
	}

  uint8_t recv[MSG_SIZE] = "\0";
  uint8_t txBuffer[MSG_SIZE];
  /* Infinite loop */
  for(;;)
  {
	osMessageQueueGet(sendQueueHandle, &recv, NULL, 200);

	/* print to txBuffer and reset */
	sprintf(txBuffer, "%s", recv);
	memset(recv, 0, sizeof recv);

	if (strcmp(recv, "") == 0) {
		HAL_UART_Transmit(&huart3, txBuffer, strlen(txBuffer), 200);
	}

    osDelay(1);
  }
  /* USER CODE END StartSendData */
}

/* USER CODE BEGIN Header_StartSensorRead */




/* USER CODE END Header_StartSensorRead */
void StartSensorRead(void *argument)
{
  /* USER CODE BEGIN StartSensorRead */
	if (ENABLE_THREAD_INIT_MESSAGES) {
		HAL_UART_Transmit(&huart3, "sensorRead_init\n\r", strlen("sensorRead_init\n\r"), 200);
	}
	//InitFlask();
  /* Infinite loop */
  for(;;)
  {
	vTaskSuspend(NULL); // wait for sTIM resume

	Foundry = sensors_read(&Sensors, &Foundry);
//	send_values_csv_format(sendQueueHandle,endStops.e_door_h, motor_Z.PID.input );
//	Foundry.loadcell_reading = hx711_weight(&Hx711_, 10);

//	uint8_t tmp[MSG_SIZE];
//	sprintf(tmp, "%lf,%lf,%lf,%lf,%lf;\r\n", Foundry.loadcell_reading, pouringControl.q_ref, pouringControl.theta, pouringControl.omega, motor_C.PID.input);
//	osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
	uint8_t tmp[MSG_SIZE];
	sprintf(tmp, "Z:%lf,T:%lf,C:%lf,TAR:%lf;\r\n", motor_Z.PID.input, motor_T.PID.input, motor_C.PID.input, motor_Z.motion.target);
	osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
	Foundry.loadcell_reading = material_count;

//	uint8_t tmp[MSG_SIZE];
//	if (ENABLE_VALUE_MESSAGES) {
//		sprintf(tmp, "%lf,%lf;\r\n", Foundry.loadcell_reading, Foundry.thermocouplesArray[1]);
//		osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
//	}
//	send_value(sendQueueHandle, Foundry.pressure);
	smoothPressure(&Foundry);
//	send_value(sendQueueHandle,Foundry.Smooth_pressure);

	laser_distance_converter(&Foundry);
//	send_value(sendQueueHandle,Foundry.distance_interne);

    osDelay(1);
  }
	}
  /* USER CODE END StartSensorRead */
//}

/* USER CODE BEGIN Header_StartControlTemperature */
/**
* @brief Function implementing the ControlTemperat thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartControlTemperature */
void StartControlTemperature(void *argument)
{
  /* USER CODE BEGIN StartControlTemperature */
	if (ENABLE_THREAD_INIT_MESSAGES) {
		HAL_UART_Transmit(&huart3, "controlTemp_init\n\r", strlen("controlTemp_init\n\r"), 200);
	}

	float highHeater,lowHeater;
  /* Infinite loop */
  for(;;)
  {
	vTaskSuspend(NULL);

	if (temperature_control_enabled) {
		if (abs(Foundry.thermocouplesArray[0] - Foundry.thermocouplesArray[1]) < TEMP_THRESHOLD) {
			float temp_ref_local = temp_target;

			highHeater = temp_ref_local;
			lowHeater = temp_ref_local - 5;

			temp_crucible = (Foundry.thermocouplesArray[0]+Foundry.thermocouplesArray[1])/2;

			if (temp_ref_local != 0.0){
				if (temp_crucible > highHeater) {
					HAL_GPIO_WritePin(HEATER_PIN, OFF);
				}
				else if (temp_crucible < lowHeater) {
					HAL_GPIO_WritePin(HEATER_PIN, ON);
				}
				else if((temp_crucible < highHeater) && (temp_crucible > lowHeater)) {
					is_target_temp_achieved = true;
				}
			}
		}
		else send_error(sendQueueHandle, "TEMP_READING_ERROR");
	}


    osDelay(1);
  }
  /* USER CODE END StartControlTemperature */
}

/* USER CODE BEGIN Header_StartMotorControl */



/* USER CODE END Header_StartMotorControl */
void StartMotorControl(void *argument)
{
  /* USER CODE BEGIN StartMotorControl */
	if (ENABLE_THREAD_INIT_MESSAGES) {
		HAL_UART_Transmit(&huart3, "motorControl_init\n\r", strlen("motorControl_init\n\r"), 200);
	}
  /* Infinite loop */
  for(;;)
  {
	vTaskSuspend(NULL); // wait for sTIM resume

	motor_PID_Update(&motor_Z);
	set_PWM(&motor_Z);

	motor_PID_Update(&motor_T);
	set_PWM(&motor_T);

	motor_PID_Update(&motor_C);
	set_PWM(&motor_C);

	osDelay(1);
  }
  /* USER CODE END StartMotorControl */
}

/* USER CODE BEGIN Header_StartMotionControl */
/**
* @brief Function implementing the motionControl thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartMotionControl */
void StartMotionControl(void *argument)
{
  /* USER CODE BEGIN StartMotionControl */
	if (ENABLE_THREAD_INIT_MESSAGES) {
		HAL_UART_Transmit(&huart3, "motionControl_init\n\r", strlen("motionControl_init\n\r"), 200);
	}
  /* Infinite loop */

	for(;;) {

		vTaskSuspend(NULL); // wait for sTIM resume

		if (ENABLE_MOTION_CONTROL && current_process.current_pouring_subprocess != POURING_PATTERN_GENERATOR) {

			/* MOTOR Z */
			float new_setpoint_Z;
			new_setpoint_Z = motion_update(&motor_Z.motion);

			/* if current setpoint is greater than previous, motor goes forward */
			if ((MOTOR_Z_FRWD_FACTOR * new_setpoint_Z) >= (MOTOR_Z_FRWD_FACTOR * motor_Z.PID.setpoint)) {
				goingForwardZ = true;
			}
			else {
				goingForwardZ = false;
			}
			motor_Z.PID.setpoint = new_setpoint_Z;

			/* MOTOR T */
			float new_setpoint_T;
			new_setpoint_T = motion_update(&motor_T.motion);

			/* if current setpoint is greater than previous, motor goes forward */
			if ((MOTOR_T_FRWD_FACTOR * new_setpoint_T) >= (MOTOR_T_FRWD_FACTOR * motor_T.PID.setpoint)) {
				goingForwardT = true;
			}
			else {
				goingForwardT = false;
			}
			motor_T.PID.setpoint = new_setpoint_T;

			/* MOTOR C */
			float new_setpoint_C;
			new_setpoint_C = motion_update(&motor_C.motion);

			/* if current setpoint is greater than previous, motor goes forward */
			if ((MOTOR_C_FRWD_FACTOR * new_setpoint_C) >= (MOTOR_C_FRWD_FACTOR * motor_C.PID.setpoint)) {
				goingForwardC = true;
			}
			else {
				goingForwardC = false;
			}
			motor_C.PID.setpoint = new_setpoint_C;

		}

		osDelay(1);
	}
  /* USER CODE END StartMotionControl */
}

/* USER CODE BEGIN Header_StartProcessHandler */
/**
* @brief Function implementing the processHandler thread.
* @param argument: Not used
* @retval None
*/
/* USER CODE END Header_StartProcessHandler */
void StartProcessHandler(void *argument)
{

	if (ENABLE_THREAD_INIT_MESSAGES) {
		HAL_UART_Transmit(&huart3, "processHandler_init\n\r", strlen("processHandler_init\n\r"), 200);
	}

	process_init(&current_process);
  /* Infinite loop */
  for(;;)
  {
	vTaskSuspend(NULL);
	static bool emergencyMessageTransmitted = false;

	if (endStops.em_stop_flag == true && !emergencyMessageTransmitted)
	{
			        HAL_UART_Transmit(&huart3, "EMERGENCY BUTTON PRESSED\n\r", strlen("EMERGENCY BUTTON PRESSED\n\r"), 200);
			        emergencyMessageTransmitted = true; // Set the flag after transmitting the
			        osDelay(500);
			    }

	else if (endStops.em_stop_flag == false && emergencyMessageTransmitted) {
			        HAL_UART_Transmit(&huart3, "EMERGENCY BUTTON  RELEASED\n\r", strlen("EMERGENCY BUTTON RELEASED\n\r"), 200);
			        emergencyMessageTransmitted = false; // Reset the flag after transmitting the

			        osDelay(500);
	}

	if (current_process.state != BUSY) {
		process_init(&current_process);

		osStatus_t status = 0;
		status = osMessageQueueGet(processQueueHandle, &current_process, NULL, 200);

		osDelay(50);

//		send_values(sendQueueHandle, status, osMessageQueueGetCount(processQueueHandle));
	}
	else {
		processHandler(&current_process);
	}

	if (endStops.em_stop_flag==true ) {

	       Emergency_process(&current_process);

	   }

    osDelay(1);
  }
  /* USER CODE END StartProcessHandler */
}

void Emergency_process(Process *process)
{
	// Release the current process
	releaseProcess(process);

	// Turn off the temperature control and the heater
	temperature_control_enabled = false;
	HAL_GPIO_WritePin(HEATER_PIN, OFF);

	HAL_GPIO_WritePin(LED_GREEN, OFF);
	HAL_GPIO_WritePin(LED_PANEL, ON);
	HAL_GPIO_WritePin(LED_RED, ON);
	// Turn off all the motors
	// Stopping T motor
	HAL_GPIO_WritePin(MOTOR_T_R_EN, OFF);
	HAL_GPIO_WritePin(MOTOR_T_L_EN, OFF);
	motor_T.motion.target = motor_T.PID.input;

	// Stopping Z motor
	HAL_GPIO_WritePin(MOTOR_Z_R_EN, OFF);
	HAL_GPIO_WritePin(MOTOR_Z_L_EN, OFF);
	motor_Z.motion.target = motor_Z.PID.input;

	// Stopping C motor
	HAL_GPIO_WritePin(MOTOR_C_R_EN, OFF);
	HAL_GPIO_WritePin(MOTOR_C_L_EN, OFF);
	motor_C.motion.target = motor_C.PID.input;

	// Execute home stop commands for all motors
	execute_command_from_value(MOTOR, MOTOR_C_STOP_HOME, 0);
	execute_command_from_value(MOTOR, MOTOR_T_STOP_HOME, 0);
	execute_command_from_value(MOTOR, MOTOR_Z_STOP_HOME, 0);

}


/* sTIM_sensorRead_Callback function */
void sTIM_sensorRead_Callback(void *argument)
{
  /* USER CODE BEGIN sTIM_sensorRead_Callback */
	vTaskResume(sensorReadHandle); /* resume reading sensors */
  /* USER CODE END sTIM_sensorRead_Callback */
}

/* sTIM_controlTemperatureHandle_Callback function */
void sTIM_controlTemperatureHandle_Callback(void *argument)
{
  /* USER CODE BEGIN sTIM_controlTemperatureHandle_Callback */
	vTaskResume(controlTemperatHandle);
  /* USER CODE END sTIM_controlTemperatureHandle_Callback */
}

/* sTIM_motorPID_Callback function */
void sTIM_motorPID_Callback(void *argument)
{
  /* USER CODE BEGIN sTIM_motorPID_Callback */
	vTaskResume(motorControlHandle); /* resume motor control */
  /* USER CODE END sTIM_motorPID_Callback */
}

/* sTIM_processHandler_Callback function */
void sTIM_processHandler_Callback(void *argument)
{
  /* USER CODE BEGIN sTIM_processHandler_Callback */
	vTaskResume(processHandlerHandle);
  /* USER CODE END sTIM_processHandler_Callback */
}

/* sTIM_motionControl_Callback function */
void sTIM_motionControl_Callback(void *argument)
{
  /* USER CODE BEGIN sTIM_motionControl_Callback */
	vTaskResume(motionControlHandle);
  /* USER CODE END sTIM_motionControl_Callback */
}

/* Private application code --------------------------------------------------*/
/* USER CODE BEGIN Application */

/* USER CODE END Application */

