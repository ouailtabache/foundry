/*
 * encoders.c
 *
 *  Created on: Aug 23, 2022
 *      Author: MM3D
 */

#include "encoders.h"

void encoders_init() {
	HAL_GPIO_WritePin(ENCODER_T_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(ENCODER_Z_PIN, GPIO_PIN_SET);
	HAL_GPIO_WritePin(ENCODER_C_PIN, GPIO_PIN_SET);

	/* set to quadrature */
	SPI_SET_MDR0(ENCODER_C);
	SPI_SET_MDR0(ENCODER_T);
	SPI_SET_MDR0(ENCODER_Z);

	correct_setup_C=check_MDR0_was_set(ENCODER_C);
	correct_setup_T=check_MDR0_was_set(ENCODER_T);
	correct_setup_Z=check_MDR0_was_set(ENCODER_Z);


	encoder_reset(ENCODER_C);
	encoder_reset(ENCODER_T);
	encoder_reset(ENCODER_Z);
}

long encoder_read(uint8_t slave_ID) {
	long count_value;
	const uint8_t CNTR_READ = 0b01100000; //command to read CNTR register
	SPI1Buffer[0]=0;
	SPI1Buffer[1]=0;
	SPI1Buffer[2]=0;
	SPI1Buffer[3]=0;
	SPI_SET(slave_ID, SPI_START);
	HAL_StatusTypeDef status_1 = HAL_SPI_Transmit(&hspi1,(uint8_t *)&CNTR_READ,1,100); //transmit the command 'read'
	HAL_StatusTypeDef status_2 = HAL_SPI_Receive(&hspi1, (uint8_t *)SPI1Buffer, 4, 100);
	SPI_SET(slave_ID, SPI_STOP);

	if((status_1==HAL_OK) && (status_2==HAL_OK)){
		count_value = (SPI1Buffer[0] << 8) + SPI1Buffer[1];
		count_value = (count_value << 8) + SPI1Buffer[2];
		count_value = (count_value << 8) + SPI1Buffer[3]; //shift the 4 'int' inputs to fit into one 'long'
	}else{
		count_value=0;
	}
	return count_value;
}

void encoder_reset(uint8_t slave_ID) {
	const uint8_t CNTR_CLR = 0b00100000; //clear cntr register of an encoder to reset its current position to 0

	SPI_SET(slave_ID, SPI_START);
	HAL_SPI_Transmit(&hspi1,(uint8_t *)&CNTR_CLR,1,100);
	SPI_SET(slave_ID, SPI_STOP);
}

uint8_t encoder_get_MDR0(uint8_t slave_ID){
	const uint8_t MDR0_READ = 0b01001000; //command sent to IR to write MDR0

	uint8_t result=0;

	SPI_SET(slave_ID, SPI_START);
	HAL_StatusTypeDef spi_t=HAL_SPI_Transmit(&hspi1,(uint8_t *)&MDR0_READ,1,100);
	HAL_StatusTypeDef spi_r=HAL_SPI_Receive(&hspi1, (uint8_t *)&result, 1, 100);
	SPI_SET(slave_ID, SPI_STOP);

	bool s=(spi_t!=HAL_OK)||(spi_r!=HAL_OK);
	if(s){
		result=0;
	}
	return result;
}

bool check_MDR0_was_set(uint8_t slave_ID){
	const uint8_t MDR0_4_QUAD_WRIT_EXPECTED = 0b00000011; //command to set MDR0 in 4x quadrature
	uint8_t r=encoder_get_MDR0(slave_ID);
	bool isCom=false;
	if(r==MDR0_4_QUAD_WRIT_EXPECTED){
		isCom=true;
	}
	return isCom;
}

bool isThereCommunication(uint8_t slave_ID){
	// it cant be used constantly bcs it makes crash de encoder driver in high velocities
	return check_MDR0_was_set(slave_ID);
}

float encoder_convert_to_unit(float value, UNIT_ unit, int gear_ratio) {
	float converted = 0.0;

	switch (unit) {
	case MM:
		converted = (float)value / (ENCODER_RES * gear_ratio) * GEAR_TOOTH_N * GEAR_TOOTH_VAL;
		break;
	case DEG:
		converted = (float)value / (ENCODER_RES * gear_ratio) * 360;
		break;
	default:
		//
		break;
	}

	return converted;
}

float encoder_convert_from_unit(float value, UNIT_ unit, int gear_ratio) {
	float converted = 0.0;

	switch (unit) {
	case MM:
		converted = value * (ENCODER_RES * gear_ratio) / GEAR_TOOTH_N / GEAR_TOOTH_VAL;
		break;
	case DEG:
		converted = value * (ENCODER_RES * gear_ratio) / 360;
		break;
	default:
		//
		break;
	}
	return converted;
}

Encoder_statusHandle encoder_check(long prev_reading, long curr_reading, Motor_statusHandle state) {

	Encoder_statusHandle status;

	if (prev_reading != curr_reading && state == MOTOR_IS_OFF) {
		status = ENCODER_FAULT;
	}
	else status = ENCODER_OK;

	return status;
}
