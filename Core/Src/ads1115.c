/*
 * ads1115.c
 *
 *  Created on: Aug 17, 2022
 *      Author: DAVID QUEZADA
 */

#include "ads1115.h"

#include "cmsis_os.h"




/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "stdbool.h"
#include "queue.h"
#include <math.h>



Ads1115_HandleTypeDef ads1115_Init(I2C_HandleTypeDef* i2c_handle,uint8_t device_address) {
	Ads1115_HandleTypeDef ADS1115_;
	ADS1115_.i2c_handle = i2c_handle;
	ADS1115_.device_address = device_address;
	return ADS1115_;
}


void ads1115_GetValue(Ads1115_HandleTypeDef *ads1115, uint16_t *pressure) {
	uint16_t voltage;
	unsigned char ADSwrite[6];
	int16_t reading;
	const float voltageConv = 6.114 / 32768.0;

	ADSwrite[0] = 0x01; //00000001
	ADSwrite[1] = 0xC1; //11000001
	ADSwrite[2] = 0x83; //10000011 LSB

	HAL_I2C_Master_Transmit(ads1115->i2c_handle, ads1115->device_address, ADSwrite, 3,HAL_MAX_DELAY);
	ADSwrite[0] = 0x00;
	HAL_I2C_Master_Transmit(ads1115->i2c_handle, ads1115->device_address, ADSwrite, 1,HAL_MAX_DELAY);
	osDelay(20);
	HAL_I2C_Master_Receive(ads1115->i2c_handle, ads1115->device_address, ADSwrite, 2,HAL_MAX_DELAY);

	reading = (ADSwrite[0] << 8 | ADSwrite[1] );

	if(reading < 0) {
		reading = 0;
	}

	voltage = (uint16_t)(reading * voltageConv*3034.38);
	//then add a multiplier to convert this voltage into pressure
	*pressure = (uint16_t)(voltage);
}

void ads1115_GetValue_v2(Ads1115_HandleTypeDef *ads1115, uint16_t *pressure, HAL_StatusTypeDef res[]) {
	uint16_t voltage;
	unsigned char ADSwrite[6];
	int16_t reading;
	const float voltageConv = 6.114 / 32768.0;
	ADSwrite[0] = 0x01; //00000001
	ADSwrite[1] = 0xC1; //11000001
	ADSwrite[2] = 0x83; //10000011 LSB

	res[0]=HAL_ERROR;
	res[1]=HAL_ERROR;
	res[2]=HAL_ERROR;

	res[0]=HAL_I2C_Master_Transmit(ads1115->i2c_handle, ads1115->device_address, ADSwrite, 3, 5);

	ADSwrite[0] = 0x00;
	res[1]=HAL_I2C_Master_Transmit(ads1115->i2c_handle, ads1115->device_address, ADSwrite, 1, 5);

	ADSwrite[0] = 0x00;
	ADSwrite[1] = 0x00;
	res[2]=HAL_I2C_Master_Receive(ads1115->i2c_handle, ads1115->device_address, ADSwrite, 2, 5);

	reading = (ADSwrite[0] << 8 | ADSwrite[1] );

	if(reading < 0) {
		reading = 0;
	}

	voltage = (uint16_t)(reading * voltageConv * 3034.38);
	//then add a multiplier to convert this voltage into pressure
	*pressure = (uint16_t)(voltage);
}
