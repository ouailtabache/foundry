/*
 * SPI_Handler.c
 *
 *  Created on: Aug 23, 2022
 *      Author: MM3D
 */

#include "SPI_Handler.h"

uint8_t SPI1Buffer[30];

void SPI_SET(uint8_t slave_ID, SPI_PinState state) {
	switch (slave_ID) {
		case ENCODER_T:
			HAL_GPIO_WritePin(ENCODER_T_PIN, state);     // Stop SPI conversation 1st Slave
			break;
		case ENCODER_Z:
			HAL_GPIO_WritePin(ENCODER_Z_PIN, state);     // Stop SPI conversation 2nd Slave
			break;
		case ENCODER_C:
			HAL_GPIO_WritePin(ENCODER_C_PIN, state);     // Stop SPI conversation 3 Slave
			break;
		default:
			//we should never go here
			break;
    }
}

void SPI_SET_MDR0(uint8_t slave_ID) {
	const uint8_t MDR0_WRITE = 0b10001000; //command sent to IR to write MDR0
	const uint8_t MDR0_4_QUAD_WRITE = 0b00000011; //command to set MDR0 in 4x quadrature
	SPI_SET(slave_ID, SPI_START);
	HAL_SPI_Transmit(&hspi1,(uint8_t *)&MDR0_WRITE,1,100);
	HAL_SPI_Transmit(&hspi1,(uint8_t *)&MDR0_4_QUAD_WRITE,1,100);
	SPI_SET(slave_ID, SPI_STOP);
}

uint8_t SPI_READ_MDR0(uint8_t slave_ID) {
	uint8_t count_1;
	const unsigned int MDR0_READ = 0b01001000; //command sent to IR to read MDR0
	SPI_SET(slave_ID, SPI_START);
	HAL_SPI_Transmit(&hspi1,(uint8_t *)&MDR0_READ,1,100);
	HAL_SPI_Receive(&hspi1,(uint8_t *)SPI1Buffer,1,100);
	count_1=(uint8_t)SPI1Buffer[0];
	SPI_SET(slave_ID, SPI_STOP);
	return count_1;
}
