#include "additionalFunctions.h"

float abs_here(float val){
	if(val<0){
		val=val*-1;
	}
	return val;
}

int sign_here(float val){
	int r=-1;
	if(val>0){
		r=1;
	}else if(val==0){
		r=0;
	}
	return r;
}
