/*
 * motorHandler.c
 *
 *  Created on: 4 sty 2023
 *      Author: MM3D
 */

#include "motorHandler.h"
#include "define.h"

void motor_Init(Motor_Handle *motor, MOTOR_ thisMotor, ENCODER_ thisEncoder, UNIT_ unit) {
	motor->motor = thisMotor;
	motor->encoder = thisEncoder;
	motor->state = MOTOR_IS_OFF;
	motor->unit = unit;

	motor_Config_Init(motor);
	motor->initPos = motor->config.FRWD_FACTOR * encoder_convert_from_unit(motor->config.INIT_POS_OFFSET, motor->unit, motor->config.GEARBOX_RATIO);
	motor_PID_Init(motor);
	motor_Motion_Init(motor);
}

void motor_PID_Init(Motor_Handle *motor) {
	motor_PID_Reset(motor);
	motor->PID.Kp = motor->config.K_P;
	motor->PID.Ki = motor->config.K_I;
	motor->PID.Kd = motor->config.K_D;

	motor->PID.output_MIN = -1 * motor->config.PWM_LIMIT;
	motor->PID.output_MAX = 1 * motor->config.PWM_LIMIT;
}

void motor_PID_Update(Motor_Handle *motor) {
	motor->PID.input = encoder_read(motor->encoder) + encoder_convert_from_unit(motor->config.INIT_POS_OFFSET, motor->unit, motor->config.GEARBOX_RATIO);

	motor->PID.error_p = (motor->PID.setpoint - motor->PID.input)  * motor->PID.Kp;         /* proportional error calc. */
	motor->PID.error_d = (motor->PID.input - motor->PID.prev_input) * motor->PID.Kd;        /* derivative error calc. */

	if (motor->PID.error_d == 0.0) {
		motor->PID.error_i += (motor->PID.error_p * motor->PID.Ki);
	}
	else {
		motor->PID.error_i -= (motor->PID.error_d * motor->PID.Ki);
	}

	if (motor->PID.error_i > motor->PID.output_MAX) {
		motor->PID.error_i = motor->PID.output_MAX;
	}
	else if (motor->PID.error_i < motor->PID.output_MIN){
		motor->PID.error_i = motor->PID.output_MIN;
	}

	motor->PID.output = motor->PID.error_p + motor->PID.error_i - motor->PID.error_d;  /* PID output */

	if (motor->PID.output > motor->PID.output_MAX) {
		motor->PID.output = motor->PID.output_MAX;
	}
	else if (motor->PID.output < motor->PID.output_MIN) {
		motor->PID.output = motor->PID.output_MIN;
	}

	if (motor->PID.prev_input != motor->PID.input){
		motor->PID.prev_input = motor->PID.input;
	}
}

void motor_PID_Update_v2(Motor_Handle *motor) {
	motor->PID.input = encoder_read(motor->encoder) + encoder_convert_from_unit(motor->config.INIT_POS_OFFSET, motor->unit, motor->config.GEARBOX_RATIO);

	float Ts = MOTOR_CONTROL_T / 1000.0; 	// in seconds
	float error = motor->PID.setpoint - motor->PID.input;

	float new_integral = motor->PID.integral + error * Ts;

	motor->PID.derivative = (motor->PID.input - motor->PID.prev_input) / Ts;

	/* control signal */
	motor->PID.output = motor->PID.Kp * error + motor->PID.Ki * motor->PID.integral + motor->PID.Kd * motor->PID.derivative;

	if (motor->PID.output > motor->PID.output_MAX) {
		motor->PID.output = motor->PID.output_MAX;
	}
	else if (motor->PID.output < motor->PID.output_MIN) {
		motor->PID.output = motor->PID.output_MIN;
	}
	else motor->PID.integral = new_integral;

	if (motor->PID.prev_input != motor->PID.input){
		motor->PID.prev_input = motor->PID.input;
	}
}

void motor_PID_Reset(Motor_Handle *motor) {
	motor->PID.error			= 0;
	motor->PID.error_p			= 0;
	motor->PID.error_d			= 0;
	motor->PID.error_i			= 0;
	motor->PID.setpoint			= motor->initPos;
	motor->PID.input			= motor->initPos;
	motor->PID.prev_input		= 0;
	motor->PID.output			= 0;

	motor->PID.integral = 0;
	motor->PID.derivative = 0;
}

void motor_Config_Init(Motor_Handle *motor) {
	switch (motor->motor) {
	case MOTOR_C:
		motor->config.K_P 					= MOTOR_C_KP;
		motor->config.K_I 					= MOTOR_C_KI;
		motor->config.K_D 					= MOTOR_C_KD;
		motor->config.PWM_LIMIT 			= MOTOR_C_LIMIT;

		motor->config.MAX_VEL 				= MOTOR_C_MAX_VEL;
		motor->config.MAX_ACC 				= MOTOR_C_MAX_ACC;
		motor->config.FRWD_FACTOR 			= MOTOR_C_FRWD_FACTOR;
		motor->config.LIMIT_MIN 			= MOTOR_C_LIM_MIN;
		motor->config.LIMIT_MAX 			= MOTOR_C_LIM_MAX;
		motor->config.INIT_POS_OFFSET 		= MOTOR_C_OFFSET;
		motor->config.HOME_OFFSET 			= MOTOR_C_HOME_OFFSET;
		motor->config.OFFSET_THRESHOLD 		= MOTOR_C_OFFSET_THRESHOLD;
		motor->config.HOME_BACK_OFF_DIST 	= MOTOR_C_HOME_BACK_OFF_DISTANCE_DEG;
		motor->config.HOME_MAX_DIST 		= MOTOR_C_HOME_MAX_DISTANCE_DEG;
		motor->config.HOME_MAX_VEL 			= MOTOR_C_HOME_MAX_VEL;
		motor->config.GEARBOX_RATIO 		= C_RATIO;

		break;

	case MOTOR_T:
		motor->config.K_P 					= MOTOR_T_KP;
		motor->config.K_I 					= MOTOR_T_KI;
		motor->config.K_D 					= MOTOR_T_KD;
		motor->config.PWM_LIMIT 			= MOTOR_T_LIMIT;

		motor->config.MAX_VEL 				= MOTOR_T_MAX_VEL;
		motor->config.MAX_ACC 				= MOTOR_T_MAX_ACC;
		motor->config.FRWD_FACTOR 			= MOTOR_T_FRWD_FACTOR;
		motor->config.LIMIT_MIN 			= MOTOR_T_LIM_MIN;
		motor->config.LIMIT_MAX 			= MOTOR_T_LIM_MAX;
		motor->config.INIT_POS_OFFSET 		= MOTOR_T_OFFSET;
		motor->config.HOME_OFFSET 			= MOTOR_T_HOME_OFFSET;
		motor->config.OFFSET_THRESHOLD 		= MOTOR_T_OFFSET_THRESHOLD;
		motor->config.HOME_BACK_OFF_DIST 	= MOTOR_T_HOME_BACK_OFF_DISTANCE_DEG;
		motor->config.HOME_MAX_DIST 		= MOTOR_T_HOME_MAX_DISTANCE_DEG;
		motor->config.HOME_MAX_VEL 			= MOTOR_T_HOME_MAX_VEL;
		motor->config.GEARBOX_RATIO 		= T_RATIO;

		break;

	case MOTOR_Z:
		motor->config.K_P 					= MOTOR_Z_KP;
		motor->config.K_I 					= MOTOR_Z_KI;
		motor->config.K_D 					= MOTOR_Z_KD;
		motor->config.PWM_LIMIT 			= MOTOR_Z_LIMIT;

		motor->config.MAX_VEL 				= MOTOR_Z_MAX_VEL;
		motor->config.MAX_ACC 				= MOTOR_Z_MAX_ACC;
		motor->config.FRWD_FACTOR 			= MOTOR_Z_FRWD_FACTOR;
		motor->config.LIMIT_MIN 			= MOTOR_Z_LIM_MIN;
		motor->config.LIMIT_MAX 			= MOTOR_Z_LIM_MAX;
		motor->config.INIT_POS_OFFSET 		= MOTOR_Z_OFFSET;
		motor->config.HOME_OFFSET 			= MOTOR_Z_HOME_OFFSET;
		motor->config.OFFSET_THRESHOLD 		= MOTOR_Z_OFFSET_THRESHOLD;
		motor->config.HOME_BACK_OFF_DIST 	= MOTOR_Z_HOME_BACK_OFF_DISTANCE_MM;
		motor->config.HOME_MAX_DIST 		= MOTOR_Z_HOME_MAX_DISTANCE_MM;
		motor->config.HOME_MAX_VEL 			= MOTOR_Z_HOME_MAX_VEL;
		motor->config.GEARBOX_RATIO 		= Z_RATIO;

		break;

	default:
		break;
	}
}

void init_PWM() {
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	TIM3->CCR1 = 0;
	TIM3->CCR2 = 0;

	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
	TIM2->CCR1 = 0;
	TIM2->CCR2 = 0;


	HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_4);
	TIM5->CCR3 = 0;
	TIM5->CCR4 = 0;
}

void set_PWM(Motor_Handle *motor) {
	switch (motor->motor) {
		case MOTOR_T:
			if (abs(motor->PID.setpoint - motor->PID.input) < 2) {   /* If it reaches the setpoint, stop the motor. */
				TIM2->CCR1 = 0;
				TIM2->CCR2 = 0;
				HAL_GPIO_WritePin(MOTOR_T_R_EN, OFF);
				HAL_GPIO_WritePin(MOTOR_T_L_EN, OFF);
				osDelay(3);

			}
			else {                     			/* If the position is yet to be reached, rotate in the respective direction, based on the output of the PID */
			  if (motor->PID.output < 0.5) {
				  TIM2->CCR2 = 0;
				  TIM2->CCR1 = abs(motor->PID.output);
				  HAL_GPIO_WritePin(MOTOR_T_R_EN, ON);
				  HAL_GPIO_WritePin(MOTOR_T_L_EN, ON);
				  osDelay(3);
			  }
			  else {
				  TIM2->CCR1 = 0;
				  TIM2->CCR2 = abs(motor->PID.output);
				  HAL_GPIO_WritePin(MOTOR_T_R_EN, ON);
				  HAL_GPIO_WritePin(MOTOR_T_L_EN, ON);
				  osDelay(3);

			  }
			}
			break;
		case MOTOR_Z:
			if (abs(motor->PID.setpoint - motor->PID.input) < 2) {   /* If it reaches the setpoint, stop the motor. */
				TIM5->CCR3 = 0;
				TIM5->CCR4 = 0;
				HAL_GPIO_WritePin(MOTOR_Z_R_EN, OFF);
				HAL_GPIO_WritePin(MOTOR_Z_L_EN, OFF);
				osDelay(3);

			}
			else {                     			/* If the position is yet to be reached, rotate in the respective direction, based on the output of the PID */
			  if (motor->PID.output < 0.5) {
				  TIM5->CCR4 = 0;
				  TIM5->CCR3 = abs(motor->PID.output);
				  HAL_GPIO_WritePin(MOTOR_Z_R_EN, ON);
				  HAL_GPIO_WritePin(MOTOR_Z_L_EN, ON);
				  osDelay(3);

			  }
			  else {
				  TIM5->CCR3 = 0;
				  TIM5->CCR4 = abs(motor->PID.output);
				  HAL_GPIO_WritePin(MOTOR_Z_R_EN, ON);
				  HAL_GPIO_WritePin(MOTOR_Z_L_EN, ON);
				  osDelay(3);
			  }
			}
			break;
		case MOTOR_C:
			if (abs(motor->PID.setpoint - motor->PID.input) < 2) {   /* If it reaches the setpoint, stop the motor. */
				TIM3->CCR1 = 0;
				TIM3->CCR2 = 0;
				HAL_GPIO_WritePin(MOTOR_C_R_EN, OFF);
				HAL_GPIO_WritePin(MOTOR_C_L_EN, OFF);
				osDelay(3);
			}
			else {                     			/* If the position is yet to be reached, rotate in the respective direction, based on the output of the PID */
			  if (motor->PID.output < 0.5) {
				  TIM3->CCR2 = 0;
				  TIM3->CCR1 = abs(motor->PID.output);
				  HAL_GPIO_WritePin(MOTOR_C_R_EN, ON);
				  HAL_GPIO_WritePin(MOTOR_C_L_EN, ON);
				  osDelay(3);
			  }
			  else {
				  TIM3->CCR1 = 0;
				  TIM3->CCR2 = abs(motor->PID.output);
				  HAL_GPIO_WritePin(MOTOR_C_R_EN, ON);
				  HAL_GPIO_WritePin(MOTOR_C_L_EN, ON);
				  osDelay(3);
			  }
			}
			break;
		default:
			break;
	}
}

void enable_PWM(Motor_Handle *motor) {
	switch (motor->motor) {
		case MOTOR_T:
			HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
			HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_2);
			break;
		case MOTOR_Z:
			HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_3);
			HAL_TIM_PWM_Start(&htim5, TIM_CHANNEL_4);
			break;
		case MOTOR_C:
			HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
			HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
			break;
		default:
			break;
	}
}

void disable_PWM(Motor_Handle *motor) {
	switch (motor->motor) {
		case MOTOR_T:
			HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_1);
			HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_2);
			break;
		case MOTOR_Z:
			HAL_TIM_PWM_Stop(&htim5, TIM_CHANNEL_3);
			HAL_TIM_PWM_Stop(&htim5, TIM_CHANNEL_4);
			break;
		case MOTOR_C:
			HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_1);
			HAL_TIM_PWM_Stop(&htim3, TIM_CHANNEL_2);
			break;
		default:
			break;
	}
}

void motor_Motion_Init(Motor_Handle *motor) {
	motor->motion = motionGenerator_Init(motor->config.MAX_VEL, motor->config.MAX_ACC, motor->initPos);
}

bool isMotorClose(Motor_Handle *motor, float target_position, float threshold) {
	float current_position = 0;
	current_position = getCurrentMotorPosition(motor);

//	send_values(sendQueueHandle, current_position, target_position);

//	float tmp = 0;
//	tmp = fabs(target_position - current_position);
//	send_value(sendQueueHandle, tmp);

	if (fabs(target_position - current_position) < threshold) {

		return true;
	}
	else {
		return false;
	}
}

bool isMotorBetween(Motor_Handle *motor, float lower_limit, float higher_limit) {
	float current_position = 0;
	current_position = getCurrentMotorPosition(motor);

	/* this is inverted, because the 'top position' is the lower value */
	if (current_position <= lower_limit && current_position >= higher_limit) return true;
	else return false;
}

float getCurrentMotorPosition(Motor_Handle *motor) {
	float motorPosition = 0;

	motorPosition = motor->config.FRWD_FACTOR * encoder_convert_to_unit(motor->PID.input, motor->unit, motor->config.GEARBOX_RATIO);

	return motorPosition;
}

float getTargetMotorPosition(Motor_Handle *motor) {
	float motorPosition = 0.0;

	motorPosition = motor->config.FRWD_FACTOR * encoder_convert_to_unit(motor->PID.setpoint, motor->unit, motor->config.GEARBOX_RATIO);

	return motorPosition;
}

bool setMotorPosition(Motor_Handle *motor, float position) {
	if (motor->state != MOTOR_IS_RUNNING && !isMotorClose(motor, position, motor->config.OFFSET_THRESHOLD)) {
		motor->motion.target = motor->config.FRWD_FACTOR * encoder_convert_from_unit(position, motor->unit, motor->config.GEARBOX_RATIO);
		motor->state = MOTOR_IS_RUNNING;
	}
	else if (isMotorClose(motor, position, motor->config.OFFSET_THRESHOLD)) {
		motor->motion.target = motor->PID.input; 		// stop the motor
		motor->state = MOTOR_IS_OFF;

		return true;
	}

	return false;
}
