/*
 * max31856.c
 *
 *  Created on: Aug 19, 2022
 *      Author: DAVID QUEZADA
 */


#include "max31856.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "string.h"
#include "stdlib.h"
#include "stdbool.h"
#include "queue.h"


Max31856_HandleTypeDef max31856_Init(SPI_HandleTypeDef* spi_handle, GPIO_TypeDef *cs_port, uint16_t cs_pin) {
	Max31856_HandleTypeDef MAX31856_;
	MAX31856_.spi_handle = spi_handle;
	MAX31856_.settings.cs_pin = cs_pin;
	MAX31856_.settings.cs_port = cs_port;
	return MAX31856_;
}


void writeRegister8(Max31856_HandleTypeDef *max31856, uint8_t addr, uint8_t data) {
  addr |= 0x80; // MSB=1 for write, make sure top bit is set
  uint8_t buffer[2] = {addr, data};

  HAL_GPIO_WritePin(max31856->settings.cs_port,max31856->settings.cs_pin, GPIO_PIN_RESET);
  HAL_SPI_Transmit(max31856->spi_handle,(uint8_t *)&buffer,2,5);
  HAL_GPIO_WritePin(max31856->settings.cs_port,max31856->settings.cs_pin, GPIO_PIN_SET);

}

HAL_StatusTypeDef readRegisterN(Max31856_HandleTypeDef *max31856, uint8_t addr, uint8_t *buffer,int8_t n) {
  addr &= 0x7F; // MSB=0 for read, make sure top bit is not set
  HAL_StatusTypeDef status=HAL_ERROR;
  HAL_GPIO_WritePin(max31856->settings.cs_port,max31856->settings.cs_pin, GPIO_PIN_RESET);
  HAL_SPI_Transmit(max31856->spi_handle,& addr,1,5);
  status=HAL_SPI_Receive(max31856->spi_handle, buffer,n,5);
  HAL_GPIO_WritePin(max31856->settings.cs_port,max31856->settings.cs_pin, GPIO_PIN_SET);
  return status;
}

HAL_StatusTypeDef readRegister8(Max31856_HandleTypeDef *max31856,uint8_t* ret, uint8_t addr) {
  HAL_StatusTypeDef status=HAL_ERROR;
  status=readRegisterN(max31856, addr, ret ,1);
  return status;;
}

HAL_StatusTypeDef readRegister16(Max31856_HandleTypeDef *max31856,uint16_t* r, uint8_t addr) {
  uint8_t buffer[2] = {0, 0};
  HAL_StatusTypeDef status=HAL_ERROR;
  status=readRegisterN(max31856, addr, (uint8_t *) &buffer, 2);
  uint16_t ret = buffer[0];
  ret <<= 8;
  ret |= buffer[1];
  *r=ret;
  return status;
}


HAL_StatusTypeDef readRegister24(Max31856_HandleTypeDef *max31856,uint32_t* r, uint8_t addr) {
  uint8_t buffer[3] = {0, 0, 0};
  HAL_StatusTypeDef status=HAL_ERROR;
  status=readRegisterN(max31856, addr,(uint8_t *) &buffer, 3);

  uint32_t ret = buffer[0];
  ret <<= 8;
  ret |= buffer[1];
  ret <<= 8;
  ret |= buffer[2];
  *r=ret;
  return status;
}

void setThermocoupleType(Max31856_HandleTypeDef *max31856, max31856_thermocoupletype_t type) {
  uint8_t t;
  readRegister8(max31856,&t, MAX31856_CR1_REG);
  t &= 0xF0; // mask off bottom 4 bits
  t |= (uint8_t)type & 0x0F;
  writeRegister8(max31856, MAX31856_CR1_REG, t);
}

void setConversionMode(Max31856_HandleTypeDef *max31856, max31856_conversion_mode_t mode) {
	  max31856->conversionMode = mode;
	  uint8_t t ;
	  readRegister8(max31856,&t, MAX31856_CR0_REG); // get current register value
	  if (max31856->conversionMode == MAX31856_CONTINUOUS) {
	    t |= MAX31856_CR0_AUTOCONVERT; // turn on automatic
	    t &= ~MAX31856_CR0_1SHOT;      // turn off one-shot
	  } else {
	    t &= ~MAX31856_CR0_AUTOCONVERT; // turn off automatic
	    t |= MAX31856_CR0_1SHOT;        // turn on one-shot
	  }
	  writeRegister8(max31856, MAX31856_CR0_REG, t); // write value back to register
}

void setNoiseFilter(Max31856_HandleTypeDef *max31856, max31856_noise_filter_t noiseFilter) {
  uint8_t t;
  readRegister8(max31856, &t,MAX31856_CR0_REG);
  if (noiseFilter == MAX31856_NOISE_FILTER_50HZ) {
    t |= 0x01;
  } else {
    t &= 0xfe;
  }
  writeRegister8(max31856, MAX31856_CR0_REG, t);
}

void setFaultMode(Max31856_HandleTypeDef *max31856, max31856_mode mode) {
	  uint8_t t;
	  readRegister8(max31856, &t, MAX31856_CR0_REG);
	  if (mode == MAX31856_COMPARATOR_MODE) {
		    t &= 0xfb;
	  } else {
		    t |= 0x04;
	  }
	  writeRegister8(max31856, MAX31856_CR0_REG, t);
}

void max31856_readFault(Max31856_HandleTypeDef *max31856, uint8_t* result) {
	readRegister8(max31856,result,MAX31856_SR_REG);
}

void setCJ(Max31856_HandleTypeDef *max31856, max31856_CJ mode) {
	  uint8_t t;
	  readRegister8(max31856, &t,MAX31856_CR0_REG);
	  if (mode == MAX31856_CJ_ENABLED) {
		    t &= 0xf7;
	  } else {
		    t |= 0x08;
	  }
	  writeRegister8(max31856, MAX31856_CR0_REG, t);
}


void returnStringFromFaultRegister(char sensor[], uint8_t fault, char* msgr[]) {
	strcpy(msgr,"");
	if (fault) {
	    strcat(msgr,sensor);
	    if ((fault & MAX31856_FAULT_CJRANGE)!=0){
	        strcat(msgr,":");
	        strcat(msgr,"CJRANGE");
	    }
	    if ((fault & MAX31856_FAULT_TCRANGE)!=0){
	        strcat(msgr,":");
	        strcat(msgr,"TCRANGE");
	    }
	    if ((fault & MAX31856_FAULT_CJHIGH)!=0){
	        strcat(msgr,":");
	       strcat(msgr,"CJHIGH");
	    }
	    if ((fault & MAX31856_FAULT_CJLOW)!=0){
	        strcat(msgr,":");
	        strcat(msgr,"CJLOW");
	    }
	    if ((fault & MAX31856_FAULT_TCHIGH)!=0){
	        strcat(msgr,":");
	        strcat(msgr,"TCHIGH");
	    }
	    if ((fault & MAX31856_FAULT_TCLOW)!=0){
	        strcat(msgr,":");
	        strcat(msgr,"TCLOW");
	    }
	    if ((fault & MAX31856_FAULT_OVUV)!=0){
	        strcat(msgr,":");
	        strcat(msgr,"OVUV");
	    }
	    if ((fault & MAX31856_FAULT_OPEN)!=0){
	        strcat(msgr,":");
	        strcat(msgr,"OPEN");
	    }
	    strcat(msgr,"");
	}
}

uint8_t max31856_beginSetup(Max31856_HandleTypeDef *max31856){
	uint8_t a=1;
	//comprobe there-s SPI communicaiton
	//MISSING!

	// enable open circuit fault detection
	writeRegister8(max31856, MAX31856_CR0_REG, MAX31856_CR0_OCFAULT0); // writes 0 in all register but 01 in Open Circuit detection mode

	// set One-Shot conversion mode
	setConversionMode(max31856, MAX31856_CONTINUOUS);

	setCJ(max31856, MAX31856_CJ_ENABLED);

	// set Fault mode to Comparator mode
	setFaultMode(max31856, MAX31856_COMPARATOR_MODE);

	setNoiseFilter(max31856, MAX31856_NOISE_FILTER_50HZ);

	// set Type K by default
	setThermocoupleType(max31856, MAX31856_TCTYPE_K);

	// set cold junction temperature offset to zero
	writeRegister8(max31856, MAX31856_CJTO_REG, 0x0);

	// assert on any fault
	writeRegister8(max31856, MAX31856_MASK_REG, 0x0); //default value OxOO to write to registers (MASK and CJT0)

	return a;
}

HAL_StatusTypeDef  max31856_GetThermocouple(Max31856_HandleTypeDef *max31856, float* result){
	uint32_t temp24;
	HAL_StatusTypeDef status=HAL_ERROR;
	// read the thermocouple temperature registers (3 bytes)
	status=readRegister24(max31856, &temp24, MAX31856_LTCBH_REG);
	// and compute temperature
	if (temp24 & 0x800000) {
		temp24 |= 0xFF000000; // fix sign
	}

	temp24 >>= 5; // bottom 5 bits are unused

	*result = temp24 * 0.0078125;
	return status;
}

void max31856_handleError(HAL_StatusTypeDef spiStatus, float readValue){

}
float readCJTemperature(Max31856_HandleTypeDef *max31856) {
	uint16_t t;
	readRegister16(max31856, &t,MAX31856_CJTH_REG);
	return t/ 256.0;
}
