/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "dma.h"
#include "i2c.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "usb_otg.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "commandHandler.h"
#include "sendData.h"
#include "sensorRead.h"
#include "endStops.h"
#include "vl53l0x_api.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

extern osThreadId_t commandHandlerHandle;
extern osThreadId_t EXTI_HandlerHandle;
extern osSemaphoreId_t commandBufferHandle;
extern osMessageQueueId_t commandsRxQueueHandle;
extern end_stops endStops;

extern bool goingForwardC, goingForwardT, goingForwardZ;
int material_count;

VL53L0X_RangingMeasurementData_t RangingData;
VL53L0X_Dev_t  vl53l0x_c;
VL53L0X_DEV    Dev = &vl53l0x_c;
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  uint32_t refSpadCount;
    uint8_t isApertureSpads;
    uint8_t VhvSettings;
    uint8_t PhaseCal;



    Dev->I2cHandle = &hi2c1;
    Dev->I2cDevAddr = 0x53; //0x29 base address 0x52 write address 0x53 write address
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USB_OTG_HS_USB_Init();
  MX_I2C1_Init();
  MX_TIM17_Init();
  MX_SPI1_Init();
  MX_TIM8_Init();
  MX_TIM3_Init();
  MX_SPI2_Init();
  MX_TIM5_Init();
  MX_UART5_Init();
  MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */
  HAL_TIM_Base_Start_IT(&htim17);
  HAL_UART_Receive_IT(&huart3, &rxBuffer, 1);
  init_PWM();

  // VL53L0X init for Single Measurement
   VL53L0X_WaitDeviceBooted( Dev );
   VL53L0X_DataInit( Dev );
   VL53L0X_StaticInit( Dev );
   VL53L0X_PerformRefCalibration(Dev, &VhvSettings, &PhaseCal);
   VL53L0X_PerformRefSpadManagement(Dev, &refSpadCount, &isApertureSpads);
   VL53L0X_SetDeviceMode(Dev, VL53L0X_DEVICEMODE_SINGLE_RANGING);

   // Enable/Disable Sigma and Signal check
   VL53L0X_SetLimitCheckEnable(Dev, VL53L0X_CHECKENABLE_SIGMA_FINAL_RANGE, 1);
   VL53L0X_SetLimitCheckEnable(Dev, VL53L0X_CHECKENABLE_SIGNAL_RATE_FINAL_RANGE, 1);
   VL53L0X_SetLimitCheckValue(Dev, VL53L0X_CHECKENABLE_SIGNAL_RATE_FINAL_RANGE, (FixPoint1616_t)(0.1*65536));
   VL53L0X_SetLimitCheckValue(Dev, VL53L0X_CHECKENABLE_SIGMA_FINAL_RANGE, (FixPoint1616_t)(60*65536));
   VL53L0X_SetMeasurementTimingBudgetMicroSeconds(Dev, 33000);
   VL53L0X_SetVcselPulsePeriod(Dev, VL53L0X_VCSEL_PERIOD_PRE_RANGE, 18);
   VL53L0X_SetVcselPulsePeriod(Dev, VL53L0X_VCSEL_PERIOD_FINAL_RANGE, 14);
  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();  /* Call init function for freertos objects (in freertos.c) */
  MX_FREERTOS_Init();

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /*AXI clock gating */
  RCC->CKGAENR = 0xFFFFFFFF;

  /** Supply configuration update enable
  */
  HAL_PWREx_ConfigSupply(PWR_DIRECT_SMPS_SUPPLY);

  /** Configure the main internal regulator output voltage
  */
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE0);

  while(!__HAL_PWR_GET_FLAG(PWR_FLAG_VOSRDY)) {}

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 24;
  RCC_OscInitStruct.PLL.PLLP = 2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  RCC_OscInitStruct.PLL.PLLR = 2;
  RCC_OscInitStruct.PLL.PLLRGE = RCC_PLL1VCIRANGE_3;
  RCC_OscInitStruct.PLL.PLLVCOSEL = RCC_PLL1VCOWIDE;
  RCC_OscInitStruct.PLL.PLLFRACN = 0;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2
                              |RCC_CLOCKTYPE_D3PCLK1|RCC_CLOCKTYPE_D1PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.SYSCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB3CLKDivider = RCC_APB3_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_APB1_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_APB2_DIV1;
  RCC_ClkInitStruct.APB4CLKDivider = RCC_APB4_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
// Check if the GPIO pin corresponds to the emergency stop button
	if (GPIO_Pin == EM_STOP) {
		// Read the current state of the emergency stop button
		endStops.em_stop = HAL_GPIO_ReadPin(GPIOE, EM_STOP);

		// Update the flag based on the state of the emergency stop button
		if (endStops.em_stop == GPIO_PIN_RESET) {
			endStops.em_stop_flag = true;  // Emergency stop button is pressed
		} else {
			endStops.em_stop_flag = false; // Emergency stop button is released
		}
	}

    else if (GPIO_Pin == E_VALVE_OPEN)
    {
		endStops.e_valve_open = HAL_GPIO_ReadPin(GPIOE, E_VALVE_OPEN);

    	if (ENABLE_EXTI_MESSAGES) {
    		HAL_UART_Transmit(&huart3, "VALVE_OPEN_EXTI\n\r", strlen("VALVE_OPEN_EXTI\n\r"), 200);
    	}
    }
    else  if (GPIO_Pin == E_VALVE_CLOSE)
    {
		endStops.e_valve_close = HAL_GPIO_ReadPin(GPIOE, E_VALVE_CLOSE);

    	if (ENABLE_EXTI_MESSAGES) {
    		HAL_UART_Transmit(&huart3, "VALVE_CLOSE_EXTI\n\r", strlen("VALVE_CLOSE_EXTI\n\r"), 200);
    	}
    }
    else if (GPIO_Pin == E_DOOR_H)
    {
    	endStops.e_door_h = HAL_GPIO_ReadPin(GPIOE, E_DOOR_H);

    	if (ENABLE_EXTI_MESSAGES) {
    		HAL_UART_Transmit(&huart3, "LATCH_EXTI\n\r", strlen("LATCH_EXTI\n\r"), 200);
    	}
    }
    else if (GPIO_Pin == E_CRUCIBLE)
    {
    	endStops.e_crucible = HAL_GPIO_ReadPin(GPIOE, E_CRUCIBLE);

    	if (endStops.e_crucible && current_process.current_homing_subprocess != HOME_C && !goingForwardC) {
    		execute_command_from_value(MOTOR, MOTOR_C_STOP_HOME,0);
    	}

    	if (ENABLE_EXTI_MESSAGES) {
    		HAL_UART_Transmit(&huart3, "C_EXTI\n\r", strlen("C_EXTI\n\r"), 200);
    	}
    }
    else if (GPIO_Pin == E_TURRET)
    {
    	endStops.e_turret = HAL_GPIO_ReadPin(GPIOE, E_TURRET);

    	if (endStops.e_turret && current_process.current_homing_subprocess != HOME_T && !goingForwardT) {
    		execute_command_from_value(MOTOR, MOTOR_T_STOP_HOME,0);
    	}

    	if (ENABLE_EXTI_MESSAGES) {
    		HAL_UART_Transmit(&huart3, "T_EXTI\n\r", strlen("T_EXTI\n\r"), 200);
    	}
    }
    else if (GPIO_Pin == E_Z_AXIS)
    {
    	endStops.e_z_axis = HAL_GPIO_ReadPin(GPIOE, E_Z_AXIS);

    	if (endStops.e_z_axis && current_process.current_homing_subprocess != HOME_Z && !goingForwardZ) {
    		execute_command_from_value(MOTOR, MOTOR_Z_STOP_HOME,0);
    	}

    	if (ENABLE_EXTI_MESSAGES) {
    		HAL_UART_Transmit(&huart3, "Z_EXTI\n\r", strlen("Z_EXTI\n\r"), 200);
    	}
    }
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (rxBuffer == '$' && state_rx==0) { // start of command
		cnt_rx = 0;
		state_rx=1;
		memset(queueCommandRx.msg, 0, sizeof queueCommandRx.msg); // reset data
	}
	else if ((cnt_rx > (RX_SIZE - 1)) && (state_rx == 1)) { // corrupted/wrong input (lack of end character)
		state_rx=0;
	}
	else if (rxBuffer == ';') { // end of command detected
		if(state_rx==1){
			queueCommandRx.msg[cnt_rx] = rxBuffer;
			queueCommandRx.len = cnt_rx+1;
			osMessageQueuePut(commandsRxQueueHandle, &queueCommandRx, 1, 0);
		}
		state_rx=0;
	}

	// store character
	if(state_rx==1){
		queueCommandRx.msg[cnt_rx] = rxBuffer;
		cnt_rx++;
	}
	HAL_UART_Receive_IT(&huart3, &rxBuffer, 1); // continue listening
}
/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM6 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM6) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */
  if (htim->Instance == TIM17) {
  	led_blink();
  }
  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
