/*
 * motionGenerator.c
 *
 *  Created on: 14 Sep 2022
 *      Author: MM3D
 */


#include "motionGenerator.h"


motionGenerator_HandleTypeDef motionGenerator_Init(float aMaxVel,float aMaxAcc ,float aInitPos) {
	motionGenerator_HandleTypeDef motionGenerator_;
	motionGenerator_.maxVel = aMaxVel;
	motionGenerator_.maxAcc = aMaxAcc;
	motionGenerator_.initPos = aInitPos;

	// Time variables
	motionGenerator_.oldTime = xTaskGetTickCount();
	motionGenerator_.lastTime = motionGenerator_.oldTime;
	motionGenerator_.deltaTime = 0;

	// State variables
	motion_reset(&motionGenerator_);

	// Misc
	motionGenerator_.signM = 1;		// 1 = positive change, -1 = negative change
	motionGenerator_.shape = true;   // true = trapezoidal, false = triangular
	motionGenerator_.isFinished = false;

	return motionGenerator_;
}


float motion_update(motionGenerator_HandleTypeDef *motionGenerator){

	float posRef=motionGenerator->target;

	if (motionGenerator->oldPosRef != posRef)  // reference changed
	{
		motionGenerator->isFinished = false;
		// Shift state variables
		motionGenerator->oldPosRef = posRef;
		motionGenerator->oldPos = motionGenerator->pos;
		motionGenerator->oldVel = motionGenerator->vel;
		motionGenerator->oldTime = motionGenerator->lastTime;

		// Calculate braking time and distance (in case is neeeded)
		motionGenerator->tBrk = abs(motionGenerator->oldVel) / motionGenerator->maxAcc;
		motionGenerator->dBrk = motionGenerator->tBrk * abs(motionGenerator->oldVel) / 2;

		// Caculate Sign of motion
		motionGenerator->signM = sign_here(posRef - (motionGenerator->oldPos + sign_here(motionGenerator->oldVel)*motionGenerator->dBrk));

		if (motionGenerator->signM != sign_here(motionGenerator->oldVel))  // means brake is needed
		{
			motionGenerator->tAcc = (motionGenerator->maxVel / motionGenerator->maxAcc);
			motionGenerator->dAcc = motionGenerator->tAcc * (motionGenerator->maxVel / 2);
		}
		else
		{
			motionGenerator->tBrk = 0;
			motionGenerator->dBrk = 0;
			motionGenerator->tAcc = (motionGenerator->maxVel - abs(motionGenerator->oldVel)) / motionGenerator->maxAcc;
			motionGenerator->dAcc = motionGenerator->tAcc * (motionGenerator->maxVel + abs(motionGenerator->oldVel)) / 2;
		}

		// Calculate total distance to go after braking
		motionGenerator->dTot = abs(posRef - motionGenerator->oldPos + motionGenerator->signM*motionGenerator->dBrk);

		motionGenerator->tDec = motionGenerator->maxVel / motionGenerator->maxAcc;
		motionGenerator->dDec = motionGenerator->tDec * (motionGenerator->maxVel) / 2;
		motionGenerator->dVel = motionGenerator->dTot - (motionGenerator->dAcc + motionGenerator->dDec);
		motionGenerator->tVel = motionGenerator->dVel / motionGenerator->maxVel;

		if (motionGenerator->tVel > 0)    // trapezoidal shape
			motionGenerator->shape = true;
		else             // triangular shape
		{
			motionGenerator->shape = false;
			// Recalculate distances and periods
			if (motionGenerator->signM != sign_here(motionGenerator->oldVel))  // means brake is needed
			{
				motionGenerator->velSt = sqrt(motionGenerator->maxAcc*(motionGenerator->dTot));
				motionGenerator->tAcc = (motionGenerator->velSt / motionGenerator->maxAcc);
				motionGenerator->dAcc = motionGenerator->tAcc * (motionGenerator->velSt / 2);
			}
			else
			{
				motionGenerator->tBrk = 0;
				motionGenerator->dBrk = 0;
				motionGenerator->dTot = abs(posRef - motionGenerator->oldPos);      // recalculate total distance
				motionGenerator->velSt = sqrt(0.5*motionGenerator->oldVel*motionGenerator->oldVel + motionGenerator->maxAcc*motionGenerator->dTot);
				motionGenerator->tAcc = (motionGenerator->velSt - abs(motionGenerator->oldVel)) / motionGenerator->maxAcc;
				motionGenerator->dAcc = motionGenerator->tAcc * (motionGenerator->velSt + abs(motionGenerator->oldVel)) / 2;
			}
			motionGenerator->tDec = motionGenerator->velSt / motionGenerator->maxAcc;
			motionGenerator->dDec = motionGenerator->tDec * (motionGenerator->velSt) / 2;
		}

	}

	unsigned long time = xTaskGetTickCount();		// current time
	// Calculate time since last set-point change
	motionGenerator->deltaTime = (time - motionGenerator->oldTime);
	// Calculate new setpoint
	motion_calculateTrapezoidalProfile(motionGenerator, posRef);
	// Update last time
	motionGenerator->lastTime = time;

	//calculateTrapezoidalProfile(setpoint);
	return motionGenerator->pos;

}

void motion_calculateTrapezoidalProfile(motionGenerator_HandleTypeDef *motionGenerator, float posRef) {

	float t = (float) (motionGenerator->deltaTime);	// conversion from milliseconds to seconds

	if (motionGenerator->shape)   // trapezoidal shape
	{
		if (t <= (motionGenerator->tBrk+motionGenerator->tAcc))
		{
			motionGenerator->pos = motionGenerator->oldPos + motionGenerator->oldVel*t + motionGenerator->signM * 0.5*motionGenerator->maxAcc*t*t;
			motionGenerator->vel = motionGenerator->oldVel + motionGenerator->signM * motionGenerator->maxAcc*t;
			motionGenerator->acc = motionGenerator->signM * motionGenerator->maxAcc;
		}
		else if (t > (motionGenerator->tBrk+motionGenerator->tAcc) && t < (motionGenerator->tBrk+motionGenerator->tAcc+motionGenerator->tVel))
		{
			motionGenerator->pos = motionGenerator->oldPos + motionGenerator->signM * (-motionGenerator->dBrk + motionGenerator->dAcc + motionGenerator->maxVel*(t-motionGenerator->tBrk-motionGenerator->tAcc));
			motionGenerator->vel = motionGenerator->signM * motionGenerator->maxVel;
			motionGenerator->acc = 0;
		}
		else if (t >= (motionGenerator->tBrk+motionGenerator->tAcc+motionGenerator->tVel) && t < (motionGenerator->tBrk+motionGenerator->tAcc+motionGenerator->tVel+motionGenerator->tDec))
		{
			motionGenerator->pos = motionGenerator->oldPos + motionGenerator->signM * (-motionGenerator->dBrk + motionGenerator->dAcc + motionGenerator->dVel + motionGenerator->maxVel*(t-motionGenerator->tBrk-motionGenerator->tAcc-motionGenerator->tVel) - 0.5*motionGenerator->maxAcc*(t-motionGenerator->tBrk-motionGenerator->tAcc-motionGenerator->tVel)*(t-motionGenerator->tBrk-motionGenerator->tAcc-motionGenerator->tVel));
			motionGenerator->vel = motionGenerator->signM * (motionGenerator->maxVel - motionGenerator->maxAcc*(t-motionGenerator->tBrk-motionGenerator->tAcc-motionGenerator->tVel));
			motionGenerator->acc = - motionGenerator->signM * motionGenerator->maxAcc;
		}
		else
		{
			motionGenerator->pos = posRef;
			motionGenerator->vel = 0;
			motionGenerator->acc = 0;
			motionGenerator->isFinished = true;
		}
	}
	else            // triangular shape
	{
		if (t <= (motionGenerator->tBrk+motionGenerator->tAcc))
		{
			motionGenerator->pos = motionGenerator->oldPos + motionGenerator->oldVel*t + motionGenerator->signM * 0.5*motionGenerator->maxAcc*t*t;
			motionGenerator->vel = motionGenerator->oldVel + motionGenerator->signM * motionGenerator->maxAcc*t;
			motionGenerator->acc = motionGenerator->signM * motionGenerator->maxAcc;
		}
		else if (t > (motionGenerator->tBrk+motionGenerator->tAcc) && t < (motionGenerator->tBrk+motionGenerator->tAcc+motionGenerator->tDec))
		{
			motionGenerator->pos = motionGenerator->oldPos + motionGenerator->signM * (-motionGenerator->dBrk + motionGenerator->dAcc + motionGenerator->velSt*(t-motionGenerator->tBrk-motionGenerator->tAcc) - 0.5*motionGenerator->maxAcc*(t-motionGenerator->tBrk-motionGenerator->tAcc)*(t-motionGenerator->tBrk-motionGenerator->tAcc));
			motionGenerator->vel = motionGenerator->signM * (motionGenerator->velSt - motionGenerator->maxAcc*(t-motionGenerator->tBrk-motionGenerator->tAcc));
			motionGenerator->acc = - motionGenerator->signM * motionGenerator->maxAcc;
		}
		else
		{
			motionGenerator->pos = posRef;
			motionGenerator->vel = 0;
			motionGenerator->acc = 0;
			motionGenerator->isFinished = true;
		}

	}

}
void motion_reset(motionGenerator_HandleTypeDef *motionGenerator){

	motionGenerator->pos 		= motionGenerator->initPos;
	motionGenerator->oldPos 	= motionGenerator->initPos;
	motionGenerator->oldPosRef 	= 0;
	motionGenerator->vel 		= 0;
	motionGenerator->acc 		= 0;
	motionGenerator->oldVel 	= 0;

	motionGenerator->dBrk 		= 0;
	motionGenerator->dAcc 		= 0;
	motionGenerator->dVel 		= 0;
	motionGenerator->dDec 		= 0;
	motionGenerator->dTot 		= 0;

	motionGenerator->tBrk 		= 0;
	motionGenerator->tAcc 		= 0;
	motionGenerator->tVel 		= 0;
	motionGenerator->tDec 		= 0;

	motionGenerator->velSt 		= 0; // motionGenerator->initPos;

	motionGenerator->target		= motionGenerator->initPos; // motion reset
}

bool getFinished(motionGenerator_HandleTypeDef *motionGenerator) {
	return motionGenerator->isFinished;
}

float getVelocity(motionGenerator_HandleTypeDef *motionGenerator) {
	return motionGenerator->vel;
}

float getAcceleration(motionGenerator_HandleTypeDef *motionGenerator) {
	return motionGenerator->acc;
}

void setMaxVelocity(motionGenerator_HandleTypeDef *motionGenerator, float aMaxVel) {
	motionGenerator->maxVel = aMaxVel;
}

void setMaxAcceleration(motionGenerator_HandleTypeDef *motionGenerator, float aMaxAcc) {
	motionGenerator->maxAcc = aMaxAcc;
}

void setInitPosition(motionGenerator_HandleTypeDef *motionGenerator, float aInitPos) {
	motionGenerator->initPos 	= aInitPos;
	motionGenerator->pos 		= aInitPos;
	motionGenerator->oldPos 		= aInitPos;
}


