/*
 * VL53L0x.c
 *
 *  Created on: Jul 24, 2023
 *      Author: damie
 */
#include "vl53L0x.h"

void InitFlask(void)
{
  uint16_t FlaskInitValue = 800;           //need a real steady value
  flaskInitDistance = SingleMeasurement();
  MessageLen = sprintf((char*)Message, "Distance Difference: %i\n\r",abs(FlaskInitValue - flaskInitDistance));
	 HAL_UART_Transmit(&huart3, Message, MessageLen, 100);

  if (abs(FlaskInitValue - flaskInitDistance) <= 50)
  {
	if (ENABLE_SENSOR_INIT_MESSAGES) {
		  HAL_UART_Transmit(&huart3, "FlaskInit_done\n\r", strlen("FlaskInit_done\n\r"), 200);
	}
	InitStatuts = 1;
  }
  else
  {
    InitStatuts = 2;
  }
}

uint16_t SingleMeasurement (void)
{
VL53L0X_PerformSingleRangingMeasurement(Dev, &RangingData);

		  if(RangingData.RangeStatus == 0)
		  {
			    MessageLen = sprintf((char*)Message, "Measured distance: %i\n\r", RangingData.RangeMilliMeter);
			  	 HAL_UART_Transmit(&huart3, Message, MessageLen, 100);
			  return RangingData.RangeMilliMeter;
		  }
		  else
		  {
			  return 0;
		  }
}

uint16_t CheckMeasurement (void)
{
	uint16_t MeasurementOne = SingleMeasurement();
	uint16_t MeasurementTwo = SingleMeasurement();

if (abs(MeasurementOne - MeasurementTwo) <= 100)
 {
     return (MeasurementOne + MeasurementTwo) / 2;
 }
else
 {
     return 0;
 }

}


uint16_t Measurement (void)
{
	 uint16_t measurements[3];
	  uint16_t sum = 0;

	for(int i;i<3;i++)
	{
VL53L0X_PerformSingleRangingMeasurement(Dev, &RangingData);

		  if(RangingData.RangeStatus == 0)
		  {
			  measurements[i] = RangingData.RangeMilliMeter;
		  }
		  else
		     {
		       measurements[i] = 0;
		     }
		  sum += measurements[i];
	}
	 uint16_t average = sum / 3;
	 // MessageLen = sprintf((char*)Message, "Measured distance: %i\n\r", average);
     // HAL_UART_Transmit(&huart3, Message, MessageLen, 100);
	  return average;
}

FLASK_DETECTION_STATUS CheckFlask(void)
{
  uint16_t flaskTestDistance;

  flaskTestDistance = Measurement();

   if ((flaskTestDistance >= (flaskInitDistance - 50)) && (flaskTestDistance <= (flaskInitDistance + 50)) && (flaskTestDistance > 40))
   {
	   return FLASK_DETECTED;

     }
   else if  (flaskTestDistance < 5000)
    {
	   return FLASK_NOT_DETECTED;

      }
   if (flaskTestDistance > 8000)
 		{
	   return SENSOR_ERROR;

 		}
}


