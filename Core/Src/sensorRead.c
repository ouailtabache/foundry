/*
 * sensorRead.c
 *
 *  Created on: Aug 18, 2022
 *      Author: MM3D
 */

#include "sensorRead.h"

sensors sensors_init() {
	sensors Sensors;

	Sensors.Am2320_ = am2320_Init(&hi2c1, AM2320_ADDRESS);
	Sensors.Ads1115_ = ads1115_Init(&hi2c1, ADS1115_ADDRESS);
	encoders_init();

	Sensors.Max31856_1_ = max31856_Init(&hspi2, THERM_1);
	Sensors.Max31856_2_ = max31856_Init(&hspi2, THERM_2);

	HAL_GPIO_WritePin(THERM_1, GPIO_PIN_SET);
	HAL_GPIO_WritePin(THERM_2 , GPIO_PIN_SET); //SPI CS SET

	max31856_beginSetup(&Sensors.Max31856_1_);
	max31856_beginSetup(&Sensors.Max31856_2_);

	return Sensors;
}

void handle_thermocouple_spi_error(char *code[],HAL_StatusTypeDef status_t1, float value){
	char msg_1[64];
	char msgr_2[64];
	sprintf(msg_1, "%s:SPI:", code);
	sprintf(msgr_2, "%s:SPI:DEAD", code);

	char msgr[64];
	strcpy(msgr,"");
	if(status_t1 != HAL_OK){
		strcpy(msgr,msg_1);
		switch(status_t1){
			case HAL_ERROR:
				strcat(msgr,"ERROR");
				break;
			case HAL_BUSY:
				strcat(msgr,"BUSY");
				break;
			case HAL_TIMEOUT:
				strcat(msgr,"TIMEOUT");
				break;
			default:
				strcat(msgr,"UNKNOWN");
				break;
		}
	}
	if(value == 0.0){
		if(strcmp(msgr, "")!=0){
			strcat(msgr,":DEAD");
		}else{
			strcpy(msgr,msgr_2);
		}
	}
	if(strcmp(msgr, "")!=0){
		send_error(sendQueueHandle,msgr);
	}
}
void handle_thermocouple_error(char *code[], uint8_t value){
	char msg_1[64];
	sprintf(msg_1, "%s:TCOUP", code);

	char mssg[64];
	if(value){
		returnStringFromFaultRegister(msg_1,value,&mssg);
		send_error(sendQueueHandle,mssg);
	}
}

void handle_ads1115_i2c_error(uint16_t *pressure, HAL_StatusTypeDef res[]){
	char msg_1[64];
	sprintf(msg_1, "%s:I2C", "ADS1115");

	char msgr[64];
	strcpy(msgr,"");

	if(res[0] != HAL_OK){
		strcpy(msgr,msg_1);
		switch(res[0]){
			case HAL_ERROR:
				strcat(msgr,":Tx1_ERROR");
				break;
			case HAL_BUSY:
				strcat(msgr,":Tx1_BUSY");
				break;
			case HAL_TIMEOUT:
				strcat(msgr,":Tx1_TIMEOUT");
				break;
			default:
				strcat(msgr,":Tx1_UNKNOWN");
				break;
		}
	}

	if(res[1] != HAL_OK){
		switch(res[1]){
			case HAL_ERROR:
				strcat(msgr,":Tx2_ERROR");
				break;
			case HAL_BUSY:
				strcat(msgr,":Tx2_BUSY");
				break;
			case HAL_TIMEOUT:
				strcat(msgr,":Tx2_TIMEOUT");
				break;
			default:
				strcat(msgr,":Tx2_UNKNOWN");
				break;
		}
	}

	if(res[2] != HAL_OK){
		switch(res[2]){
			case HAL_ERROR:
				strcat(msgr,":Rx_ERROR");
				break;
			case HAL_BUSY:
				strcat(msgr,":Rx_BUSY");
				break;
			case HAL_TIMEOUT:
				strcat(msgr,":Rx_TIMEOUT");
				break;
			default:
				strcat(msgr,":Rx_UNKNOWN");
				break;
		}
	}
	if(*pressure == 0){
		strcat(msgr,":DEAD");
	}

	if(strcmp(msgr, "")!=0){
		send_error(sendQueueHandle,msgr);
	}

}


void handle_am2320_i2c_error( float *temperature, float *humidity,  HAL_StatusTypeDef res[]){
	char msg_1[64];
	sprintf(msg_1, "%s:I2C", "AM2320");

	char msgr[64];
	strcpy(msgr,"");

//	if(res[0] != HAL_OK){
//		switch(res[0]){
//			case HAL_ERROR:
//				strcat(msgr,":Tx1_ERROR");
//				break;
//			case HAL_BUSY:
//				strcat(msgr,":Tx1_BUSY");
//				break;
//			case HAL_TIMEOUT:
//				strcat(msgr,":Tx1_TIMEOUT");
//				break;
//			default:
//				strcat(msgr,":Tx1_UNKNOWN");
//				break;
//		}
//	}

	if(res[1] != HAL_OK){
		strcpy(msgr,msg_1);
		switch(res[1]){
			case HAL_ERROR:
				strcat(msgr,":Tx2_ERROR");
				break;
			case HAL_BUSY:
				strcat(msgr,":Tx2_BUSY");
				break;
			case HAL_TIMEOUT:
				strcat(msgr,":Tx2_TIMEOUT");
				break;
			default:
				strcat(msgr,":Tx2_UNKNOWN");
				break;
		}
	}

	if(res[2] != HAL_OK){
		switch(res[2]){
			case HAL_ERROR:
				strcat(msgr,":Rx_ERROR");
				break;
			case HAL_BUSY:
				strcat(msgr,":Rx_BUSY");
				break;
			case HAL_TIMEOUT:
				strcat(msgr,":Rx_TIMEOUT");
				break;
			default:
				strcat(msgr,":Rx_UNKNOWN");
				break;
		}
	}

	if(res[3] != 0){
		strcat(msgr,":REG_0_1");
	}

	if(*temperature == 0){
		strcat(msgr,":TEMP_DEAD");
	}


	if(*humidity == 0){
		strcat(msgr,":HUM_DEAD");
	}

	if(strcmp(msgr, "")!=0){
		send_error(sendQueueHandle,msgr);
	}
}

void resetVariables(foundry *Foundry){
	Foundry->pressure=-1;
	Foundry->ambient_temp=-1;
	Foundry->humidity=-1;
}

foundry sensors_read(sensors *Sensors, foundry *Foundry) {

	resetVariables(Foundry);

	HAL_StatusTypeDef res_ads[3];
	ads1115_GetValue_v2(&Sensors->Ads1115_, &Foundry->pressure, res_ads); 										/* read pressure */
	handle_ads1115_i2c_error(&Foundry->pressure, res_ads);

	HAL_StatusTypeDef res_am[4];
	am2320_GetTemperatureAndHumidity_v2(&Sensors->Am2320_, &Foundry->ambient_temp, &Foundry->humidity, res_am); 	/* read humidity */										/* read pressure */
	handle_am2320_i2c_error(&Foundry->ambient_temp, &Foundry->humidity, res_am);

	HAL_StatusTypeDef status_t1 = max31856_GetThermocouple(&Sensors->Max31856_1_,&Foundry->thermocouplesArray[0]);
	handle_thermocouple_spi_error("T1",status_t1,Foundry->thermocouplesArray[0]);

	HAL_StatusTypeDef status_t2 = max31856_GetThermocouple(&Sensors->Max31856_2_,&Foundry->thermocouplesArray[1]);
	handle_thermocouple_spi_error("T2",status_t1,Foundry->thermocouplesArray[1]);

	max31856_readFault(&Sensors->Max31856_1_,&Foundry->faultThermocouplesArray[0]);
	handle_thermocouple_error("T1",Foundry->faultThermocouplesArray[0]);

	max31856_readFault(&Sensors->Max31856_2_,&Foundry->faultThermocouplesArray[1]);
	handle_thermocouple_error("T2",Foundry->faultThermocouplesArray[1]);

	return *Foundry;
}
