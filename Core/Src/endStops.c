#include "endStops.h"

void initStateInterrupts(end_stops* endStops){
	endStops->e_z_axis=false;
	endStops->em_stop=false;
	endStops->e_valve_open=false;
	endStops->e_valve_close=false;
	endStops->e_door_h=false;
	endStops->e_turret=false;
	endStops->e_crucible=false;

	endStops->em_stop_flag = false;
}

void updateStateInterrupts(end_stops* endStops) {
	endStops->e_z_axis = endStops->e_z_axis=HAL_GPIO_ReadPin(GPIOE, E_Z_AXIS);
	endStops->em_stop = endStops->em_stop=HAL_GPIO_ReadPin(GPIOE, EM_STOP);;
	endStops->e_valve_open = endStops->e_valve_open=HAL_GPIO_ReadPin(GPIOE, E_VALVE_OPEN);
	endStops->e_valve_close = endStops->e_valve_close=HAL_GPIO_ReadPin(GPIOE, E_VALVE_CLOSE);
	endStops->e_door_h = endStops->e_door_h=HAL_GPIO_ReadPin(GPIOE, E_DOOR_H);
	endStops->e_turret = endStops->e_turret=HAL_GPIO_ReadPin(GPIOE, E_TURRET);
	endStops->e_crucible = endStops->e_crucible=HAL_GPIO_ReadPin(GPIOE, E_CRUCIBLE);
}

void printStates(osMessageQueueId_t sendQueueHandle, end_stops* endStops){
	uint8_t tmp[50];
	sprintf(tmp, "E_Z_AXIS:%i - ", endStops->e_z_axis);
	osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
	sprintf(tmp, "EM_STOP:%i - ", endStops->em_stop);
	osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
	sprintf(tmp, "E_VALVE_OPEN:%i - ", endStops->e_valve_open);
	osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
	sprintf(tmp, "E_VALVE_CLOSE:%i - ", endStops->e_valve_close);
	osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
	sprintf(tmp, "E_DOOR_H:%i - ", endStops->e_door_h);
	osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
	sprintf(tmp, "E_TURRET:%i - ", endStops->e_turret);
	osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
	sprintf(tmp, "E_CRUCIBLE:%i \n\r", endStops->e_crucible);
	osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
}


