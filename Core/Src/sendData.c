/*
 * sendData.c
 *
 *  Created on: Aug 17, 2022
 *  updated on: Mai 29, 2023 | Mohamed
 *      Author: MM3D | awitc
 *
 */

#include "sendData.h"

void send_acknowledge(osMessageQueueId_t sendQueueHandle, const char cmd[]) {
	if(ENABLE_ACK_MESSAGES) {
//		uint8_t tmp[MSG_SIZE]="";
		char tmp[MSG_SIZE]="";

		sprintf(tmp, "$ACK:%s;\r\n", cmd);
		osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
	}
}

void send_error(osMessageQueueId_t sendQueueHandle, const char error[]) {
	if(ENABLE_ERR_MESSAGES) {
//		uint8_t tmp[MSG_SIZE];
		char tmp[MSG_SIZE];
		sprintf(tmp, "$ERR:%s;\r\n", error);
		osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
	}
}

void send_echo(osMessageQueueId_t sendQueueHandle, const char user_cmd[]) {
	if(ENABLE_ECHO_MESSAGES){
//		uint8_t tmp[MSG_SIZE];
		char tmp[MSG_SIZE];
		sprintf(tmp, "$ECHO:%s\r\n", user_cmd);
		osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
	}
}

void send_value(osMessageQueueId_t sendQueueHandle, double value) {
	if(ENABLE_VALUE_MESSAGES){
//		uint8_t tmp[MSG_SIZE];
		char tmp[MSG_SIZE];

		sprintf(tmp, "$VALUE:%lf\r\n", value);
		osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
	}
}

void send_values(osMessageQueueId_t sendQueueHandle, double value, double value2) {
	if(ENABLE_VALUE_MESSAGES){
//		uint8_t tmp[MSG_SIZE];
		char tmp[MSG_SIZE];

		sprintf(tmp, "$VALUE1:%lf\tVALUE2:%lf\t\r\n", value, value2);
		osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
	}
}

void send_values_csv_format(osMessageQueueId_t sendQueueHandle, double value, double value2) {
	if(ENABLE_VALUE_MESSAGES){
//		uint8_t tmp[MSG_SIZE];
		char tmp[MSG_SIZE];

		sprintf(tmp, "%lf,%lf;\r\n", value, value2);
		osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
	}
}

void send_data(osMessageQueueId_t sendQueueHandle, foundry *Foundry) {
//	uint8_t tmp[MSG_SIZE];
	char tmp[MSG_SIZE]="";

	if (ENABLE_DATA_MESSAGES) {
		sprintf(tmp, "$DATA:%d:", Foundry->pressure);
		osMessageQueuePut(sendQueueHandle, &tmp, 0, 200);
		sprintf(tmp, "%.2f:", Foundry->ambient_temp);
		osMessageQueuePut(sendQueueHandle, &tmp, 0, 200);
		sprintf(tmp, "%.2f:", Foundry->humidity);
		osMessageQueuePut(sendQueueHandle, &tmp, 0, 200);
		sprintf(tmp, "%.2f:", Foundry->loadcell_reading);
		osMessageQueuePut(sendQueueHandle, &tmp, 0, 200);

		sprintf(tmp, "%.2f:", Foundry->thermocouplesArray[0]);						/* thermocouple 1*/
		osMessageQueuePut(sendQueueHandle, &tmp, 0, 200);
		sprintf(tmp, "%.2f;\r\n", Foundry->thermocouplesArray[1]);					/* thermocouple 2*/
		osMessageQueuePut(sendQueueHandle, &tmp, 0, 200);
	}

//	if (ENABLE_DATA_MESSAGES) {
//		sprintf(tmp, "$DATA:%d:", 1000);
//		osMessageQueuePut(sendQueueHandle, &tmp, 0, 200);
//		sprintf(tmp, "%.2f:", 25.05);
//		osMessageQueuePut(sendQueueHandle, &tmp, 0, 200);
//		sprintf(tmp, "%.2f:", 37.17);
//		osMessageQueuePut(sendQueueHandle, &tmp, 0, 200);
//		sprintf(tmp, "%.2f:", 0.0);
//		osMessageQueuePut(sendQueueHandle, &tmp, 0, 200);
//
//		sprintf(tmp, "%.2f:", 30.01);						/* thermocouple 1*/
//		osMessageQueuePut(sendQueueHandle, &tmp, 0, 200);
//		sprintf(tmp, "%.2f;\r\n", 30.02);					/* thermocouple 2*/
//		osMessageQueuePut(sendQueueHandle, &tmp, 0, 200);
//	}
}

void send_response(osMessageQueueId_t sendQueueHandle, const char response[]) {
	char tmp[MSG_SIZE];

	sprintf(tmp, "$RESP:%s;\r\n", response);
	osMessageQueuePut(sendQueueHandle, &tmp, 1, 200);
}


void errMessage(osMessageQueueId_t sendQueueHandle){

	if(!is_com_enc_C){
		char msg_1[15];
		sprintf(msg_1, "%s:SPI1", "ENC_C");
		send_error(sendQueueHandle,msg_1);
	}

	if(!is_com_enc_T){
		char msg_1[15];
		sprintf(msg_1, "%s:SPI1", "ENC_T");
		send_error(sendQueueHandle,msg_1);
	}

	if(!is_com_enc_Z){
		char msg_1[15];
		sprintf(msg_1, "%s:SPI1", "ENC_Z");
		send_error(sendQueueHandle,msg_1);
	}

	if (!correct_setup_C) {
	    char msg_1[30];
	    snprintf(msg_1, sizeof(msg_1), "%s:SPI1", "ENC_C_NOT_SETUP");
	    send_error(sendQueueHandle, msg_1);
	}

	if (!correct_setup_T) {
	    char msg_1[30];
	    snprintf(msg_1, sizeof(msg_1), "%s:SPI1", "ENC_T_NOT_SETUP");
	    send_error(sendQueueHandle, msg_1);
	}

	if (!correct_setup_Z) {
	    char msg_1[30];
	    snprintf(msg_1, sizeof(msg_1), "%s:SPI1", "ENC_Z_NOT_SETUP");
	    send_error(sendQueueHandle, msg_1);
	}





	if(ENABLE_PID_CONTROL){
		if(!enable_temp_pid_C || !enable_abs_pid_C){
			char msg_1[15];
			sprintf(msg_1, "%s:OFF", "PID_C");
			send_error(sendQueueHandle,msg_1);
		}
	}

	if(ENABLE_PID_CONTROL){
		if(!enable_temp_pid_T || !enable_abs_pid_T){
			char msg_1[15];
			sprintf(msg_1, "%s:OFF", "PID_T");
			send_error(sendQueueHandle,msg_1);
		}
	}

	if(ENABLE_PID_CONTROL){
		if(!enable_temp_pid_Z || !enable_abs_pid_Z){
			char msg_1[15];
			sprintf(msg_1, "%s:OFF", "PID_Z");
			send_error(sendQueueHandle,msg_1);
		}
	}

}
