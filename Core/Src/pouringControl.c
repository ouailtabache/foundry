/*
 * pouringControl.c
 *
 * In this file is presented all the code related with the pouring control.
 * Also, all the equations that model the crucible and were tested in Matlab/Simulink.
 *
 *  Created on: 29 Sep 2022
 *      Author: MM3D | DAVID QUEZADA
 */
#include "pouringControl.h"
#include <math.h>
#include <define.h>

pouringControl_HandleTypeDef pouringControl_Init() {
	pouringControl_HandleTypeDef pouringControl_;
	pouringControl_.hDetected = 0;
	pouringControl_.thetaStart = 0;
	pouringControl_.initialVolume = 0;

	pouringControl_.dh_coeff = 1;
	pouringControl_.deltaTheta = 0.2;

	// Variables for flow rate generator
	pouringControl_.Q   = DEFAULT_Q;
	pouringControl_.Tr  = DEFAULT_TST;
	pouringControl_.Tst = DEFAULT_TR;

	// Variables for main loop
	pouringControl_.theta 	= pouringControl_.thetaStart;
	pouringControl_.h 		= 0;
	pouringControl_.omega	= 0;
	pouringControl_.dt_h	= 0;

	// Variables for integrals
	pouringControl_.newTime = 0;
	pouringControl_.oldTime = 0;

	return pouringControl_;
}

/**
 * @	Calculate initial Volume
 * @ This function is in charge to calculate the initial volume according with the height
 * detected of the liquid of the surface.Considering the distance from the surface of the
 * liquid to the edge of the crucible's lip. The value of the height should be considered in mm
 * and considered within the range.
 *
 * @ float heightDetected
 *
 *
 */
float calculateInitialVolume( float heightDetected) {
   float initialVolumeTemp = 0;
   float p1, p2;

   // It force the height to be within the range
	if (H_DETECTED_MAX < heightDetected){
		heightDetected = 243;
	}

	if (heightDetected < H_DETECTED_MAX && H_DETECTED_MIN<=heightDetected){
	   p1 = -98930 ;
	   p2 =  24110000;
	   initialVolumeTemp = p1*heightDetected + p2;
	} else{
		initialVolumeTemp = 0;
	}
	return initialVolumeTemp;
}

/**
 * @	Calculate initial Pouring Angle     Theta_Pouring(Volumen_Initial)
 * @ This function is in charge to calculate the initial pouring angle according with the
 * initial volume of the crucible. The value of the angle should be considered in degrees
 * and the result is given in mm^3.
 * (The function is divided in three sections)
 *
 * @ float initialVolume
 *
 *
 */
float calculateInitialPouringAngle(float initialVolume){
	float initialThetaTemp = 0;
	float p1, p2, p3, p4;


	if(0<=initialVolume && initialVolume<=9778300){
		p1 =  -0.0000000000000000000142;
		p2 =   0.0000000000003892;
		p3 =    -0.0000065;
		p4 =       89.42;

		initialThetaTemp = p1*pow(initialVolume,3) + p2*pow(initialVolume,2) + p3*initialVolume + p4;
	}else if (9778000 <initialVolume  && initialVolume<13700000){
		p1 =  -0.00000000000000000001956;
		p2 =   0.0000000000006814;
		p3 =  -0.00001037;
		p4 =       104.5;
		initialThetaTemp = p1*pow(initialVolume,3) + p2*pow(initialVolume,2) + p3*initialVolume + p4;
	}else if(13700000 <initialVolume  && initialVolume<24110000){
		p1 =   0.000000000000000000003302;
		p2 =  -0.0000000000002912;
		p3 =   0.000003536;
		p4 =       37.72;

		initialThetaTemp = p1*pow(initialVolume,3) + p2*pow(initialVolume,2) + p3*initialVolume + p4;
	}else{
		initialThetaTemp = 0;
	}

	float Correction=0;
	float volume=initialVolume/1000000;
	if(0<=volume && volume<1.8){
			Correction=0.72;//this has not been tested, just hypothesis
		}
	else if (1.8<=volume  && volume<=9.86){
			p1=0.00639303;
			p2=-0.13843267;
			p3=0.797212;
			p4=-0.30617;
		    Correction = p1*pow(volume,3) + p2*pow(volume,2) + p3*volume + p4;
		}
	else if(9.86 <volume  && volume<18.62){
			p1=-0.00033066;
			p2=0.00757715;
			p3=-0.0338476;
			p4=0.13794287;
		    Correction = p1*pow(volume,3) + p2*pow(volume,2) + p3*volume + p4;
		}
	else{
			Correction = 0;//+-0.2
		}

	initialThetaTemp=initialThetaTemp+Correction;

	return initialThetaTemp;
}

/**
 * @	float Calculate Volume under the pouring lip surface Vs(theta)
 * @ This function is in charge to calculate the volume that is under the lip surface when
 * the crucible is at theta position. The value of the angle should be considered in degrees
 * and the result is given in mm^3.
 * (The function is divided in three sections)
 *
 * @ float theta
 *
 *
 */
float calculateVs( float theta ){
	float Vs = 0;
	float p1, p2, p3, p4;

	if (0<=theta && theta<40){
		p1 =	-37.86 ;
		p2 =	575.1 ;
		p3 =	-222100 ;
		p4 =	24130000  ;

		Vs = p1*pow(theta,3) + p2*pow(theta,2) + p3*theta + p4;
	}else if (40 <= theta && theta<50){
		p1 =      -389.3 ;
		p2 =  -364700 ;
		p3 =   28970000 ;

		Vs = p1*pow(theta,2) + p2*theta + p3;
	}else if(50<=theta && theta<90){
		p1 =        2857;
		p2 =   -643300;
		p3 =   34740000;

		Vs = p1*pow(theta,2) + p2*theta + p3;
	}else{

		Vs = 0;
	}


	return Vs;
}


/*
 * @	double Calculate Volume over the pouring lip surface Vr(theta,h)
 *
 * @ This function is in charge to calculate the volume over the pouring lip at specific angular position
 * theta and given height. The value of the angle should be considered in degrees
 * and the height in mm. The result is given in mm^3
 * (The function is divided in three sections)
 *
 * @ float theta
 *
 * @ float h
 *
 * */
double calculateVr(float theta, float h){
	double Vr = 0;
	float p00, p10, p01, p20, p11;

	if (0<=theta && theta<40){
	       p00 =	54020;
	       p10 =    -8759;
	       p01 =    94600;
	       p20 =    230.5;
	       p11 =    674;

	       Vr = p00 + p10*theta + p01*h + p20*pow(theta,2) + p11*theta*h;
	}else if (40 <= theta && theta<50){
	       p00 =  -3100000;
	       p10 =   145500;
	       p01 =   68250;
	       p20 =  -1702;
	       p11 =   1543;

		   Vr = p00 + p10*theta + p01*h + p20*pow(theta,2) + p11*theta*h;
	}else if(50<=theta && theta<90){
	       p00 =   54180;
	       p10 =  -2329;
	       p01 =   207700;
	       p20 =   13.27;
	       p11 =  -1388;

		   Vr = p00 + p10*theta + p01*h + p20*pow(theta,2) + p11*theta*h;
	}else{
		   Vr = 0;
	}

	return Vr;
}

/*
 * @	double Calculate partial derivative of the Volume over the pouring lip surface with respect h dh_Vr(theta)
 * 	   (It is equal to the function generated considering the area of the pouring lip surface at 1 mm height)
 *
 * @ This function is in charge to calculate the derivative of the Volume over the pouring lip surface
 * with respect h at theta. The value of the angle should be considered in degrees
 * and the height in mm. The result is given in mm^3
 * (The function is divided in three sections)
 *
 * @ float theta
 *
 *
 * */
double calculateDh_Vr(float theta){
	double p1, p2, p3,p4;
	double a1,b1,c1;
	double a,b,c,d;
	double dh_Vr;

	if (0<=theta && theta<40){

	       p1 =      0.2148 ;
	       p2 =       8.745 ;
	       p3 =       61.09 ;
	       p4 =   	  98960 ;

	    dh_Vr = p1*pow(theta,3) + p2*pow(theta,2) + p3*theta + p4;

	}else if (40 <= theta && theta<50){
	       a1 =  	  136100 ;
	       b1 =      0.0605  ;
	       c1 =       11.39  ;

	    dh_Vr = a1*sin(b1*theta+c1);
	}else if(50<=theta && theta<90){
	       a =   -0.0000784;
	       b =       0.2128;
	       c =       271100;
	       d =    -0.01445 ;

	    dh_Vr = a*exp(b*theta) + c*exp(d*theta);
	}else{
	    dh_Vr = 0;
	}

	return dh_Vr;
}


/*
 * @	float Calculate partial derivative of the Volume over the pouring lip surface with respect theta dtheta_Vr(theta,height)
 *
 * @ This function is in charge to calculate the derivative of the Volume over the pouring lip surface
 * with respect theta at h. The value of the angle should be considered in degrees
 * and the height in mm.
 *
 * @ float theta
 * @ float height
 *
 *
 * */
float calculateDtheta_Vr(pouringControl_HandleTypeDef *pouringControl, float theta, float h){
	float  dtheta_Vr = 0;
	float Vr, Vr_next;
	float dtheta = pouringControl->deltaTheta;

	Vr  = calculateVr(theta, h);
	Vr_next = calculateVr(theta+dtheta, h);

	dtheta_Vr = (Vr_next - Vr)/dtheta;


	return dtheta_Vr;
}

/*
 * @	float Calculate partial derivative of the Volume under the pouring lip surface with respect theta dtheta_Vs(theta)
 *
 * @ This function is in charge to calculate the derivative of the Volume under the pouring lip surface
 * with respect theta. The value of the angle should be considered in degrees
 * and the height in mm.
 *
 * @ float theta
 *
 * */
float calculateDtheta_Vs(pouringControl_HandleTypeDef *pouringControl, float theta){
	float dtheta_Vs = 0;
	float Vs, Vs_next;
	float dtheta = pouringControl->deltaTheta;

	Vs = calculateVs(theta);
	Vs_next	= calculateVs(theta+dtheta);

	dtheta_Vs = (Vs_next - Vs)/dtheta;

	return dtheta_Vs;
}

/*
 * @	float Calculate time derivative of
 *
 * @ This function is in charge to calculate the derivative of the height over the pouring lip.
 * The variables should be passed to the function in a dt_H_variables structure.
 *
 * @ dt_H_HandleTypeDef dt_H_variables
 *
 * */

double calculateDt_H(pouringControl_HandleTypeDef *pouringControl, float t){

	double dt_H=0;
    float Tr  = pouringControl->Tr;
    float Tst = pouringControl->Tst;

    double B=0.64705841;
    double coef=pouringControl->dh_coeff;
//    coef=A*pow((Q/2),B)/pow(flow_rate_coefficient,B);//should be calculated only once
    if (t==0){
        dt_H=0;
    }else if(t<Tr){
        double var= PI*t/Tr;
        dt_H=coef*sin(var)*pow((1-cos(var)),(B-1));
        //dt_H=0;
    }else if(t>=Tr && t<=Tr+Tst){
        dt_H=0;
    }else if(t>Tr+Tst && t<2*Tr+Tst){
        double var=PI*(t-Tr-Tst)/Tr;
        dt_H=-coef*sin(var)*pow((1+cos(-var)),(B-1));
    }else{
        dt_H=0;
    }
    //pouringControl->dt_h=dt_H;
	return dt_H;
}


/*
 * @	float Calculate the Pouring's Inverse Model
 *
 * @ This function is in charge to calculate the pouring's inverse model giving as output the
 * angular velocity that the crucible motor should perform in order to generate the desired flowrate.
 *
 *
 *
 * @ OmegaRef_HandleTypeDef OmegaRef_variables
 *
 * */

float calculateOmegaRef(pouringControl_HandleTypeDef *pouringControl, OmegaRef_HandleTypeDef *OmegaRef_variables){
	float q_ref 	= OmegaRef_variables->q_ref;
	float dtheta_Vr = OmegaRef_variables->dtheta_Vr;
	float dtheta_Vs = OmegaRef_variables->dtheta_Vs;
	float dh_Vr 	= OmegaRef_variables->dh_Vr;
	float dt_h		= OmegaRef_variables->dt_h;
	float omega_ref = 0;
	float NUM, DEN;

	NUM = q_ref + (dt_h*dh_Vr);
    //NUM = q_ref;
    DEN = dtheta_Vr + dtheta_Vs;
    if (DEN==0){
    	omega_ref=0;
    }
    else{
    	omega_ref = -(NUM/DEN);
    }

	return omega_ref;
}

/*
 * @	float Flow rate Generator
 *
 * @ This function is in charge to generate a smooth pattern of flow rate that the pouring control
 * should follow as a reference. The time should be given in seconds. (Important!)
 * The constants  TimeRamp (Tr) and Time stable (Tst) has to be modified in a separate function.
 * for now the default values are 3 s and 5 s respectively.
 *
 * @ float time
 *
 * */
float generateFlowRate(pouringControl_HandleTypeDef *pouringControl,float t){

	float Tr  = pouringControl->Tr;
	float Tst = pouringControl->Tst;
	float Q   = pouringControl->Q;
	float q_ref = 0;

	if (0<=t && t<=Tr){
	    q_ref = 0.5*Q*(1-cos(PI*t/Tr));

	}else if (Tr<=t && t<=Tst+Tr){
	    q_ref = Q;

	}else if(Tst+Tr<t && t<=Tst+2*Tr){
	    q_ref = 0.5*Q*(1+cos(PI*(t-Tst-Tr)/Tr));

	}else{
	    q_ref = 0;

	}
	return q_ref;
}




float calculateIntegralOmega(pouringControl_HandleTypeDef *pouringControl, float Omega, float theta){
	float integral;
	float dt;
	dt = pouringControl->newTime - pouringControl->oldTime;

	integral = theta +(Omega*dt);


	return integral;
}


float calculateIntegralDh(pouringControl_HandleTypeDef *pouringControl, float Dh, float h){
	float integral;
	float dt;
	dt = pouringControl->newTime - pouringControl->oldTime;

	integral = h + (Dh*dt);

	return integral;
}

/*
 * @	float Update Theta Profile
 *
 * @ This function is in charge to compute the needed trajectory of Theta crucible
 * To obtain the desired poured volume and flow rate according with the equations of the crucible's
 * shape previously obtained. This function must be running in a thread with high priority and
 * The time should be given in seconds. (Important!)
 *
 * (Aqui esta lo chido)
 *
 *
 * @ float time
 *
 * */
float updateThetaProfile(pouringControl_HandleTypeDef *pouringControl, float t){

	OmegaRef_HandleTypeDef OmegaRef_variables_;
//	dt_H_HandleTypeDef  dt_H_variables_;

	//float q_ref = flow_rate_coefficient * generateFlowRate(pouringControl, t); 	// 1.1 worked for 5liters
	float q_ref = generateFlowRate(pouringControl, t); 	// 1.1 worked for 5liters

	float dh_Vr 	= calculateDh_Vr(pouringControl->theta);
	float dtheta_Vs = calculateDtheta_Vs(pouringControl, pouringControl->theta);
	float dtheta_Vr = calculateDtheta_Vr(pouringControl, pouringControl->theta, pouringControl->h);

	pouringControl->oldTime = pouringControl->newTime;
	pouringControl->newTime = t;

//	dt_H_variables_.dh_Vr		= dh_Vr;
//	dt_H_variables_.dtheta_Vr	= dtheta_Vr;
//	dt_H_variables_.dtheta_Vs	= dtheta_Vs;
//	dt_H_variables_.h			= pouringControl->h;
//	dt_H_variables_.omega_ref	= pouringControl->omega;
//	dt_H_variables_.q_ref		= q_ref;

	pouringControl->q_ref = q_ref;

//	dt_H_variables_.theta		= pouringControl->theta;

	pouringControl->dt_h = calculateDt_H(pouringControl, t);

	//Calculate integral and update h value
	pouringControl->h = calculateIntegralDh(pouringControl, pouringControl->dt_h, pouringControl->h);

	OmegaRef_variables_.dh_Vr 			= dh_Vr;
	OmegaRef_variables_.dt_h			= pouringControl->dt_h;
	OmegaRef_variables_.dtheta_Vr		= dtheta_Vr;
	OmegaRef_variables_.dtheta_Vs		= dtheta_Vs;
	OmegaRef_variables_.q_ref			= q_ref;

	pouringControl->omega = calculateOmegaRef(pouringControl, &OmegaRef_variables_);

	//Calculate integral and update theta value
	pouringControl->theta = calculateIntegralOmega(pouringControl, pouringControl->omega, pouringControl->theta);
	if (t>=pouringControl->Tr + pouringControl->Tst){
		return pouringControl->theta -0.5;
	}

	return pouringControl->theta;
}

/*
 * @	int set Pouring Parameters
 *
 * @ This function is in charge to set the parameters to the flow Rate generator. The units for Q(mm^3/s) Tst,Tr (s)
 *	 if the given values are out of the limits it will return 0
 *	 if are ok it will return 1
 *
 * @ pouringControl_HandleTypeDef *pouringControl
 * @ float Q
 * @ float Tr
 * @ float Tst
 *
 * */
int setPouringParameters(pouringControl_HandleTypeDef *pouringControl, float Q, float Tr, float Tst){
	float volumePart = Q * Tst;
	if(Q_MAX <= Q && VOLUME_MAX <= volumePart ){
		//Return 0 in case the parameter is correct
		return 0;
	}else{
		// Variables for flow rate generator
		pouringControl->Q   = Q;
		pouringControl->Tr  = Tr;
		pouringControl->Tst = Tst;

        double A=0.00147774151;
        double B=0.64705841;
        double coef=0;
        coef=PI*B*A*pow((Q/(2*flow_rate_coefficient)),B)/Tr;//should be calculated only once
        pouringControl->dh_coeff = coef;

		return 1;
	}
}

/*
 * @	int Set Initial angle
 *
 * @ This function is in charge to set the parameters to the initial angle.
 * The value must be given in degrees in a range from  0 to 90.
 * If the value is within range will return 1, otherwise will return 0.
 *
 * @ pouringControl_HandleTypeDef *pouringControl
 * @ float init_angle
 *
 * */
int setInitialAngle(pouringControl_HandleTypeDef *pouringControl, float init_angle){
	// Variables for main loop
	pouringControl->h 		= 0;
	pouringControl->omega	= 0;
	pouringControl->dt_h	= 0;

	// Variables for integrals
	pouringControl->newTime = 0;
	pouringControl->oldTime = 0;

	if(init_angle> -0.1 && init_angle<=90){
		pouringControl->thetaStart = init_angle;
		pouringControl->theta 	= pouringControl->thetaStart;
		return 1;
	}else{
		pouringControl->thetaStart = 0;
		return 0;
	}
}


/*Saludos cordiales*/
