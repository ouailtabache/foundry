/*
 * Processes_2.c
 *
 *  Created on: 21 gru 2022
 *      Author: MM3D
 */

#include "processHandler.h"

extern int material_count;

void process_init(Process *process) {
	process->state = AVAILABLE;
	process->current = IDLE;

	process->current_homing_subprocess = HOMING_IDLE;
	process->current_pouring_subprocess = POURING_IDLE;
	process->current_VOLUME_DETECTION_subprocess = VOLUME_DETECTION_IDLE;
	process->current_degassing_subprocess = DEGASSING_IDLE;
	process->current_adding_subprocess = ADDING_IDLE;
	process->current_nozzle_subprocess = NOZZLE_IDLE;
	process->current_emptying_subprocess = EMPTY_IDLE;
	process->current_crucible_change_subprocess = CRUCIBLE_CHANGE_IDLE;
	process->current_cover_crucible_subprocess = COVER_CRUCIBLE_IDLE;

	/* flags for acknowledgment messages */
	process->flag.ack_process = false;
	process->flag.ack_subprocess = false;

	/* initialize flags */
	process->flag.ack_homing_C = false;
	process->flag.ack_homing_T = false;
	process->flag.ack_homing_Z = false;
	process->flag.motor_start_home = false;

	process->flag.initial_endstop_check = false;
	process->flag.motor_backed_off = false;
	process->flag.motor_initially_not_in_endstop = false;

	process->flag.home_offset_C_start = false;
	process->flag.home_offset_T_start = false;
	process->flag.home_offset_Z_start = false;

	process->flag.degassing_pressure_fault = false;
	process->flag.degassing_reset = false;
	process->flag.degassing_pattern_start = false;
	process->flag.degassing_pattern_done = false;
	process->flag.motor_T_left_pos = false;
	process->flag.motor_T_right_pos = false;
	process->flag.motor_Z_top_pos = false;
	process->flag.motor_Z_down_pos = false;
	process->flag.motor_T_init_pos = false;
	process->flag.motor_T_cover_pos = false;

	process->flag.valve_open = false;
	process->flag.degassing_init_pos_reached = false;

	process->flag.motor_T_overlap = false;
	process->flag.motor_Z_overlap = false;

	/* adding */
	process->flag.latch_open = false;
	process->flag.latch_released = false;
	process->flag.motor_Z_adding_pos=false;

	/* nozzle change */
	process->flag.nozzle_change_done = false;

	/* remaining variables */
	process->t_pouring_generator_start = 0;
	process->pressure_reading = 0;

	process->t_degassing_duration = 0;
	process->t_degassing_start = 0;


	process->flag.motor_T_cover_crucible_pos = false;

	process->flag.emptying_done = false;

	process->flag.motor_C_empty_pos = false;
	process->flag.motor_C_init_pos = false;
	process->flag.motor_C_change_pos = false;

	process->flag.cover_crucible_pos_check = false;

	process->flag.loadcells_tared = false;

	bool em_stop_flag_init = HAL_GPIO_ReadPin(GPIOE, EM_STOP);

		// Check if the emergency stop button is pressed initially
		if (!em_stop_flag_init) {
		    // Perform emergency process (assuming "process" is the relevant data or function)
		    Emergency_process(process);
		}

		// Check if the emergency stop button is pressed initially and if the emergency message hasn't been transmitted yet
		if (!em_stop_flag_init && !endStops.emergencyMessageTransmitted_init) {
		    // Transmit the emergency message over UART
		    const char* message = "EMERGENCY BUTTON PRESSED INITIALLY \n\r";
		    HAL_UART_Transmit(&huart3, (uint8_t*)message, strlen(message), 200);

		    // Update the flag to indicate that the emergency message has been transmitted
		    endStops.emergencyMessageTransmitted_init = true;
		}

		// Check if the emergency stop button is released
		if (em_stop_flag_init) {
		    // Indicate the system status using LED signals
		    HAL_GPIO_WritePin(LED_GREEN, GPIO_PIN_SET);
		    HAL_GPIO_WritePin(LED_PANEL, GPIO_PIN_RESET);
		    HAL_GPIO_WritePin(LED_RED, GPIO_PIN_RESET);
		}

}

void processHandler(Process *process) {

	switch (process->current)
	{
		case IDLE:
			/* DO NOTHING - CAN ACCEPT NEW PROCESSES */
			if (!process->flag.ack_process) {
				send_acknowledge(sendQueueHandle, "FOUNDRY_IDLE");
				process->flag.ack_process = true;
			}
			break;
		case HOMING:
			/* EXECUTE HOMING FUNCTION */
			if (!process->flag.ack_process) {
				send_acknowledge(sendQueueHandle, "HOMING_PROCESS");
				process->flag.ack_process = true;
			}
			process_homing(process);

			break;
		case POURING:
			/* EXECUTE POURING FUNCTION */
			if (!process->flag.ack_process) {
				send_acknowledge(sendQueueHandle, "POURING_PROCESS");
				process->flag.ack_process = true;
			}
			process_pouring(process);

			break;
		case VOLUME_DETECTION:
			/* EXECUTE VOLUME_DETECTION FUNCTION */
			if (!process->flag.ack_process) {
				send_acknowledge(sendQueueHandle, "VOLUME_DETECTION_PROCESS");
				process->flag.ack_process = true;
			}
			process_VOLUME_DETECTION(process);

			break;
		case DEGASSING:
			/* EXECUTE VOLUME_DETECTION FUNCTION */
			if (!process->flag.ack_process) {
				send_acknowledge(sendQueueHandle, "DEGASSING_PROCESS");
				process->flag.ack_process = true;
			}
			process_degassing(process);

			break;
		case ADDING:
			/* EXECUTE ADDING FUNCTION */
			if (!process->flag.ack_process) {
				send_acknowledge(sendQueueHandle, "ADDING_PROCESS");
				process->flag.ack_process = true;
			}
			process_adding(process);

			break;
		case NOZZLE_CHANGE:
			/* EXECUTE NOZZLE FUNCTION */
			if (!process->flag.ack_process) {
				send_acknowledge(sendQueueHandle, "NOZZLE_CHANGE_PROCESS");
				process->flag.ack_process = true;
			}
			process_nozzle_change(process);

			break;
		case EMPTYING:
			/* EXECUTE EMPTY CRUCIBLE FUNCTION */
			if (!process->flag.ack_process) {
				send_acknowledge(sendQueueHandle, "EMPTYING_PROCESS");
				process->flag.ack_process = true;
			}
			process_emptying(process);

			break;
		case CRUCIBLE_CHANGE:
			/* EXECUTE CRUCIBLE CHANGING FUNCTION */
			if (!process->flag.ack_process) {
				send_acknowledge(sendQueueHandle, "CRUCIBLE_CHANGE_PROCESS");
				process->flag.ack_process = true;
			}
			process_crucible_change(process);

			break;
		case COVER_CRUCIBLE:
			/* EXECUTE COVER CRUCIBLE FUNCTION */
			if (!process->flag.ack_process) {
				send_acknowledge(sendQueueHandle, "CRUCIBLE_COVER_PROCESS");
				process->flag.ack_process = true;
			}
			process_cover(process);

			break;
		default:
			/* SHOULD NOT END UP HERE */
			send_error(sendQueueHandle, "UNKNOWN_PROCESS");
			break;
	}
}

void setProcess(_PROCESS newProcess) {
	Process new_process;
	process_init(&new_process);

	new_process.state = BUSY;
	new_process.current = newProcess;

	osStatus_t status;
	status = osMessageQueuePut(processQueueHandle, &new_process, 1, 200);

	osDelay(5); // used to make sure that the process has been properly taken from the queue on the other side before this function exits

//	send_values(sendQueueHandle, status, osMessageQueueGetCount(processQueueHandle));
}

void releaseProcess(Process *process) {
	/* For all processes */
	process->state = AVAILABLE;
	process->current = IDLE;

	/* Process specific */
	process->current_homing_subprocess = HOMING_IDLE;
	process->current_pouring_subprocess = POURING_IDLE;
	process->current_VOLUME_DETECTION_subprocess = VOLUME_DETECTION_IDLE;
	process->current_degassing_subprocess = DEGASSING_IDLE;
	process->current_adding_subprocess = ADDING_IDLE;
	process->current_nozzle_subprocess = NOZZLE_IDLE;
	process->current_emptying_subprocess = EMPTY_IDLE;
	process->current_crucible_change_subprocess = CRUCIBLE_CHANGE_IDLE;
	process->current_cover_crucible_subprocess = COVER_CRUCIBLE_IDLE;

	/* flags for acknowledgment messages */
	process->flag.ack_process = false;
	process->flag.ack_subprocess = false;

	/* Flags */
	process->flag.ack_homing_C = false;
	process->flag.ack_homing_T = false;
	process->flag.ack_homing_Z = false;
	process->flag.motor_start_home = false;

	process->flag.initial_endstop_check = false;
	process->flag.motor_backed_off = false;
	process->flag.motor_initially_not_in_endstop = false;

	process->flag.home_offset_C_start = false;
	process->flag.home_offset_T_start = false;
	process->flag.home_offset_Z_start = false;

	process->flag.degassing_pressure_fault = false;
	process->flag.degassing_reset = false;
	process->flag.degassing_pattern_start = false;
	process->flag.degassing_pattern_done = false;
	process->flag.motor_T_left_pos = false;
	process->flag.motor_T_right_pos = false;
	process->flag.motor_Z_top_pos = false;
	process->flag.motor_Z_down_pos = false;
	process->flag.motor_T_init_pos = false;
	process->flag.motor_T_cover_pos = false;

	process->flag.valve_open = false;
	process->flag.degassing_init_pos_reached = false;

	process->flag.motor_T_overlap = false;
	process->flag.motor_Z_overlap = false;

	/* adding */
	process->flag.latch_open = false;
	process->flag.latch_released = false;
	process->flag.motor_Z_adding_pos=false;

	/* nozzle change */
	process->flag.nozzle_change_done = false;


	/* remaining variables */
	process->t_pouring_generator_start = 0;
	process->pressure_reading = 0;

	process->t_degassing_duration = 0;
	process->t_degassing_start = 0;


	process->flag.motor_T_cover_crucible_pos = false;

	process->flag.emptying_done = false;

	process->flag.motor_C_empty_pos = false;
	process->flag.motor_C_init_pos = false;
	process->flag.motor_C_change_pos = false;

	process->flag.cover_crucible_pos_check = false;

	process->flag.loadcells_tared = false;
}

void process_homing(Process *process) {
	if (process->current_homing_subprocess == HOMING_IDLE) {
		send_acknowledge(sendQueueHandle, "HOMING_STARTED");
		process->current_homing_subprocess = HOME_Z;
	}

	switch (process->current_homing_subprocess)
	{
		case HOME_Z:
			/* HOME Z MOTOR */
			if (!process->flag.ack_homing_Z) {
				send_echo(sendQueueHandle, "HOMING_Z_MOTOR");
				setMaxVelocity(&motor_Z.motion, MOTOR_Z_HOME_MAX_VEL); 	// reduce the velocity
				process->flag.ack_homing_Z = true;
			}
			if (home_Z(process)) {
				setMaxVelocity(&motor_Z.motion, MOTOR_Z_MAX_VEL); 		// revert the velocity to the non-homing value
				send_echo(sendQueueHandle, "Z_MOTOR_HOMED");
				process->current_homing_subprocess = HOME_T;
			}
			break;
		case HOME_T:
			/* HOME T MOTOR */
			if (!process->flag.ack_homing_T) {
				send_echo(sendQueueHandle, "HOMING_T_MOTOR");
				setMaxVelocity(&motor_T.motion, MOTOR_T_HOME_MAX_VEL); 	// reduce the velocity
				process->flag.ack_homing_T = true;
			}
			if (home_T(process)) {
				setMaxVelocity(&motor_T.motion, MOTOR_T_MAX_VEL); 		// revert the velocity to the non-homing value
				send_echo(sendQueueHandle, "T_MOTOR_HOMED");
				process->current_homing_subprocess = HOME_C;
			}
			break;
		case HOME_C:
			/* HOME C MOTOR */
			if (!process->flag.ack_homing_C) {
				send_echo(sendQueueHandle, "HOMING_C_MOTOR");
				setMaxVelocity(&motor_C.motion, MOTOR_C_HOME_MAX_VEL); 	// reduce the velocity
				process->flag.ack_homing_C = true;
			}
			if (home_C(process)) {
				setMaxVelocity(&motor_C.motion, MOTOR_C_MAX_VEL); 		// revert the velocity to the non-homing value
				send_echo(sendQueueHandle, "C_MOTOR_HOMED");
				process->current_homing_subprocess = HOMING_DONE;
			}
			break;
		case HOMING_DONE:
			if (setMotorPosition(&motor_T, MOTOR_T_COVER_CRUCIBLE_POS)){
				process->flag.motor_T_init_pos=true;
				if(setMotorPosition(&motor_Z, MOTOR_Z_DOWN_POS)){
					/* Return that homing process finished */
					process->flag.motor_Z_down_pos = true;
					send_acknowledge(sendQueueHandle, "HOMING_DONE");
					releaseProcess(process);
				}
			}

			break;
		case HOMING_IDLE:
			send_acknowledge(sendQueueHandle, "HOMING_IDLE");
			break;
		default:
			/* SHOULD NOT END UP HERE */
			send_error(sendQueueHandle, "HOMING_NOT_STARTED");
			releaseProcess(process);
			break;
	}
}

bool home_C(Process *process) {
	/*
	 * Returns true only when the motor reaches the endstop.
	 * If the endstop did not trigger, the function loops,
	 * till it do.
	 */

	/* if the endstop is triggered during the call for the homing start, back off the motor a bit, and rehome it */
	if (endStops.e_crucible && !process->flag.initial_endstop_check) {
		process->flag.initial_endstop_check = true; // the initial check has been done
		send_echo(sendQueueHandle,"INITIAL ENDSTOP CHECK C");
		execute_command_from_value(MOTOR, MOTOR_C_FRWD_DEG, MOTOR_C_HOME_BACK_OFF_DISTANCE_DEG); // move motor out of the endstop
	}
	else if (!process->flag.initial_endstop_check) {
		send_echo(sendQueueHandle,"C IS NOT IN ENDSTOP AT START");
		process->flag.initial_endstop_check = true; // initial check done
		process->flag.motor_initially_not_in_endstop = true;
	}

	/* check if motor did not back off yet completely if it started homing in the endstop position */
	if (!process->flag.motor_initially_not_in_endstop && !process->flag.motor_backed_off && isMotorClose(&motor_C, MOTOR_C_HOME_BACK_OFF_DISTANCE_DEG, MOTOR_C_OFFSET_THRESHOLD)) {
		send_acknowledge(sendQueueHandle, "BACKED_OFF DONE");

		process->flag.motor_backed_off = true;
	}

	/* if motor backed off out of the endstop successfully, proceed with the homing procedure */
	if (process->flag.motor_initially_not_in_endstop || process->flag.motor_backed_off) {
		if (endStops.e_crucible && process->current_homing_subprocess == HOME_C) {
			/* begin the return motion to offset the crucibles from the endstop */

			/* ---- crucible has no offset from endstop as of now, code kept the same as for z axis ---- */

			if (!process->flag.home_offset_C_start) {
				process->flag.home_offset_C_start = true;

				execute_command_from_value(MOTOR, MOTOR_C_STOP_HOME,0); // stop motor_Z
				execute_command_from_value(MOTOR, MOTOR_C_FRWD_DEG, MOTOR_C_OFFSET + MOTOR_C_HOME_OFFSET); // offset slightly

				send_acknowledge(sendQueueHandle, "APPLY OFFSET");
			}
		}
		else if (!process->flag.motor_start_home) { 	// if motor has not started to move yet
			process->flag.motor_start_home = true;		// start moving the motor
			send_acknowledge(sendQueueHandle, "START MOVING");
			// reset the motion generator and set the current target to be the current position
			setInitPosition(&motor_C.motion, motor_C.PID.input);
			motion_reset(&motor_C.motion);
			motor_C.motion.target = motor_C.PID.input; 	// stop the motor
			execute_command_from_value(MOTOR, MOTOR_C_FRWD_DEG, MOTOR_C_HOME_MAX_DISTANCE_DEG); // move motor towards the endstop
		}
	}

	/* wait till the position has been reached */
	if (process->flag.home_offset_C_start && isMotorClose(&motor_C, MOTOR_C_OFFSET + MOTOR_C_HOME_OFFSET, MOTOR_C_OFFSET_THRESHOLD)) {
		process->flag.motor_start_home = false; // reset the variables for next homing
		process->flag.initial_endstop_check = false;
		process->flag.motor_backed_off = false;
		process->flag.motor_initially_not_in_endstop = false;
		process->flag.home_offset_C_start = false;
		send_acknowledge(sendQueueHandle, "C HOMED");

		return true;
	}
	return false;
}

bool home_T(Process *process) {
	/*
	 * Returns true only when the motor reaches the endstop.
	 * If the endstop did not trigger, the function loops,
	 * till it do.
	 */

	/* if the endstop is triggered during the call for the homing start, back off the motor a bit, and rehome it */
	if (endStops.e_turret && !process->flag.initial_endstop_check) {
		process->flag.initial_endstop_check = true; // the initial check has been done
		send_echo(sendQueueHandle,"INITIAL ENDSTOP CHECK T");
		execute_command_from_value(MOTOR, MOTOR_T_FRWD_DEG, MOTOR_T_HOME_BACK_OFF_DISTANCE_DEG); // move motor out of the endstop
	}
	else if (!process->flag.initial_endstop_check) {
		send_echo(sendQueueHandle,"T IS NOT IN ENDSTOP AT START");
		process->flag.initial_endstop_check = true; // initial check done
		process->flag.motor_initially_not_in_endstop = true;
	}
	/* check if motor did not back off yet completely if it started homing in the endstop position */
	if (!process->flag.motor_initially_not_in_endstop && !process->flag.motor_backed_off && isMotorClose(&motor_T, MOTOR_T_HOME_BACK_OFF_DISTANCE_DEG, MOTOR_T_OFFSET_THRESHOLD)) {
		send_acknowledge(sendQueueHandle, "BACKED_OFF DONE");

		process->flag.motor_backed_off = true;
	}
	/* if motor backed off out of the endstop successfully, proceed with the homing procedure */
	if (process->flag.motor_initially_not_in_endstop || process->flag.motor_backed_off) {
		if (endStops.e_turret && process->current_homing_subprocess == HOME_T) {
			/* begin the return motion to offset the crucibles from the endstop */
			/* ---- crucible has no offset from endstop as of now, code kept the same as for z axis ---- */
			if (!process->flag.home_offset_T_start) {
				process->flag.home_offset_T_start = true;
				execute_command_from_value(MOTOR, MOTOR_T_STOP_HOME,0); // stop motor_Z
				execute_command_from_value(MOTOR, MOTOR_T_FRWD_DEG, MOTOR_T_OFFSET + MOTOR_T_HOME_OFFSET); // offset slightly
				send_acknowledge(sendQueueHandle, "APPLY OFFSET");
			}
		}
		else if (!process->flag.motor_start_home) { 	// if motor has not started to move yet
			process->flag.motor_start_home = true;		// start moving the motor
			send_acknowledge(sendQueueHandle, "START MOVING");
			// reset the motion generator and set the current target to be the current position
			setInitPosition(&motor_T.motion, motor_T.PID.input);
			motion_reset(&motor_T.motion);
			motor_T.motion.target = motor_T.PID.input; 	// stop the motor
			execute_command_from_value(MOTOR, MOTOR_T_FRWD_DEG, MOTOR_T_HOME_MAX_DISTANCE_DEG); // move motor towards the endstop
		}
	}
	/* wait till the position has been reached */
	if (process->flag.home_offset_T_start && isMotorClose(&motor_T, MOTOR_T_OFFSET + MOTOR_T_HOME_OFFSET, MOTOR_T_OFFSET_THRESHOLD)) {
		process->flag.motor_start_home = false; // reset the variables for next homing
		process->flag.initial_endstop_check = false;
		process->flag.motor_backed_off = false;
		process->flag.motor_initially_not_in_endstop = false;
		process->flag.home_offset_T_start = false;
		send_acknowledge(sendQueueHandle, "T HOMED");
		return true;
	}
	return false;
}

bool home_Z(Process *process) {
	/*
	 * Returns true only when the motor reaches the endstop.
	 * If the endstop did not trigger, the function loops,
	 * till it do.
	 */

	/* if the endstop is triggered during the call for the homing start, back off the motor a bit, and rehome it */
	if (endStops.e_z_axis && !process->flag.initial_endstop_check) {
		process->flag.initial_endstop_check = true; // the initial check has been done
		send_echo(sendQueueHandle,"INITIAL ENDSTOP CHECK Z");
		execute_command_from_value(MOTOR, MOTOR_Z_FRWD_MM, MOTOR_Z_HOME_BACK_OFF_DISTANCE_MM); // move motor out of the endstop
	}
	else if (!process->flag.initial_endstop_check) {
		send_echo(sendQueueHandle,"Z IS NOT IN ENDSTOP AT START");
		process->flag.initial_endstop_check = true; // initial check done
		process->flag.motor_initially_not_in_endstop = true;
	}
	/* check if motor did not back off yet completely if it started homing in the endstop position */
	if (!process->flag.motor_initially_not_in_endstop && !process->flag.motor_backed_off && isMotorClose(&motor_Z, MOTOR_Z_HOME_BACK_OFF_DISTANCE_MM, MOTOR_Z_OFFSET_THRESHOLD)) {
		send_acknowledge(sendQueueHandle, "BACKED_OFF DONE");
		process->flag.motor_backed_off = true;
	}

	/* if motor backed off out of the endstop successfully, proceed with the homing procedure */
	if (process->flag.motor_initially_not_in_endstop || process->flag.motor_backed_off) {
		if (endStops.e_z_axis && process->current_homing_subprocess == HOME_Z) {
			/* begin the return motion to offset the z axis from the endstop */
			if (!process->flag.home_offset_Z_start) {
				process->flag.home_offset_Z_start = true;
				execute_command_from_value(MOTOR, MOTOR_Z_STOP_HOME,0); // stop motor_Z
				execute_command_from_value(MOTOR, MOTOR_Z_FRWD_MM, MOTOR_Z_OFFSET + MOTOR_Z_HOME_OFFSET); // offset slightly
				send_echo(sendQueueHandle, "APPLY OFFSET");
			}
		}
		else if (!process->flag.motor_start_home) { 	// if motor has not started to move yet
			process->flag.motor_start_home = true;		// start moving the motor
			send_echo(sendQueueHandle, "START MOVING");
			// reset the motion generator and set the current target to be the current position
			setInitPosition(&motor_Z.motion, motor_Z.PID.input);
			motion_reset(&motor_Z.motion);
			motor_Z.motion.target = motor_Z.PID.input; 	// stop the motor
			execute_command_from_value(MOTOR, MOTOR_Z_FRWD_MM, MOTOR_Z_HOME_MAX_DISTANCE_MM); // move motor towards the endstop
		}
	}
	/* wait till the position has been reached */
	if (process->flag.home_offset_Z_start && isMotorClose(&motor_Z, MOTOR_Z_OFFSET + MOTOR_Z_HOME_OFFSET, MOTOR_Z_OFFSET_THRESHOLD)) {
		process->flag.motor_start_home = false; // reset the variables for next homing
		process->flag.initial_endstop_check = false;
		process->flag.motor_backed_off = false;
		process->flag.motor_initially_not_in_endstop = false;
		process->flag.home_offset_Z_start = false;

		send_acknowledge(sendQueueHandle, "Z HOMED");

		return true;
	}
	return false;
}

void process_pouring(Process *process) {
	if (process->current_pouring_subprocess == POURING_IDLE) {
		send_acknowledge(sendQueueHandle, "POURING_STARTED");
		process->current_pouring_subprocess = POURING_INIT;
	}

	switch (process->current_pouring_subprocess)
	{
		case POURING_INIT: ;
			if (!process->flag.ack_subprocess) {
				/* Initialize the pouring parameters */
				send_acknowledge(sendQueueHandle, "POURING_INIT");

				/* Angle calculations */
				setInitialAngle(&pouringControl, calculateInitialPouringAngle(Foundry.crucible_volume));

				send_echo(sendQueueHandle, "init_angle here: ");
				send_value(sendQueueHandle, pouringControl.thetaStart);

				/* Reduce velocity to mitigate bouncing of the crucible */
				setMaxVelocity(&motor_C.motion, MOTOR_C_POURING_VEL);

				/* tare the loadcells */
				if (!process->flag.loadcells_tared) {
					hx711_tare(&Hx711_, 10);
					osDelay(2000);

					process->flag.loadcells_tared = true;
				}

				process->flag.ack_subprocess = true;
			}

			/* move Z to the top */
			else if (!process->flag.motor_Z_top_pos && setMotorPosition(&motor_Z, MOTOR_Z_TOP_POS)) {
				process->flag.motor_Z_top_pos = true;
			}

			/* move C to the initial angle */
			else if (process->flag.motor_Z_top_pos && setMotorPosition(&motor_C, pouringControl.thetaStart)) {
				process->t_pouring_generator_start = xTaskGetTickCount();	// initial t for pouring generator

				send_echo(sendQueueHandle, "calc theta_init | real pos");
				send_values(sendQueueHandle, pouringControl.thetaStart, getCurrentMotorPosition(&motor_C));

				setMaxVelocity(&motor_C.motion, MOTOR_C_MAX_VEL);

				process->current_pouring_subprocess = POURING_PATTERN_GENERATOR;
				process->flag.ack_subprocess = false;
			}

			break;
		case POURING_PATTERN_GENERATOR:
			/* Generate the motion pattern for the pouring process */
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "POURING_PATTERN_GENERATOR");
				process->flag.ack_subprocess = true;
			}

			float dt = (float) (xTaskGetTickCount() - process->t_pouring_generator_start) / 1000.0;
			float new_target_pos = updateThetaProfile(&pouringControl, dt);


			if (dt > (2*pouringControl.Tr + pouringControl.Tst)) {
				motor_C.motion.target = motor_C.PID.input; 		// stop the motor
				process->current_pouring_subprocess = POURING_RETURN;
				process->flag.ack_subprocess = false;
			}
			else {
				execute_command_from_value(MOTOR, MOTOR_C_SETPOINT, new_target_pos);
//				setMotorPosition(&motor_C, new_target_pos);
			}
			break;
		case POURING_RETURN:
			/* Return to the initial position */
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "POURING_RETURN");
				process->flag.ack_subprocess = true;
			}

			/* return to initial crucible position */
			if (!process->flag.motor_C_init_pos && setMotorPosition(&motor_C, CRUCIBLE_INIT_POS)) {
				process->flag.motor_C_init_pos = true;

				process->flag.ack_subprocess = false;
			}
			else if (process->flag.motor_C_init_pos && cover_crucible(process)) {
				process->current_pouring_subprocess = POURING_DONE;
				process->flag.ack_subprocess = false;
				process->current_pouring_subprocess = POURING_DONE;
			}

			break;
		case POURING_DONE:
			/* Return that VOLUME_DETECTION process finished */
			send_acknowledge(sendQueueHandle, "POURING_DONE");
			releaseProcess(process);
			break;
		case VOLUME_DETECTION_IDLE:
			send_acknowledge(sendQueueHandle, "POURING_IDLE");
			break;
		default:
			/* SHOULD NOT END UP HERE */
			send_error(sendQueueHandle, "POURING_NOT_STARTED");
			releaseProcess(process);
			break;
	}
}

void process_VOLUME_DETECTION(Process *process) {


	if (process->current_VOLUME_DETECTION_subprocess == VOLUME_DETECTION_IDLE) {
		send_acknowledge(sendQueueHandle, "VOLUME_DETECTION_STARTED");
		process->current_VOLUME_DETECTION_subprocess = VOLUME_DETECTION_INIT;

	}

	switch (process->current_VOLUME_DETECTION_subprocess)
	{

		case VOLUME_DETECTION_INIT: ;
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "VOLUME_DETECTION_INIT");
				process->flag.ack_subprocess = true;
			}


			process->flag.volume_detected=false;
			process->flag.position_laser_ok=false;

			if (!process->flag.motor_T_cover_crucible_pos && setMotorPosition(&motor_T, MOTOR_T_COVER_CRUCIBLE_POS)) {
				process->flag.motor_T_cover_crucible_pos = true;
			}
			/* lower Z to cover crucible position */
			else if (!process->flag.motor_Z_down_pos && process->flag.motor_T_cover_crucible_pos && setMotorPosition(&motor_Z, MOTOR_Z_DOWN_POS)) {
				process->flag.motor_Z_down_pos = true;
			}
			/* if both are reached, do VOLUME_DETECTION_SURFACE_DETECTION */
			else if (process->flag.motor_Z_down_pos && process->flag.motor_T_cover_crucible_pos) {
				process->current_VOLUME_DETECTION_subprocess = VOLUME_DETECTION_SURFACE_DETECTION;
				process->flag.ack_subprocess = false;

			}

			break;

		case VOLUME_DETECTION_SURFACE_DETECTION:

			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "VOLUME_DETECTION_SURFACE_DETECTION");
				setMaxVelocity(&motor_T.motion, LASER_MOTOR_T);
				process->flag.ack_subprocess = true;
			}
			if (process->flag.motor_T_cover_crucible_pos ){
				if ( setMotorPosition(&motor_T,LASER_MOTOR_T_POS ) ){

					process->flag.motor_T_cover_crucible_pos = false;
					process->flag.position_laser_ok=true;
					process->flag.ack_subprocess = false;

				}
			}

			if (process->flag.position_laser_ok) {
				send_acknowledge(sendQueueHandle, "POSITION_LASER_ON");
				osDelay(LASER_TIMEOUT_T);
				if (Foundry.distance_interne>=530){
//					send_echo(sendQueueHandle, "Foundry.distance_interne");
//					send_value(sendQueueHandle,Foundry.distance_interne);
//					send_echo(sendQueueHandle, "Foundry.distance");
//					send_value(sendQueueHandle,Foundry.distance);
					send_acknowledge(sendQueueHandle, "NO_LIQUIDE");
					process->current_VOLUME_DETECTION_subprocess = VOLUME_DETECTION_RETURN;
					process->flag.ack_subprocess = false;
					break;
				}

				if (Foundry.distance_interne<=260){
//					send_echo(sendQueueHandle, "Foundry.distance_interne");
//					send_value(sendQueueHandle,Foundry.distance_interne);
//					send_echo(sendQueueHandle, "Foundry.distance");
//					send_value(sendQueueHandle,Foundry.distance);
					send_acknowledge(sendQueueHandle, "OUT of the crucible");
					process->current_VOLUME_DETECTION_subprocess = VOLUME_DETECTION_RETURN;
					process->flag.ack_subprocess = false;
					break;
				}

				 if (Foundry.distance_interne==0){
					send_acknowledge(sendQueueHandle, "LASER_CANNOT_REACH_THE_LIQUIDE");
					process->current_VOLUME_DETECTION_subprocess = VOLUME_DETECTION_RETURN;
					process->flag.ack_subprocess = false;
					break;
				}

				else {
					laser_Volume(&Foundry);
//					send_echo(sendQueueHandle, "Foundry.distance_interne");
//					send_value(sendQueueHandle,Foundry.distance_interne);
//
//					send_echo(sendQueueHandle, "Foundry.distance");
//					send_value(sendQueueHandle,Foundry.distance);

					send_acknowledge(sendQueueHandle, "CRUCIBLE_VOLUME ");
					send_value(sendQueueHandle,Foundry.crucible_volume/1000000);
					char str_volume[16];
					sprintf(str_volume,"CRUCIBLE_VOLUME:%f",(Foundry.crucible_volume/1000000));
					send_acknowledge(sendQueueHandle, str_volume);
//
					process->current_VOLUME_DETECTION_subprocess = VOLUME_DETECTION_RETURN;
					process->flag.ack_subprocess = false;
					break;
				}

			}

			break;
		case VOLUME_DETECTION_RETURN:
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "VOLUME_DETECTION_RETURN");
				process->flag.ack_subprocess = true;
			}

			/* move turret to cover crucible po a	sition */
			if (!process->flag.motor_T_cover_crucible_pos && setMotorPosition(&motor_T, MOTOR_T_COVER_CRUCIBLE_POS)) {
				process->flag.motor_T_cover_crucible_pos = true;
			}
			/* lower Z to cover crucible position */
			else if (!process->flag.motor_Z_down_pos && process->flag.motor_T_cover_crucible_pos && setMotorPosition(&motor_Z, MOTOR_Z_DOWN_POS)) {
				process->flag.motor_Z_down_pos = true;
			}
			/* if both are reached, finish VOLUME_DETECTION */
			else if (process->flag.motor_Z_down_pos && process->flag.motor_T_cover_crucible_pos) {
				process->current_VOLUME_DETECTION_subprocess = VOLUME_DETECTION_DONE;
				process->flag.ack_subprocess = false;
			}

			break;
		case VOLUME_DETECTION_DONE:
			/* Return that pouring process finished */
			send_acknowledge(sendQueueHandle, "VOLUME_DETECTION_DONE");
			releaseProcess(process);
//
			break;
		case VOLUME_DETECTION_IDLE:
			send_acknowledge(sendQueueHandle, "VOLUME_DETECTION_IDLE");
			break;
		default:
			/* SHOULD NOT END UP HERE */
			send_error(sendQueueHandle, "VOLUME_DETECTION_NOT_STARTED");
			releaseProcess(process);
			break;
	}
}



void smoothPressure(foundry *Foundry) {
  static float window[WINDOW_SIZE]; // Tableau pour stocker les échantillons précédents
  static int currentIndex = 0; // Indice de la prochaine position dans le tableau

  // Ajouter la nouvelle valeur au tableau et mettre à jour l'indice
  window[currentIndex] = Foundry->pressure;
  currentIndex = (currentIndex + 1) % WINDOW_SIZE;

  // Calculer la moyenne des valeurs dans le tableau
  float sum = 0.0;
  for (int i = 0; i < WINDOW_SIZE; i++) {
    sum += window[i];
  }
  float averagePressure = sum / WINDOW_SIZE;
  //send_value(sendQueueHandle,averagePressure);
  Foundry->Smooth_pressure=averagePressure ;

}

void laser_distance_converter(foundry *Foundry){
	float D_interne;

	D_interne = (800-((Foundry->Smooth_pressure*SENSOR_RATIO)+SENSOR_ORIGINE));

	if (D_interne<=200 || D_interne>=600){
		Foundry->distance_interne=0;
	}

	else{
		Foundry->distance_interne = D_interne;
		Foundry->distance = (Foundry->distance_interne * cosf(SENSOR_ANGLE));
	}

}

void laser_Volume(foundry *Foundry){
	float h_volume;
	h_volume=(Foundry->distance-260);
	Foundry->crucible_volume = calculateInitialVolume( h_volume);

}

void process_degassing(Process *process) {
	if (process->current_degassing_subprocess == DEGASSING_IDLE) {
		send_acknowledge(sendQueueHandle, "DEGASSING_STARTED");
		process->current_pouring_subprocess = DEGASSING_INIT;

		/* Check the process time  */
		if (process->t_degassing_duration >= t_MIN_DEGASSING) {
			process->current_degassing_subprocess = DEGASSING_INIT;
		}
		else {
			send_error(sendQueueHandle, "DEGASSING_T_TOO_SHORT");
			releaseProcess(process);
		}
	}

	switch (process->current_degassing_subprocess)
	{
		case DEGASSING_INIT: ;
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "DEGASSING_INIT");
				process->flag.ack_subprocess = true;
			}

			/* open valve */
			if (!process->flag.valve_open) {
				execute_command_from_value(VALVE, VALVE_ON, 0);
				osDelay(VALVE_TIMEOUT_T); // wait for the valve to open fully

				process->flag.valve_open = true;
			}
			/* detect pressure change fault */
			else if (process->flag.valve_open && (abs(TEMP_FOUNDRY_PRESSURE_READING - DEGASSING_PRESSURE) > DEGASSING_PRESSURE_THRESHOLD)) {
				send_error(sendQueueHandle, "DEGASSING_PRESSURE_FAILURE");
				releaseProcess(process);
			}
			/* else move to init position and continue */
			else {
				if (!process->flag.degassing_init_pos_reached) {
					if (!process->flag.degassing_init_pos_requested){
						send_echo(sendQueueHandle, "Move to init_pos");
						process->flag.degassing_init_pos_requested=true;
					}

					/* move to top Z position */
					else if (!process->flag.motor_Z_top_pos && setMotorPosition(&motor_Z, MOTOR_Z_TOP_POS)) {
						process->flag.motor_Z_top_pos = true;
						send_echo(sendQueueHandle, "Z init_pos");
					}
					/* if Z is close to reaching the top position, start moving the turret */
					else if (process->flag.motor_Z_top_pos && !process->flag.motor_T_init_pos && setMotorPosition(&motor_T, DEGASSING_MOTOR_T_INIT_POS)) { //&& process->flag.motor_T_overlap
						process->flag.motor_T_init_pos = true;
						send_echo(sendQueueHandle, "T init_pos");
					}
					/* if both were reached, continue */
					else if (process->flag.motor_Z_top_pos && process->flag.motor_T_init_pos) {
						process->flag.degassing_init_pos_reached = true; // exits the above if statement till the whole process is done

						send_echo(sendQueueHandle, "Init_pos reached");
					}

					/* trigger overlaps */
					else if (!process->flag.motor_T_overlap && isMotorBetween(&motor_Z, DEGASSING_INIT_OVERLAP_MOTOR_T, MOTOR_Z_TOP_POS - 1)) {
						process->flag.motor_T_overlap = true; // motor T can start overlapping the Z movement

						send_echo(sendQueueHandle, "Init_pos - trigger overlap");
					}
				}
				else if (process->flag.motor_T_overlap){
					send_acknowledge(sendQueueHandle, "DEGASSING_INIT_DONE");
					process->current_degassing_subprocess = DEGASSING_PATTERN;
					process->flag.ack_subprocess = false;
					/* reset the overlaps */
					process->flag.motor_T_overlap = false;
				}
			}

			break;
		case DEGASSING_PATTERN:
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "DEGASSING_PATTERN");
				process->flag.ack_subprocess = true;

				/* Set a lower velocity */
				setMaxVelocity(&motor_T.motion, DEGASSING_MOTOR_T_PATTERN_VEL);
				setMaxVelocity(&motor_Z.motion, DEGASSING_MOTOR_Z_INSERT_VEL);

				process->t_degassing_start = xTaskGetTickCount();	// initial t for pouring generator
			}

			/* get current process duration */
			float dt = (float) (xTaskGetTickCount() - process->t_degassing_start) / 1000.0;

			/* perform degassing */
			if (degassing_pattern(process, dt)) {
				/* Revert the velocity to initial value */
				setMaxVelocity(&motor_T.motion, MOTOR_T_MAX_VEL);
				setMaxVelocity(&motor_Z.motion, MOTOR_Z_MAX_VEL);
				send_acknowledge(sendQueueHandle, "DEGASSING_PATTERN_DONE");
				process->current_degassing_subprocess = DEGASSING_COVER_CRUCIBLE;
				process->flag.ack_subprocess = false;
			}

			break;
			/*PREVIOUS*/
		case DEGASSING_COVER_CRUCIBLE:
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "DEGASSING_COVER_CRUCIBLE");
				process->flag.ack_subprocess = true;
			}
			/* trigger to close valve */
			else if(process->flag.valve_open){
				execute_command_from_value(VALVE, VALVE_OFF, 0);
				process->flag.valve_open = false;
				send_acknowledge(sendQueueHandle, "valve done");
				process->flag.cover_crucible_pos_check = true;

			}

			/* move to top Z position */
			else if (process->flag.cover_crucible_pos_check && !process->flag.motor_Z_top_pos && setMotorPosition(&motor_Z, MOTOR_Z_TOP_POS)) {
					process->flag.motor_Z_top_pos = true;
					process->flag.motor_Z_down_pos = false;
					send_echo(sendQueueHandle, "Z top_pos");
			}
			else if (process->flag.motor_Z_top_pos && process->flag.motor_T_init_pos&& setMotorPosition(&motor_T, MOTOR_T_COVER_CRUCIBLE_POS)) {
					process->flag.motor_T_init_pos = false;
					process->flag.motor_T_cover_crucible_pos = true;
					send_echo(sendQueueHandle, "t cover_crucible_pos");
			}
				/* ensure cover crucible pos */
			else if (process->flag.motor_Z_top_pos && process->flag.motor_T_cover_crucible_pos) {
				if (!process->flag.motor_Z_down_pos && setMotorPosition(&motor_Z, MOTOR_Z_DEGASSING_DOWN_POS)) {
					process->flag.motor_Z_top_pos = false;
					process->flag.motor_Z_down_pos = true;

					send_acknowledge(sendQueueHandle, "DEGASSING_COVER_CRUCIBLE_DONE");
					process->flag.cover_crucible_pos_check=false;
				}
			}
			else if (!process->flag.cover_crucible_pos_check){
				process->current_degassing_subprocess = DEGASSING_DONE;
				process->flag.ack_subprocess = false;
			}
			break;

		case DEGASSING_DONE:
			/* Return that degassing process finished */
			send_acknowledge(sendQueueHandle, "DEGASSING_DONE");
			releaseProcess(process);
			break;
		case DEGASSING_IDLE:
			send_acknowledge(sendQueueHandle, "DEGASSING_IDLE");
			break;
		default:
			/* SHOULD NOT END UP HERE */
			send_error(sendQueueHandle, "DEGASSING_NOT_STARTED");
			releaseProcess(process);
			break;
	}
}

bool degassing_pattern(Process *process, float elapsed_time) {
	if (elapsed_time <= process->t_degassing_duration || motor_T.state == MOTOR_IS_OFF) {
		/* bring turret down if its in the proper position */
		if (!process->flag.degassing_pattern_start) {
			if (!process->flag.motor_Z_down_pos && setMotorPosition(&motor_Z, MOTOR_Z_DEGASSING_DOWN_POS)) {
					process->flag.motor_Z_top_pos = false;
					process->flag.motor_Z_down_pos = true;
			}
			else if (process->flag.motor_Z_down_pos && setMotorPosition(&motor_T, DEGASSING_MOTOR_T_RIGHT_POS)) {
				process->flag.motor_T_right_pos = true;
				process->flag.motor_T_left_pos = false;

				// start the pattern
				process->flag.degassing_pattern_start = true;
			}
		}
		/* if motor is on the right, move to the left */
		if (process->flag.degassing_pattern_start && process->flag.motor_T_right_pos && setMotorPosition(&motor_T, DEGASSING_MOTOR_T_LEFT_POS)) {
			process->flag.motor_T_right_pos = false;
			process->flag.motor_T_left_pos = true;
		}
		/* if motor is on the left, move to the right */
		else if (process->flag.degassing_pattern_start && process->flag.motor_T_left_pos && setMotorPosition(&motor_T, DEGASSING_MOTOR_T_RIGHT_POS)) {
			process->flag.motor_T_left_pos = false;
			process->flag.motor_T_right_pos = true;
		}
	}
	else {
		if (!process->flag.degassing_pattern_done) {
			/* --- as the elapsed_time if exits even if one of the setMotorPosition functions did not finish the movement,
			 * the following are not executed and have to be typed separately --- */
			motor_T.motion.target = motor_T.PID.input; // stop the motor
			motor_T.state = MOTOR_IS_OFF;
			/* ------ */

			process->flag.degassing_pattern_done = true; // trigger once, when pattern finished, so that the motor flag is reset
		}
		/* return to the init position */
		if (process->flag.degassing_pattern_done && setMotorPosition(&motor_T, DEGASSING_MOTOR_T_INIT_POS))
		if (setMotorPosition(&motor_T, DEGASSING_MOTOR_T_INIT_POS)) {
			process->flag.motor_T_right_pos = false;
			process->flag.motor_T_left_pos = false;
			process->flag.motor_T_cover_pos=false;
			process->flag.motor_T_init_pos = true;

			return true;
		}
	}
	return false;
}

void process_adding(Process *process) {
	if (process->current_adding_subprocess == ADDING_IDLE) {
		send_acknowledge(sendQueueHandle, "ADDING_STARTED");
		process->current_adding_subprocess = ADDING_INIT;
	}

	switch (process->current_adding_subprocess)
	{
		case ADDING_INIT: ;

			if (!process->flag.ack_subprocess && process->flag.motor_Z_adding_pos && process->flag.motor_T_init_pos && process->flag.latch_released ) {
				process->flag.ack_subprocess = true;
				osDelay(10);
				send_acknowledge(sendQueueHandle, "ADDING_INIT_DONE");
				material_count=0;
			}
			/* move to adding Z position */
			else if (!process->flag.motor_Z_adding_pos && setMotorPosition(&motor_Z, MOTOR_Z_ADDING_POS)) {
//				send_echo(sendQueueHandle, "moving motor Z to adding pos");
				process->flag.motor_Z_adding_pos = true;
				send_echo(sendQueueHandle, "Init_pos - Z_adding reached");
				send_acknowledge(sendQueueHandle, "Z_DONE");

			}
			/* move to adding T position */
			else if (!process->flag.motor_T_init_pos && process->flag.motor_Z_adding_pos){
				send_echo(sendQueueHandle, "moving motor T to adding pos");
				if( setMotorPosition(&motor_T, ADDING_MOTOR_T_POS)) {
					process->flag.motor_T_init_pos = true;
					send_echo(sendQueueHandle, "Init_pos - T_adding reached");
					send_acknowledge(sendQueueHandle, "T_DONE");
				}
			}
			/* if both are reached, open latch */
			else if (!process->flag.latch_released && process->flag.motor_Z_adding_pos ) { //&& process->flag.motor_T_init_pos
				execute_command_from_value(LATCH, LATCH_ON, 0);
				process->flag.latch_released = true;
				send_echo(sendQueueHandle, "latch released");
			}
			else if (process->flag.latch_released) {
				process->current_adding_subprocess = ADDING_WAIT;
				process->flag.ack_subprocess = false;
			}
			break;
		case ADDING_WAIT:
			if (!process->flag.ack_subprocess && !process->flag.latch_open) {
				process->flag.ack_subprocess = true;
				send_acknowledge(sendQueueHandle, "ADDING_MATERIAL");
				process->flag.latch_open=false;
			}
			/* wait for the user to open the latch */
			else if (!process->flag.latch_open && process->flag.latch_released ) {
				if (endStops.e_door_h && !process->flag.latch_open_requested){
					send_acknowledge(sendQueueHandle, "START_ADDING");
					process->flag.latch_open_requested = true; // variable to Mark the message as sent (to send one single message)
					process->flag.latch_open = false;
				}
				else if (!endStops.e_door_h ) {
					process->flag.latch_open = true;
					process->flag.latch_open_requested = false; // Reset the static variable
					send_acknowledge(sendQueueHandle, "LATCH_ON");


				}
				else if (process->flag.latch_open && endStops.e_door_h ) {
						execute_command_from_value(LATCH, LATCH_OFF, 0);
						process->flag.latch_open = false;
						send_acknowledge(sendQueueHandle, "LATCH_CLOSED");
						process->current_adding_subprocess = ADDING_COVER_CRUCIBLE;
						process->flag.ack_subprocess = false;
								}
			}
			else if (process->flag.latch_open && endStops.e_door_h ) {
				execute_command_from_value(LATCH, LATCH_OFF, 0);
				process->flag.latch_open = false;
				send_acknowledge(sendQueueHandle, "LATCH_CLOSED");
				process->current_adding_subprocess = ADDING_COVER_CRUCIBLE;
				process->flag.ack_subprocess = false;
											}

			break;
		case ADDING_COVER_CRUCIBLE:
			/* Return to the cover crucible position */
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "ADDING_COVER_CRUCIBLE");
				process->flag.ack_subprocess = true;
			}
			/* move turret to cover crucible position */
			if (!process->flag.motor_T_cover_crucible_pos && setMotorPosition(&motor_T, MOTOR_T_COVER_CRUCIBLE_POS)) {
				process->flag.motor_T_cover_crucible_pos = true;
			}
			/* lower Z to cover crucible position */
			else if (!process->flag.motor_Z_down_pos && process->flag.motor_T_cover_crucible_pos && setMotorPosition(&motor_Z, MOTOR_Z_DOWN_POS)) {
				process->flag.motor_Z_down_pos = true;
			}
			/* if both are reached, finish adding */
			else if (process->flag.motor_Z_down_pos && process->flag.motor_T_cover_crucible_pos) {
				process->current_adding_subprocess = ADDING_DONE;
				process->flag.ack_subprocess = false;
			}
			break;
		case ADDING_DONE:
			/* Return that adding process finished */
			send_acknowledge(sendQueueHandle, "ADDING_PROCESS_DONE");
			releaseProcess(process);
			break;
		case ADDING_IDLE:
			send_acknowledge(sendQueueHandle, "ADDING_IDLE");
			process->current_adding_subprocess = ADDING_INIT;
			break;
		default:
			/* SHOULD NOT END UP HERE */
			send_error(sendQueueHandle, "ADDING_NOT_STARTED");
			releaseProcess(process);
			break;
	}
}

void process_nozzle_change(Process *process) {
	if (process->current_nozzle_subprocess == ADDING_IDLE) {
				send_acknowledge(sendQueueHandle, "NOZZLE_CHANGE_STARTED");
				process->current_nozzle_subprocess = NOZZLE_INIT;
	}

	switch (process->current_nozzle_subprocess)
	{
		case NOZZLE_INIT: ;
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "NOZZLE_CHANGE_INIT");
				process->flag.ack_subprocess = true;
			}

			/* move to the Z top position */
			if (!process->flag.motor_Z_top_pos && setMotorPosition(&motor_Z, MOTOR_Z_TOP_POS)) {
				process->flag.motor_Z_top_pos = true;
			}
			/* rotate turret to T nozzle change position */
			else if (!process->flag.motor_T_init_pos && process->flag.motor_Z_top_pos && setMotorPosition(&motor_T, MOTOR_T_COVER_CRUCIBLE_POS)) {
				process->flag.motor_T_init_pos = true;
			}
			/* if both are reached, change to waiting */
			else if (process->flag.motor_T_init_pos && process->flag.motor_Z_top_pos) {
				process->current_nozzle_subprocess = NOZZLE_WAIT;
				process->flag.ack_subprocess = false;
			}

			break;
		case NOZZLE_WAIT:
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "NOZZLE_WAIT");
				process->flag.ack_subprocess = true;
			}

			if (process->flag.nozzle_change_done) {
				process->current_nozzle_subprocess = NOZZLE_COVER_CRUCIBLE;
				process->flag.ack_subprocess = false;
			}

			break;
		case NOZZLE_COVER_CRUCIBLE:
			/* Return to the cover crucible position */
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "NOZZLE_COVER_CRUCIBLE");
				process->flag.ack_subprocess = true;
			}

			/* move turret to cover crucible position */
			if (!process->flag.motor_T_cover_crucible_pos && setMotorPosition(&motor_T, MOTOR_T_COVER_CRUCIBLE_POS)) {
				process->flag.motor_T_cover_crucible_pos = true;
			}
			/* lower Z to cover crucible position */
			else if (!process->flag.motor_Z_down_pos && process->flag.motor_T_cover_crucible_pos && setMotorPosition(&motor_Z, MOTOR_Z_DOWN_POS)) {
				process->flag.motor_Z_down_pos = true;
			}
			/* if both are reached, change to waiting */
			else if (process->flag.motor_Z_down_pos && process->flag.motor_T_cover_crucible_pos) {
				process->current_nozzle_subprocess = NOZZLE_DONE;
				process->flag.ack_subprocess = false;
			}

			break;
		case NOZZLE_DONE:
			/* Return that nozzle change process finished */
			send_acknowledge(sendQueueHandle, "NOZZLE_DONE");
			releaseProcess(process);
			break;
		case NOZZLE_IDLE:
			send_acknowledge(sendQueueHandle, "NOZZLE_IDLE");
			break;
		default:
			/* SHOULD NOT END UP HERE */
			send_error(sendQueueHandle, "NOZZLE_CHANGE_NOT_STARTED");
			releaseProcess(process);
			break;
	}
}

void process_emptying(Process *process) {
	if (process->current_emptying_subprocess == EMPTY_IDLE) {
		send_acknowledge(sendQueueHandle, "EMPTYING_STARTED");
		process->current_emptying_subprocess = EMPTY_INIT;
	}

	switch (process->current_emptying_subprocess)
	{
		case EMPTY_INIT: ;
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "EMPTY_INIT");
				process->flag.ack_subprocess = true;
			}

			// move to cover crucible position
			if (!process->flag.motor_T_cover_crucible_pos && setMotorPosition(&motor_T, MOTOR_T_COVER_CRUCIBLE_POS)) {
				process->flag.motor_T_cover_crucible_pos = true;
			}
			else if (!process->flag.motor_Z_down_pos && process->flag.motor_T_cover_crucible_pos && setMotorPosition(&motor_Z, MOTOR_Z_DOWN_POS)) {
				process->flag.motor_Z_down_pos = true;
			}
			else if (process->flag.motor_T_cover_crucible_pos && process->flag.motor_Z_down_pos) {
				/* start to heat up the crucible */
//				execute_command_from_value(HEATERS, HEATERS_SET, 800);

				process->current_emptying_subprocess = EMPTY_WAIT;
				process->flag.ack_subprocess = false;

				// reset position flag
				process->flag.motor_Z_down_pos = false;
			}

			break;
		case EMPTY_WAIT: ;
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "EMPTY_WAIT");
				process->flag.ack_subprocess = true;
			}

//			// wait for the temperature to be reached
//			if (abs(temp_target - temp_crucible) <= TEMP_THRESHOLD) {
//				process->current_emptying_subprocess = EMPTY_START;
//				process->flag.ack_subprocess = false;
//			}

			process->current_emptying_subprocess = EMPTY_START;
			process->flag.ack_subprocess = false;
			osDelay(10000);

			break;
		case EMPTY_START: ;
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "EMPTY_START");
				process->flag.ack_subprocess = true;

				setMaxVelocity(&motor_C.motion, MOTOR_C_MAX_VEL * 0.5); 	// reduce the velocity
			}

			// move Z to top position
			if (!process->flag.motor_Z_top_pos && setMotorPosition(&motor_Z, MOTOR_Z_TOP_POS)) {
				process->flag.motor_Z_top_pos = true;
			}
			// move crucible
			else if (!process->flag.motor_C_empty_pos && process->flag.motor_Z_top_pos && setMotorPosition(&motor_C, EMPTY_CRUCIBLE_POS)) {
				process->flag.motor_C_empty_pos = true;
				send_acknowledge(sendQueueHandle, "IS_FINISHED?");
			}
			else if (process->flag.emptying_done) {
				process->current_emptying_subprocess = EMPTY_RETURN;
				process->flag.ack_subprocess = false;

				setMaxVelocity(&motor_C.motion, MOTOR_C_MAX_VEL); 	// set the velocity back to the normal value
			}

			break;
		case EMPTY_RETURN: ;
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "EMPTY_RETURN");
				process->flag.ack_subprocess = true;
			}

			// move crucible to initial position
			if (!process->flag.motor_C_init_pos && setMotorPosition(&motor_C, CRUCIBLE_INIT_POS)) {
				process->flag.motor_C_init_pos = true;

				// wait for the crucible to settle
				osDelay(2000);
			}
			// cover crucible
			else if (!process->flag.motor_Z_down_pos && process->flag.motor_C_init_pos && setMotorPosition(&motor_Z, MOTOR_Z_DOWN_POS)) {
				process->flag.motor_Z_down_pos = true;
			}
			else if (process->flag.motor_Z_down_pos && process->flag.emptying_done) {
				process->current_emptying_subprocess = EMPTY_DONE;
				process->flag.ack_subprocess = false;
			}

			break;
		case EMPTY_DONE:
			/* Return that nozzle change process finished */
			send_acknowledge(sendQueueHandle, "EMPTY_DONE");
			releaseProcess(process);
			break;
		case EMPTY_IDLE:
			send_acknowledge(sendQueueHandle, "EMPTY_IDLE");
			break;
		default:
			/* SHOULD NOT END UP HERE */
			send_error(sendQueueHandle, "EMPTYING_NOT_STARTED");
			releaseProcess(process);
			break;
	}
}

void process_crucible_change(Process *process) {
	if (process->current_crucible_change_subprocess == CRUCIBLE_CHANGE_IDLE) {
		send_acknowledge(sendQueueHandle, "CRUCIBLE_CHANGE_STARTED");
		process->current_crucible_change_subprocess = CRUCIBLE_CHANGE_INIT;
	}

	switch (process->current_crucible_change_subprocess)
	{
		case CRUCIBLE_CHANGE_INIT: ;
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "CRUCIBLE_CHANGE_INIT");
				process->flag.ack_subprocess = true;
			}

			if (cover_crucible(process)) {
				process->current_crucible_change_subprocess = CRUCIBLE_CHANGE_START;
				process->flag.ack_subprocess = false;
			}

			break;
		case CRUCIBLE_CHANGE_START: ;
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "CRUCIBLE_CHANGE_START");
				process->flag.ack_subprocess = true;
			}

			send_values(sendQueueHandle, process->flag.motor_Z_down_pos, process->flag.motor_Z_top_pos);

			if (process->flag.motor_Z_down_pos && !process->flag.motor_Z_top_pos && setMotorPosition(&motor_Z, MOTOR_Z_TOP_POS)) {
				process->flag.motor_Z_down_pos = false;
				process->flag.motor_Z_top_pos = true;

				send_echo(sendQueueHandle, "moving Z");
			}
			// move crucible to 180deg position
			else if (!process->flag.motor_C_change_pos && process->flag.motor_Z_top_pos && setMotorPosition(&motor_C, CRUCIBLE_CHANGE_POS)) {
				process->flag.motor_C_change_pos = true;

				process->current_crucible_change_subprocess = CRUCIBLE_CHANGE_WAIT;
				process->flag.ack_subprocess = false;
			}

			break;
		case CRUCIBLE_CHANGE_WAIT: ;
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "CRUCIBLE_CHANGE_WAIT");
				process->flag.ack_subprocess = true;
			}

			if (1 == 0) {
				process->current_crucible_change_subprocess = CRUCIBLE_CHANGE_DONE;
				process->flag.ack_subprocess = false;
			}

			break;
		case CRUCIBLE_CHANGE_DONE:
			send_acknowledge(sendQueueHandle, "CRUCIBLE_CHANGE_DONE");
			releaseProcess(process);
			break;
		case CRUCIBLE_CHANGE_IDLE:
			send_acknowledge(sendQueueHandle, "CRUCIBLE_CHANGE_IDLE");
			break;
		default:
			/* SHOULD NOT END UP HERE */
			send_error(sendQueueHandle, "CRUCIBLE_CHANGE_NOT_STARTED");
			releaseProcess(process);
			break;
	}
}

void process_cover(Process *process) {
	if (process->current_cover_crucible_subprocess == COVER_CRUCIBLE_IDLE) {
		send_acknowledge(sendQueueHandle, "COVER_CRUCIBLE_STARTED");
		process->current_cover_crucible_subprocess = COVER_CRUCIBLE_START;
	}

	switch (process->current_cover_crucible_subprocess)
	{
		case COVER_CRUCIBLE_START: ;
			if (!process->flag.ack_subprocess) {
				send_acknowledge(sendQueueHandle, "COVER_CRUCIBLE_START");
				process->flag.ack_subprocess = true;
			}
			if (cover_crucible(process)) {
				process->current_cover_crucible_subprocess = COVER_CRUCIBLE_DONE;
				process->flag.ack_subprocess = false;
			}
			break;
		case COVER_CRUCIBLE_DONE:
			send_acknowledge(sendQueueHandle, "COVER_CRUCIBLE_DONE");
			releaseProcess(process);
			break;
		case COVER_CRUCIBLE_IDLE:
			send_acknowledge(sendQueueHandle, "COVER_CRUCIBLE_IDLE");
			break;
		default:
			/* SHOULD NOT END UP HERE */
			send_error(sendQueueHandle, "COVER_CRUCIBLE_NOT_STARTED");
			releaseProcess(process);
			break;
	}
}

bool cover_crucible(Process *process) {
	// check if the position is cover crucible position
	if (!process->flag.cover_crucible_pos_check) {
		osDelay(1000); // mitigate crucible bouncing

		process->flag.cover_crucible_pos_check = true;
		if (isMotorClose(&motor_C, CRUCIBLE_INIT_POS, motor_C.config.OFFSET_THRESHOLD)) {
			process->flag.motor_C_init_pos = true;

			send_echo(sendQueueHandle, "C ok");
		}
		if (isMotorClose(&motor_T, MOTOR_T_COVER_CRUCIBLE_POS, motor_T.config.OFFSET_THRESHOLD)) {
			process->flag.motor_T_cover_crucible_pos = true;

			send_echo(sendQueueHandle, "T ok");
		}
		if (isMotorClose(&motor_Z, MOTOR_Z_DOWN_POS, motor_Z.config.OFFSET_THRESHOLD)) {
			process->flag.motor_Z_down_pos = true;

			send_echo(sendQueueHandle, "Z ok");
		}
	}
	else if (process->flag.motor_C_init_pos && process->flag.motor_T_cover_crucible_pos && process->flag.motor_Z_down_pos) {
		send_echo(sendQueueHandle, "all ok");
		process->flag.cover_crucible_pos_check = false;

		return true;
	}
	else {
		if (!process->flag.motor_Z_top_pos && setMotorPosition(&motor_Z, MOTOR_Z_TOP_POS)) {
			process->flag.motor_Z_top_pos = true;
			process->flag.motor_Z_down_pos = false;
			send_echo(sendQueueHandle, "Z -> top");
		}
		else if (!process->flag.motor_T_cover_crucible_pos && process->flag.motor_Z_top_pos && setMotorPosition(&motor_T, MOTOR_T_COVER_CRUCIBLE_POS)) {
			process->flag.motor_T_cover_crucible_pos = true;
			send_echo(sendQueueHandle, "T -> cover");
		}
		else if (process->flag.motor_T_cover_crucible_pos && setMotorPosition(&motor_Z, MOTOR_Z_DOWN_POS)) {
			process->flag.motor_Z_top_pos = false;
			process->flag.motor_Z_down_pos = true;

			send_echo(sendQueueHandle, "Z -> down");
		}
	}
	return false;
}

