/*
 * encoders.h
 *
 *  Created on: Aug 23, 2022
 *      Author: MM3D
 */

#ifndef INC_ENCODERS_H_
#define INC_ENCODERS_H_

#include "SPI_Handler.h"
#include "cmsis_os2.h"
#include "stdbool.h"

extern bool correct_setup_C;
extern bool correct_setup_T;
extern bool correct_setup_Z;

typedef enum {
	ENCODER_T = 0,
	ENCODER_Z,
	ENCODER_C
} ENCODER_;

typedef enum {
	ENCODER_OK = 0,
	ENCODER_FAULT
} Encoder_statusHandle;

typedef enum {
	MM = 0,
	DEG
} UNIT_;

typedef enum {
	MOTOR_IS_RUNNING = 0,
	MOTOR_IS_OFF
} Motor_statusHandle;

void encoders_init(); 						/*initializes and resets the encoders */
long encoder_read(uint8_t slave_ID);
void encoder_reset(uint8_t slave_ID);
bool check_MDR0_was_set(uint8_t slave_ID);
bool isThereCommunication(uint8_t slave_ID);

float encoder_convert_to_unit(float value, UNIT_ unit, int gear_ratio);
float encoder_convert_from_unit(float value, UNIT_ unit, int gear_ratio);

Encoder_statusHandle encoder_check(long prev_reading, long curr_reading, Motor_statusHandle state); /* checks for faults */

#endif /* INC_ENCODERS_H_ */
