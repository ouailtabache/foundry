/*
 * foundryStatus.h
 *
 *  Created on: Aug 18, 2022
 *      Author: MM3D | awitc
 */

#ifndef INC_FOUNDRYDATA_H_
#define INC_FOUNDRYDATA_H_

#include "stdio.h"

typedef struct {
	uint8_t state;
	uint16_t pressure;
	float loadcell_reading;
	double crucible_volume; // [mm^3]
	float humidity;
	float ambient_temp;
	long c_pos, z_pos, t_pos;
	float thermocouplesArray[2];
	uint8_t faultThermocouplesArray[2];
	uint16_t Smooth_pressure;
	uint16_t distance;
	uint16_t distance_interne;
} foundry;

extern foundry Foundry;

void foundry_init(foundry *Foundry);
void setCrucibleVolume(foundry *Foundry, double volume);
double getCrucibleVolume(foundry *Foundry);

#endif /* INC_FOUNDRYDATA_H_ */
