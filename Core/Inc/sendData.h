/*
 * sendData.h
 *
 *  Created on: Aug 17, 2022
 *  updated on: Mai 29, 2023 | Mohamed
 *      Author: MM3D | awitc
 */

#ifndef INC_SENDDATA_H_
#define INC_SENDDATA_H_

#include "foundryData.h"
#include "stdio.h"
#include "string.h"
#include "stm32h7xx_hal.h"
#include "cmsis_os2.h"
#include "define.h"
#include "encoders.h"

extern UART_HandleTypeDef huart3;
extern osThreadId_t sendDataHandle;

extern bool enable_temp_pid_C;
extern bool enable_temp_pid_T;
extern bool enable_temp_pid_Z;

extern bool enable_abs_pid_C;
extern bool enable_abs_pid_T;
extern bool enable_abs_pid_Z;

extern bool enable_motion_C;
extern bool enable_motion_T;
extern bool enable_motion_Z;

extern bool is_com_enc_C;
extern bool is_com_enc_T;
extern bool is_com_enc_Z;

extern bool correct_setup_C;
extern bool correct_setup_T;
extern bool correct_setup_Z;

extern float temp_crucible;
extern float enableSimulation;

void send_acknowledge(osMessageQueueId_t sendQueueHandle, const char cmd[]);
void send_error(osMessageQueueId_t sendQueueHandle, const char error[]);
void send_echo(osMessageQueueId_t sendQueueHandle, const char user_cmd[]);
void send_value(osMessageQueueId_t sendQueueHandle, double value);
void send_values(osMessageQueueId_t sendQueueHandle, double value, double value2);
void send_values_csv_format(osMessageQueueId_t sendQueueHandle, double value, double value2);
void send_data(osMessageQueueId_t sendQueueHandle, foundry *Foundry);
void send_response(osMessageQueueId_t sendQueueHandle, const char response[]);
void errMessage();

#endif /* INC_SENDDATA_H_ */
