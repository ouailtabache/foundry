/*
 * VL53L0x.h
 *
 *  Created on: Jul 24, 2023
 *      Author: damie
 */
#ifndef INC_VL53L0X_H_
#define INC_VL53L0X_H_

#include "main.h"
#include "vl53l0x_api.h"
#include "commandHandler.h"


uint8_t Message[64];
uint8_t MessageLen;
uint8_t InitStatuts;
uint16_t flaskTestDistance;
uint16_t flaskInitDistance;
extern VL53L0X_RangingMeasurementData_t RangingData;
extern VL53L0X_Dev_t  vl53l0x_c;
extern VL53L0X_DEV    Dev;


void InitFlask(void);
FLASK_DETECTION_STATUS CheckFlask(void);
uint16_t SingleMeasurement (void);
uint16_t Measurement (void);
uint16_t CheckMeasurement (void);


#endif /* INC_VL53L0X_H_ */
