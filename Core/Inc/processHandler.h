/*
 * Processes_2.h
 *
 *  Created on: 21 gru 2022
 *      Author: MM3D
 */

#ifndef INC_PROCESSHANDLER_H_
#define INC_PROCESSHANDLER_H_

#include "cmsis_os.h"
#include "commandHandler.h"
#include "sendData.h"
#include "endStops.h"
#include "pouringControl.h"
#include "foundryData.h"
#include "motorHandler.h"

typedef enum {
	BUSY = 0,
	AVAILABLE
} _STATE;

typedef enum {
	IDLE = 0,
	HOMING,
	POURING,
	VOLUME_DETECTION,
	DEGASSING,
	ADDING,
	NOZZLE_CHANGE,
	EMPTYING,
	CRUCIBLE_CHANGE,
	COVER_CRUCIBLE
} _PROCESS;

typedef enum {
	HOMING_IDLE = 0,
	HOME_Z,
	HOME_T,
	HOME_C,
	HOMING_DONE
} _SUBPROCESS_HOMING;

typedef enum {
	POURING_IDLE = 0,
	POURING_INIT,
	POURING_PATTERN_GENERATOR,
	POURING_RETURN,
	POURING_DONE
} _SUBPROCESS_POURING;

typedef enum {
	VOLUME_DETECTION_IDLE = 0,
	VOLUME_DETECTION_INIT,
	VOLUME_DETECTION_SURFACE_DETECTION,
	VOLUME_DETECTION_RETURN,
	VOLUME_DETECTION_DONE
} _SUBPROCESS_VOLUME_DETECTION;

typedef enum {
	DEGASSING_IDLE = 0,
	DEGASSING_INIT,
	DEGASSING_PATTERN,
	DEGASSING_COVER_CRUCIBLE,
	DEGASSING_DONE
} _SUBPROCESS_DEGASSING;

typedef enum {
	ADDING_IDLE = 0,
	ADDING_INIT,
	ADDING_WAIT,
	ADDING_COVER_CRUCIBLE,
	ADDING_DONE
} _SUBPROCESS_ADDING;

typedef enum {
	NOZZLE_IDLE = 0,
	NOZZLE_INIT,
	NOZZLE_WAIT,
	NOZZLE_COVER_CRUCIBLE,
	NOZZLE_DONE
} _SUBPROCESS_NOZZLE;

typedef enum {
	EMPTY_IDLE = 0,
	EMPTY_INIT,
	EMPTY_WAIT,
	EMPTY_START,
	EMPTY_RETURN,
	EMPTY_DONE
} _SUBPROCESS_EMPTYING;

typedef enum {
	CRUCIBLE_CHANGE_IDLE = 0,
	CRUCIBLE_CHANGE_INIT,
	CRUCIBLE_CHANGE_START,
	CRUCIBLE_CHANGE_WAIT,
	CRUCIBLE_CHANGE_DONE
} _SUBPROCESS_CRUCIBLE_CHANGE;

typedef enum {
	COVER_CRUCIBLE_IDLE = 0,
	COVER_CRUCIBLE_START,
	COVER_CRUCIBLE_DONE
} _SUBPROCESS_COVER_CRUCIBLE;

typedef enum {
  FLASK_NOT_DETECTED,
  FLASK_DETECTED,
  SENSOR_ERROR
} FLASK_DETECTION_STATUS;


typedef struct {
	/* flags for acknowledgment messages */
	bool ack_process;
	bool ack_subprocess;

	/* homing flags */
	bool ack_homing_C, ack_homing_T, ack_homing_Z; // triggers for one-time acknowledgment message
	bool motor_start_home; 		// indication for the motor to start movement

	bool initial_endstop_check; // if the endstop is triggered at the moment the homing has been started, used to back off the motor a bit
	bool motor_backed_off; 		// if the motor reached the back-off position
	bool motor_initially_not_in_endstop; // if the homing started with the motor not triggering the endstop

	bool home_offset_C_start, home_offset_T_start, home_offset_Z_start; // used to trigger the motor movement for offset after homing

	/* degassing flags */
	bool degassing_pressure_fault; 	// when the pressure reading is faulty
	bool degassing_reset; 			// provides a trigger for the commands to reset the valve and motors in case of a faulty pressure reading
	bool degassing_pattern_done; 	// triggers when pattern is finished
	bool degassing_cover_pos_reached; // when both T and Z motors are in the correct positions to open the valve
	bool degassing_init_pos_requested;	// variable to Mark the moving message as sent (to send one single message)
	bool degassing_init_pos_reached;
	bool valve_open;
	bool degassing_pattern_start;


	/* degassing flags for motor movement triggers */
	bool motor_Z_top_pos, motor_Z_down_pos;
	bool motor_T_init_pos, motor_T_cover_pos;
	bool motor_T_left_pos, motor_T_right_pos;

	/* adding flags */
	bool latch_open;
	bool latch_released;
	bool latch_open_requested ; // Static variable to track if message has been sent
	bool motor_Z_adding_pos;


	/* nozzle change flags */
	bool nozzle_change_done;

	/* overlap flags */
	bool motor_T_overlap, motor_Z_overlap;


	bool motor_T_cover_crucible_pos;

	bool emptying_done;

	bool motor_C_empty_pos, motor_C_init_pos, motor_C_change_pos;

	bool cover_crucible_pos_check;

	bool loadcells_tared;


	/* VOLUME_DETECTION */

	  bool volume_detected;

	  bool position_laser_ok;


} _FLAGS;

typedef struct {
	_STATE state;
	_PROCESS current;
	_SUBPROCESS_HOMING current_homing_subprocess;
	_SUBPROCESS_POURING current_pouring_subprocess;
	_SUBPROCESS_VOLUME_DETECTION current_VOLUME_DETECTION_subprocess;
	_SUBPROCESS_DEGASSING current_degassing_subprocess;
	_SUBPROCESS_ADDING current_adding_subprocess;
	_SUBPROCESS_NOZZLE current_nozzle_subprocess;
	_SUBPROCESS_EMPTYING current_emptying_subprocess;
	_SUBPROCESS_CRUCIBLE_CHANGE current_crucible_change_subprocess;
	_SUBPROCESS_COVER_CRUCIBLE current_cover_crucible_subprocess;
	_FLAGS flag;

	unsigned long t_pouring_generator_start;
	unsigned long t_degassing_duration, t_degassing_start;
	float pressure_reading;

} Process;

extern osMessageQueueId_t processQueueHandle;
extern Process current_process;

void process_init(Process *process);
void processHandler(Process *process); 		// main function for handling processes
void setProcess(_PROCESS newProcess);		// set a new process to the foundry
void releaseProcess(Process *process);

/* Homming functions */
void process_homing(Process *process);
bool home_C(Process *process);
bool home_T(Process *process);
bool home_Z(Process *process);

/* Pouring functions */
void process_pouring(Process *process);

/* Degassing functions */
void process_degassing(Process *process);
bool degassing_pattern(Process *process, float elapsed_time);

/* Adding functions */
void process_adding(Process *process);

/* VOLUME_DETECTION functions */
void process_VOLUME_DETECTION(Process *process);
//bool VOLUME_DETECTION_volume_detection(Process *process);


/* VOLUME_DETECTION functions */
void process_VOLUME_DETECTION(Process *process);
void smoothPressure(foundry *Foundry);
void laser_distance_converter(foundry *Foundry);
void laser_Volume(foundry *Foundry);

/* Nozzle change functions */
void process_nozzle_change(Process *process);

/* Crucible change functions */
void process_crucible_change(Process *process);

/* Empty crucible functions */
void process_emptying(Process *process);

/* Cover crucible functions */
void process_cover(Process *process);
bool cover_crucible(Process *process);		// checks if the foundry is in the cover crucible position - if not, move motors safely to that position


#endif /* INC_PROCESSHANDLER_H_ */
