/*
 * commandHandler.h
 *
 *  Created on: Aug 12, 2022
 *      Author: MM3D | awitc
 */

#ifndef INC_COMMANDHANDLER_H_
#define INC_COMMANDHANDLER_H_

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stm32h7xx_hal.h"
#include "define.h"
#include "sendData.h"
#include "cmsis_os2.h"
#include "stdbool.h"
#include "motionGenerator.h"
#include "motorHandler.h"
#include "processHandler.h"
#include "pouringControl.h"
#include "sensorRead.h"

enum commandType {
	LED = 0,
	BUZZER,
	LATCH,
	MOTOR,
	VALVE,
	HEATERS,
	SENSOR,
	PROCESS,
	SYSTEM,
};

enum LED_commands {
	LED_GREEN_ON = 0,
	LED_GREEN_OFF,
	LED_GREEN_BLINK,

	LED_YELLOW_ON,
	LED_YELLOW_OFF,
	LED_YELLOW_BLINK,

	LED_RED_ON,
	LED_RED_OFF,
	LED_RED_BLINK,

	LED_PANEL_ON,
	LED_PANEL_OFF,
};

enum BUZZER_commands {
	BUZZER_ON = 0,
	BUZZER_OFF,
	BUZZER_SET
};

enum LATCH_commands {
	LATCH_ON = 0,
	LATCH_OFF,
};

enum MOTOR_commands {
	MOTOR_C_SETPOINT = 0,
	MOTOR_C_FRWD_DEG,
	MOTOR_C_SET_MAX_VEL,
	MOTOR_C_SET_MAX_ACC,
	MOTOR_C_PAUSE,
	MOTOR_C_STOP_HOME,
	MOTOR_C_SET_KP,
	MOTOR_C_SET_KI,
	MOTOR_C_SET_KD,
	MOTOR_C_SET_PWM,

	MOTOR_T_SETPOINT,
	MOTOR_T_FRWD_DEG,
	MOTOR_T_SET_MAX_VEL,
	MOTOR_T_SET_MAX_ACC,
	MOTOR_T_PAUSE,
	MOTOR_T_STOP_HOME,
	MOTOR_T_SET_KP,
	MOTOR_T_SET_KI,
	MOTOR_T_SET_KD,
	MOTOR_T_SET_PWM,


	MOTOR_Z_SETPOINT,
	MOTOR_Z_FRWD_MM,
	MOTOR_Z_SET_MAX_VEL,
	MOTOR_Z_SET_MAX_ACC,
	MOTOR_Z_PAUSE,
	MOTOR_Z_STOP_HOME,
	MOTOR_Z_SET_KP,
	MOTOR_Z_SET_KI,
	MOTOR_Z_SET_KD,
	MOTOR_Z_SET_PWM,

	MOTOR_ALL_STOP_HOME
};

enum VALVE_commands {
	VALVE_ON = 0,
	VALVE_OFF
};

enum SYSTEM_commands {
	SOFT_RESET = 0,
	TARE_LOADCELL,
	SET_VOLUME,
	EMERGENCY_STOP,
	CHECK_CRUCIBLE
};

enum HEATERS_commands {
	HEATERS_ON = 0,
	HEATERS_OFF,
	HEATERS_SET,
	HEATERS_CONTROL_ON,
	HEATERS_CONTROL_OFF
};

enum PROCESS_commands {
	START_HOMING = 0,
	START_POURING,
	START_VOLUME_DETECTION,
	START_DEGASSING,
	START_ADDING,
	START_NOZZLE_CHANGE,
	NOZZLE_CHANGE_DONE,
	START_EMPTYING,
	EMPTYING_DONE,
	START_CRUCIBLE_CHANGE,
	START_COVER_CRUCIBLE,
	START_FLASK_STATUS,
	STOP_PROCESS
};

typedef struct {
	uint8_t type;
	uint8_t command;
	double value;
	double value2;
	double value3;
} command;

extern uint8_t rxBuffer;
//extern uint8_t rxData[RX_SIZE];
extern uint8_t cnt_rx;
extern uint8_t state_rx;

extern uint8_t led_green_blink, temp_green_blink_state;
extern uint8_t led_yellow_blink, temp_yellow_blink_state;
extern uint8_t led_red_blink, temp_red_blink_state;

extern TIM_HandleTypeDef htim3, htim8;
extern osMessageQueueId_t sendQueueHandle;

extern Motor_Handle motor_T, motor_Z, motor_C;

extern osMessageQueueId_t commandsExecuteQueueHandle;

extern float temp_target;
extern bool temperature_control_enabled;

extern hx711_t Hx711_;

typedef struct {
	uint8_t msg[RX_SIZE];
	int len;
} queue_commandRx;

extern queue_commandRx queueCommandRx;
extern int is_target_temp_achieved;
command get_command(uint8_t rxData[]); 		/* splits received data into commands */
void execute_command(command *Command);		/* performs actions based on the given commands */
void execute_command_from_value(uint8_t type_v, uint8_t command_v, float value_v);
void execute_command_from_values(uint8_t type_v, uint8_t command_v, float value_v, float value2_v);
void write_state(command* Command);
void led_blink(); 							/* used in tim17 to check if blink commands have been used */

#endif /* INC_COMMANDHANDLER_H_ */
