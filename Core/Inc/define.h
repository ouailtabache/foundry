/*
 * define.h
 *
 *  Created on: Aug 17, 2022
 *      Author: MM3D
 */

#ifndef INC_DEFINE_H_
#define INC_DEFINE_H_

/* Enable - 1, disable - 0 for all the messages sent over by the STM to the Raspberry Pi
 *
 * ACK - acknowledgment messages, should be enabled
 * ERR - error messages, should be enabled
 * ECHO - used mostly for debugging, should be disabled during normal foundry operation
 * VALUE - same as ECHO
 * DATA - main source of sensor info for the RPi, should be enabled
 *
 * SENSOR_INIT - message after proper initialization of sensors, for debugging
 * THREAD_INIT - message after proper thread initialization, for debugging
 * EXTI - message triggered by interrupts, for debugging
 *
 *  */

#define ENABLE_ACK_MESSAGES 			1
#define ENABLE_ERR_MESSAGES 			0
#define ENABLE_ECHO_MESSAGES 			1
#define ENABLE_VALUE_MESSAGES 			1
#define ENABLE_DATA_MESSAGES 			1
#define ENABLE_SENSOR_INIT_MESSAGES 	0
#define ENABLE_THREAD_INIT_MESSAGES 	0
#define ENABLE_EXTI_MESSAGES 			0

/* Enables for motor control and motor generation
 *
 * PID - should be always enabled
 * MOTION - can be disabled for PID tuning, enabled for normal operation -  !! CRUCIAL for processes to work !!
 *
 *  */

#define ENABLE_PID_CONTROL 				1
#define ENABLE_MOTION_CONTROL 			1

#define ENABLE_MOTOR_LIMITS 			0 	// disable - allows for full range of movements

/*	commandHandler definitions	*/
#define HANDSHAKE_TYPE_COMMAND 			"$SYSTEM:HANDSHAKE;"
#define HANDSHAKE_COMMAND 				"HANDSHAKE"
#define RX_SIZE 						50
#define TX_SIZE 						160
#define MSG_SIZE 						80

/*	Task timing constants	*/
#define SENSOR_READ_T 					300
#define CONTROL_TEMP_T 					500
#define PROCESS_HANDLER_T 				50
#define MOTION_CONTROL_T 				20
#define MOTOR_CONTROL_T 				10

/*
 * ---------------------------- PIN DEFINITIONS -----------------------------
 *  */

/* LEDS */
#define LED_RED 				GPIOD, GPIO_PIN_1
#define LED_YELLOW 				GPIOD, GPIO_PIN_10
#define LED_GREEN				GPIOD, GPIO_PIN_4
#define LED_PANEL				GPIOB, GPIO_PIN_14

#define VALVE_PIN				GPIOD, GPIO_PIN_0
#define HEATER_PIN				GPIOF, GPIO_PIN_11

/*	Thermocouples MAX31856	*/
#define THERM_1 				GPIOD, GPIO_PIN_15
#define THERM_2 				GPIOG, GPIO_PIN_9

/* Loadcells */
#define HX711_CLK_GPIO_Port		GPIOE
#define HX711_DATA_GPIO_Port	GPIOE
#define HX711_CLK_Pin			GPIO_PIN_11
#define HX711_DATA_Pin			GPIO_PIN_14

/* Encoders */
#define ENCODER_T_PIN 			GPIOD, GPIO_PIN_11
#define ENCODER_Z_PIN 			GPIOD, GPIO_PIN_12
#define ENCODER_C_PIN 			GPIOD, GPIO_PIN_13

/* EXTI */
#define E_Z_AXIS				GPIO_PIN_2
#define EM_STOP					GPIO_PIN_3
#define E_VALVE_OPEN			GPIO_PIN_4
#define E_VALVE_CLOSE 			GPIO_PIN_5
#define E_DOOR_H				GPIO_PIN_9
#define E_TURRET 				GPIO_PIN_7
#define E_CRUCIBLE				GPIO_PIN_8

/* MOTORS ENABLE PINS*/
#define MOTOR_C_R_EN			GPIOA, GPIO_PIN_1
#define MOTOR_C_L_EN			GPIOA, GPIO_PIN_0
#define MOTOR_T_R_EN			GPIOD, GPIO_PIN_3
#define MOTOR_T_L_EN			GPIOC, GPIO_PIN_0
#define MOTOR_Z_R_EN			GPIOB, GPIO_PIN_0
#define MOTOR_Z_L_EN			GPIOA, GPIO_PIN_4
/* ---------------------------------------------------------------------------*/

/* Motor & Encoder specific conversions */
#define ENCODER_RES 			200//64
//#define C_RATIO 				1090		// gear reduction ratio for the C motor
#define C_RATIO					1108
#define T_RATIO 				100			// gear reduction ratio for the T motor
#define Z_RATIO 				15			// gear reduction ratio for the Z motor
#define GEAR_TOOTH_N 			20			// number of gear teeth for the Z belt
#define GEAR_TOOTH_VAL 			2			// [mm] of jump which each gear tooth provides


/*	PID parameters	*/

/* For now - PD control, the integral term is causing difficulties
 *
 * In the motor_PID_Update_v2 function, it is advised to keep the Kd at a very low value, as it oscillates greatly
 * Exemplary values for v2: (not extensively tested)
 * Kp =	7
 * Ki = 5
 * Kd =	0.1
 *
 * ############################################################################
 * ### DO NOT USE MOTION CONTROL GAINS WHEN MOTION GENERATOR IS NOT ENABLED ###
 * ############################################################################
 *
 * When the motor generator is enabled, the controller has to be more robust
 * than in the case of pure PD control, as the tests have shown that
 * the error after the motion can be minimized with increasing the
 * proportional gain. Without motion generator enabled, lower values are advised:
 *
 * ---------- CRUCIBLE -----------------
 *
 * Motion generator:
 * Kp =	18
 * Ki = 0
 * Kd =	4.5
 *
 * Raw PD Control:
 * Kp =	9
 * Ki = 0
 * Kd = 1.5
 *
 * NOTES:
 * - motion gen. : Error ~20-40 encoder ticks, 0.1 - 0.2 deg
 * - raw PD : Error ~10-20 encoder ticks, 0.05 - 0.1 deg
 */

#define MOTOR_C_KP 			18
#define MOTOR_C_KI 			0
#define MOTOR_C_KD 			4.5
#define MOTOR_C_LIMIT 		4000

 /* ----------- TURRET ------------------
 *
 * Motion generator:
 * Kp = 30
 * Ki = 0
 * Kd = 12
 *
 * Raw PD Control:
 * Kp = 28
 * Ki = 0
 * Kd = 12
 *
 *	NOTES:
 *	- Error ~10-20 encoder ticks, 0.562 - 1.125 deg
 *	- very aggressive, movements above 10 deg with the motion generator is disabled are not advisable
 *	- depending on the direction mostly
 */

#define MOTOR_T_KP 			30
#define MOTOR_T_KI 			0
#define MOTOR_T_KD 			12
#define MOTOR_T_LIMIT 		4000

 /* ----------- Z-AXIS ------------------
 *
 * Motion generator:
 * Kp = 26
 * Ki = 0
 * Kd = 20
 *
 * Raw PD Control:
 * Kp = 20
 * Ki = 0
 * Kd = 20
 *
 * NOTES:
 * - Error ~5-20 encoder ticks, 0.2 - 0.8 mm
 * - longer movements -> smaller error
 * - depending on the direction mostly
*/

#define MOTOR_Z_KP 			26
#define MOTOR_Z_KI 			0.001
#define MOTOR_Z_KD 			20
#define MOTOR_Z_LIMIT 		4000


/*	----------------------------- Motion parameters -----------------------------------*/

#define MOTOR_C_MAX_VEL 		1*2.7
#define MOTOR_C_MAX_ACC 		0.0005*2.7
#define MOTOR_C_LIM_MIN 		0
#define MOTOR_C_LIM_MAX 		180
#define MOTOR_C_OFFSET 			0 		// offsets the motor endstop position
#define MOTOR_C_HOME_OFFSET 	0 		// offset applied after homing is done

#define MOTOR_C_FRWD_FACTOR 	-1 		// Sets the direction of forward. If it is -1, it means that the forwards direction is against the encoder readings.

#define MOTOR_T_MAX_VEL 		9*2.7
#define MOTOR_T_MAX_ACC 		0.005*2.7
#define MOTOR_T_LIM_MIN 		-180		//PREVIOUS VALUE:11
#define MOTOR_T_LIM_MAX 		180		//PREVIOUS VALUE:180
#define MOTOR_T_OFFSET 			12 		// 11	// offsets the motor endstop position
#define MOTOR_T_HOME_OFFSET 	0 		// offset applied after homing is done
#define MOTOR_T_FRWD_FACTOR 	+1

#define MOTOR_Z_MAX_VEL 		10*2.7
#define MOTOR_Z_MAX_ACC 		0.005*2.7//PREVIOUS VALUE:0.025
#define MOTOR_Z_LIM_MIN 		5	//PROEVIOUS VALUE:5
#define MOTOR_Z_LIM_MAX 		270
#define MOTOR_Z_OFFSET 			0 		// offsets the motor endstop position
#define MOTOR_Z_HOME_OFFSET 	5 		// offset applied after homing is done
#define MOTOR_Z_FRWD_FACTOR 	+1

/*	----------------------------- Process parameters -----------------------------------*/

/*
 *
 * General positions
 *
 *  */

#define MOTOR_Z_DOWN_POS 265
#define MOTOR_Z_TOP_POS 5

#define MOTOR_T_COVER_CRUCIBLE_POS 52.5

#define CRUCIBLE_INIT_POS 0

/*
 *
 * Thresholds - sensors and movements
 *
 *  */

#define PRESSURE_THRESHOLD 						10
#define TEMP_THRESHOLD 							20 			// deg
#define MOTOR_C_OFFSET_THRESHOLD 				0.25		// deg
#define MOTOR_T_OFFSET_THRESHOLD 				2		// deg PREVIOUS=1.5
#define MOTOR_Z_OFFSET_THRESHOLD 				1 		// mmv previous=1

/*
 *
 * Timings
 *
 *  */

#define VALVE_TIMEOUT_T 3000

/*
 *
 * Homing
 *
 * */

#define MOTOR_C_HOME_BACK_OFF_DISTANCE_DEG 		5
#define MOTOR_C_HOME_MAX_DISTANCE_DEG 			-180
#define MOTOR_C_HOME_MAX_VEL 					1*2.7
#define MOTOR_C_POURING_VEL 					1*2.7

#define MOTOR_T_HOME_BACK_OFF_DISTANCE_DEG 		16
#define MOTOR_T_HOME_MAX_DISTANCE_DEG 			-200
#define MOTOR_T_HOME_MAX_VEL 					0.25*2.7

#define MOTOR_Z_HOME_BACK_OFF_DISTANCE_MM 		5
#define MOTOR_Z_HOME_MAX_DISTANCE_MM 			-290
#define MOTOR_Z_HOME_MAX_VEL 					0.85*2.7

/* Pouring */
#define flow_rate_coefficient				1

/* Probing */
#define PROBING_PRESSURE_THRESHOLD 			1050

/* VOLUME_DETECTION */
#define LASER_TIMEOUT_T           1500

#define MOTOR_Z_PROB_VERY_LOW_VEL       0.1
#define MOTOR_Z_PROB_LOW_VEL        0.2
#define MOTOR_Z_DETECTION         0.4
#define MOTOR_Z_LIQUIDE_POS         85


#define LASER_MOTOR_T            1
#define MOTOR_Z_PROB_VERY_LOW_VEL       0.1*2.7
#define MOTOR_Z_PROB_LOW_VEL        0.2*2.7
#define MOTOR_Z_DETECTION         0.4*2.7
#define LASER_MOTOR_T_POS           10

//for reading smooth pressure sensor
#define WINDOW_SIZE               14

//
#define DELTA_1_MM_VOLUME             98930
#define DELTA_MM                9.1

#define SENSOR_RATIO              0.0262186614 // bit to mm
#define SENSOR_ORIGINE              200 //MINIMAL VALUE
#define SENSOR_ANGLE              0.349066 // 20°

/* Degassing */

#define t_MIN_DEGASSING 					10 // minimal time for the degassing process to start
#define DEGASSING_MOTOR_T_INIT_POS 			152.5

#define DEGASSING_MOTOR_T_LEFT_POS 			125
#define DEGASSING_MOTOR_T_RIGHT_POS 		180

#define DEGASSING_INIT_OVERLAP_MOTOR_T 		35 	// in mm, refers to z position
#define DEGASSING_COVER_OVERLAP_MOTOR_T 	35 	// in mm, refers to z position
#define DEGASSING_COVER_OVERLAP_MOTOR_Z 	55 // in deg, refers to T position

#define DEGASSING_MOTOR_T_PATTERN_VEL 		0.1
#define DEGASSING_MOTOR_Z_INSERT_VEL 		5

#define DEGASSING_PRESSURE 					1000 // to be changed
#define DEGASSING_PRESSURE_THRESHOLD 		500 // to be changed

#define TEMP_FOUNDRY_PRESSURE_READING 		1000
#define MOTOR_Z_DEGASSING_DOWN_POS 			240


/* Adding */
#define ADDING_MOTOR_T_POS 					10
#define MOTOR_Z_ADDING_POS 					250


/* Nozzle Change */


/* Emptying */
#define EMPTY_CRUCIBLE_POS 					120

/* Crucible Change */
#define CRUCIBLE_CHANGE_POS 				180

#endif /* INC_DEFINE_H_ */

