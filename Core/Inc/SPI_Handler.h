/*
 * SPI_Handler.h
 *
 *  Created on: Aug 23, 2022
 *      Author: MM3D
 */

#ifndef INC_SPI_HANDLER_H_
#define INC_SPI_HANDLER_H_

#include "define.h"
#include "stdio.h"
#include "stm32h7xx_hal.h"
#include "string.h"
#include "encoders.h"

typedef enum
{
  SPI_START = 0U,
  SPI_STOP
} SPI_PinState;

extern UART_HandleTypeDef huart3;
extern SPI_HandleTypeDef hspi1;
extern uint8_t SPI1Buffer[30]; // SPI buffer of SPI1

void SPI_SET(uint8_t slave_ID, SPI_PinState state); /* START/STOP communication to a chosen slave */
void SPI_SET_MDR0(uint8_t slave_ID);
uint8_t SPI_READ_MDR0(uint8_t slave_ID);

#endif /* INC_SPI_HANDLER_H_ */
