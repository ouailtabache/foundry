/*
 * motionGenerator.h
 *
 *  Created on: 14 Sep 2022
 *      Author: MM3D
 */

#ifndef INC_MOTIONGENERATOR_H_
#define INC_MOTIONGENERATOR_H_

#include "stdbool.h"
#include "math.h"
#include "cmsis_os.h"
#include "stdlib.h"
#include "additionalFunctions.h"

typedef struct {
	float maxVel;
	float maxAcc;
	float initPos;
	float pos;
	float vel;
	float acc;
	float oldPos;
	float oldPosRef;
	float oldVel;

	float dBrk;
	float dAcc;
	float dVel;
	float dDec;
	float dTot;

	float tBrk;
	float tAcc;
	float tVel;
	float tDec;

	float velSt;

	unsigned long oldTime;
	unsigned long lastTime;
	unsigned long deltaTime;

	short int signM;      	// 1 = positive change, -1 = negative change
	bool shape;      	// true = trapezoidal, false = triangular

	bool isFinished;

	/* speed control parameters */
	double target; /* the point which the pid has to reach */
	//double t_target; /* time needed to reach the target [ms] */
	//double t_v_max; /* time during which the motor rotates at v_max */
	//double t_acc; /* acceleration time [ms] */
	//double t_dec; /* deceleration time [ms] */
	//double v_max; /* max velocity [ticks/ms]  |  5813.333 ticks/s = 5 rpm */

} motionGenerator_HandleTypeDef;

motionGenerator_HandleTypeDef motionGenerator_Init(float aMaxVel,float aMaxAcc ,float aInitPos);
float motion_update(motionGenerator_HandleTypeDef *motionGenerator);
void motion_reset(motionGenerator_HandleTypeDef *motionGenerator);
void motion_calculateTrapezoidalProfile(motionGenerator_HandleTypeDef *motionGenerator, float posRef) ;

bool getFinished(motionGenerator_HandleTypeDef *motionGenerator);

float getVelocity(motionGenerator_HandleTypeDef *motionGenerator);

float getAcceleration(motionGenerator_HandleTypeDef *motionGenerator);

void setMaxVelocity(motionGenerator_HandleTypeDef *motionGenerator, float aMaxVel);

void setMaxAcceleration(motionGenerator_HandleTypeDef *motionGenerator, float aMaxAcc);

void setInitPosition(motionGenerator_HandleTypeDef *motionGenerator, float aInitPos);

#endif /* INC_MOTIONGENERATOR_H_ */
