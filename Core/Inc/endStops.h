#ifndef INC_ENDSTOPS_H_
#define INC_ENDSTOPS_H_

#include "stdbool.h"
#include "define.h"
#include "stm32h7xx_hal.h"
#include "cmsis_os.h"

typedef struct{
	bool e_z_axis;
	bool em_stop;
	bool e_valve_open;
	bool e_valve_close;
	bool e_door_h;
	bool e_turret;
	bool e_crucible;

	bool em_stop_flag;
	bool emergencyMessageTransmitted_init;
} end_stops;

extern end_stops endStops;

void initStateInterrupts(end_stops *endStops);
void updateStateInterrupts(end_stops *endStops);
void printStates(osMessageQueueId_t sendQueueHandle, end_stops *endStops);

#endif /* INC_ENDSTOPS_H_ */
