/*
 * ads1115.h
 *
 *  Created on: Aug 17, 2022
 *      Author: DAVID QUEZADA
 */

#ifndef INC_ADS1115_H_
#define INC_ADS1115_H_


#include "main.h"

#define ADS1115_ADDRESS 0x48 << 1

typedef struct {
	I2C_HandleTypeDef* i2c_handle;
	uint8_t device_address;
	uint8_t data[8];
} Ads1115_HandleTypeDef;

Ads1115_HandleTypeDef ads1115_Init(I2C_HandleTypeDef* i2c_handle,uint8_t device_address);



void ads1115_GetValue(Ads1115_HandleTypeDef *ads1115, uint16_t *pressure);
void ads1115_GetValue_v2(Ads1115_HandleTypeDef *ads1115, uint16_t *pressure,HAL_StatusTypeDef res[]);



#endif /* INC_ADS1115_H_ */
