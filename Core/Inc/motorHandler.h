/*
 * motorHandler.h
 *
 *  Created on: 4 sty 2023
 *      Author: MM3D
 *
 */

#ifndef SRC_MOTORHANDLER_H_
#define SRC_MOTORHANDLER_H_

#include "cmsis_os2.h"
#include "sendData.h"
#include "define.h"
#include "encoders.h"
#include "motionGenerator.h"

enum state {
	OFF = 0,
	ON
};

typedef enum {
	MOTOR_C = 0,
	MOTOR_T,
	MOTOR_Z
} MOTOR_;

//typedef enum {
//	MOTOR_IS_RUNNING = 0,
//	MOTOR_IS_OFF
//} Motor_statusHandle;

typedef struct {
	double input, prev_input;
	double setpoint;
	double error_p, error_i, error_d;
	double error, last_error;
	double Kp, Ki, Kd;
	double output, output_MAX, output_MIN;
	double initPos;

	double integral, derivative;
} PID_;

typedef struct {

	float K_P, K_I, K_D;
	float PWM_LIMIT;
	float MAX_VEL;
	float MAX_ACC;
	int FRWD_FACTOR;
	float LIMIT_MIN, LIMIT_MAX;
	float INIT_POS_OFFSET;
	float HOME_OFFSET;
	float OFFSET_THRESHOLD;
	float HOME_BACK_OFF_DIST;
	float HOME_MAX_DIST;
	float HOME_MAX_VEL;
	float GEARBOX_RATIO;

} CONFIG_;

typedef struct {
	PID_ PID;
	motionGenerator_HandleTypeDef motion;
	CONFIG_ config;
	ENCODER_ encoder;
	MOTOR_ motor;
	UNIT_ unit;

	Motor_statusHandle state;
	double initPos; // used to reset both PID and Motion

} Motor_Handle;

/* Extern variables */
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim5;
extern osMessageQueueId_t sendQueueHandle;

/* Init functions */
void motor_Init(Motor_Handle *motor, MOTOR_ thisMotor, ENCODER_ thisEncoder, UNIT_ unit);

/* PID functions */
void motor_PID_Init(Motor_Handle *motor);
void motor_PID_Update(Motor_Handle *motor);
void motor_PID_Update_v2(Motor_Handle *motor); /* second attempt - derivative creates a lot of oscillations */
void motor_PID_Reset(Motor_Handle *motor);

/* Motion functions */
void motor_Motion_Init(Motor_Handle *motor); // just a kind of wrapper for the motion generator

/* Config functions */
void motor_Config_Init(Motor_Handle *motor);

/* PWM functions */
void init_PWM();
void set_PWM(Motor_Handle *motor);
void enable_PWM(Motor_Handle *motor);
void disable_PWM(Motor_Handle *motor);

/* Motor movement functions */
bool isMotorClose(Motor_Handle *motor, float target_position, float threshold);
bool isMotorBetween(Motor_Handle *motor, float lower_limit, float higher_limit);
float getCurrentMotorPosition(Motor_Handle *motor);
float getTargetMotorPosition(Motor_Handle *motor);
bool setMotorPosition(Motor_Handle *motor, float position);

#endif /* SRC_MOTORHANDLER_H_ */
