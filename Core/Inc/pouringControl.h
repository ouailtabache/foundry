/*
 * pouringControl.h
 *
 *  In this file is presented all the code related with the pouring control.
 * Also, all the equations that model the crucible and were tested in Matlab/Simulink.
 *  Created on: 29 Sep 2022
 *      Author: MM3D | DAVID QUEZADA
 */

#ifndef INC_POURINGCONTROL_H_
#define INC_POURINGCONTROL_H_


#define H_DETECTED_MAX  243
#define H_DETECTED_MIN	0

#define Q_MAX    500000			// This value it may change according with the maximum flowrate [mm^3/s]
#define VOLUME_MAX 24122000     // [mm^3]

#define MAX_OMEGA 89
#define MAX_MOUTH_HEIGHT 20

// Default test values for flow rate generator
#define DEFAULT_Q  	350000
#define DEFAULT_TST 8
#define DEFAULT_TR 	2.5

#define DEFAULT_VOLUME 6000000 // [mm^3]


#define PI 3.141592654

typedef struct {
	float hDetected;
	float thetaStart;
	float initialVolume;


	double dh_coeff;
	float deltaTheta;

	float Tr;
	float Tst;
	float Q;


	float theta;
	float omega;
	float h;
	double dt_h;

	float q_ref;

	float newTime;
	float oldTime;

} pouringControl_HandleTypeDef;


typedef struct {
	float theta;
	float h;
	float q_ref;
	float omega_ref;

	float dh_Vr;
	float dtheta_Vr;
	float dtheta_Vs;


} dt_H_HandleTypeDef;


typedef struct {
	float q_ref;
	float dtheta_Vr;
	float dtheta_Vs;
	float dh_Vr;
	double dt_h;

} OmegaRef_HandleTypeDef;


extern pouringControl_HandleTypeDef pouringControl;

pouringControl_HandleTypeDef pouringControl_Init();


float calculateInitialVolume( float heightDetected);

float calculateInitialPouringAngle( float initialVolume);

float calculateVs( float theta);

double calculateVr(float theta, float h);

double calculateDh_Vr(float theta);

float calculateDtheta_Vr(pouringControl_HandleTypeDef *pouringControl,float theta, float h);

float calculateDtheta_Vs(pouringControl_HandleTypeDef *pouringControl,float theta);

double calculateDt_H(pouringControl_HandleTypeDef *pouringControl,float t);

float calculateOmegaRef(pouringControl_HandleTypeDef *pouringControl, OmegaRef_HandleTypeDef *OmegaRef_variables);

float generateFlowRate(pouringControl_HandleTypeDef *pouringControl,float t);

float updateThetaProfile(pouringControl_HandleTypeDef *pouringControl, float t);

float calculateIntegralOmega(pouringControl_HandleTypeDef *pouringControl, float Omega, float theta);

float calculateIntegralDh(pouringControl_HandleTypeDef *pouringControl, float Dh, float h);

int setPouringParameters(pouringControl_HandleTypeDef *pouringControl, float Q, float Tr, float Ts);

int setInitialAngle(pouringControl_HandleTypeDef *pouringControl, float init_angle);

#endif /* INC_POURINGCONTROL_H_ */
