/*
 * additionalFunctions.h
 *
 *  Created on: Sep 23, 2022
 *      Author: MM3D
 */

#ifndef INC_ADITIONALFUNCTIONS_H_
#define INC_ADITIONALFUNCTIONS_H_

int sign_here(float val);
float abs_here(float val);



// sign_here is CRUCIAL for the motionGenerator to work, play with it carefully


#endif /* INC_ADITIONALFUNCTIONS_H_ */
