/*
 * sensorRead.h
 *
 *  Created on: Aug 18, 2022
 *      Author: MM3D
 */

#ifndef INC_SENSORREAD_H_
#define INC_SENSORREAD_H_

#include <foundryData.h>
#include "define.h"
#include "hx711.h"		/* loadcells */
#include "am2320.h"		/* temperature & humidity sensor */
#include "ads1115.h"	/* pressure sensor */
#include "max31856.h"
#include "encoders.h"
#include "cmsis_os.h"
#include "sendData.h"

extern I2C_HandleTypeDef hi2c1;
extern SPI_HandleTypeDef hspi2;
extern osMessageQueueId_t sendQueueHandle;

typedef struct {
	Ads1115_HandleTypeDef Ads1115_;
	Am2320_HandleTypeDef Am2320_;
	hx711_t Hx711_;
	Max31856_HandleTypeDef Max31856_1_;
	Max31856_HandleTypeDef Max31856_2_;
} sensors;

sensors sensors_init();
foundry sensors_read(sensors *Sensors, foundry *Foundry);

#endif /* INC_SENSORREAD_H_ */
